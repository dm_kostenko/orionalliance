import Intro from 'components/industries/healthcare/intro';
import Description from 'components/industries/healthcare/description';
import Services from 'components/industries/healthcare/services';
import Contact from 'components/industries/healthcare/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Healthcare = () => (
  <React.Fragment>
    <PageHead
      title='Medical IT Services and Support - Industries We Serve | Orion Alliance'
      description='Orion Alliance provides a range of medical IT services to improve patient incomes, support remote medical delivery, and streamline internal business processes.'
      keywords='medical IT services, medical IT support, IT services for clinics, mobile app for hospital, web app for clinic, health information exchange, mobile app for patients, web app for patients'
    />
    <Intro />
    <Description />
    <Services />
    <Contact />
  </React.Fragment>
);

export default memo(Healthcare);
