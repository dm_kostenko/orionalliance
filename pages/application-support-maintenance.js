import Intro from 'components/services/applicationMaintenance/intro';
import Choose from 'components/services/applicationMaintenance/choose';
import AppServices from 'components/services/applicationMaintenance/appServices';
import Contact from 'components/services/applicationMaintenance/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Support = () => (
  <React.Fragment>
    <PageHead
      title='Application Support & Maintenance - Development Services | Orion Alliance'
      description='Connect with Orion Alliance’s 24/7 support team today for reliable ongoing application maintenance, monitoring and support.'
      keywords='application support, application maintenance, application development, app support, app maintenance, app development'
    />
    <Intro />
    <Choose />
    <AppServices />
    <Contact />
  </React.Fragment>
);

export default memo(Support);
