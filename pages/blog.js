import Intro from 'components/blog/intro';
import Contact from 'components/blog/contact';
import Articles from 'components/blog/articles';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Blog = () => (
  <React.Fragment>
    <PageHead
      title='Blog | Orion Alliance'
      // description='Orion Alliance is a leading software development company who provides onsite and offshore services in Latvia, The Netherlands and Poland. Founded in 2007, Orion Alliance works closely with ambitious clients to overcome challenging tasks and scale new heights.'
      // keywords='orion alliance about, software solutions, software for business, software technology, technical support, IT trends, app development, application development'
    />
    <Intro />
    <Articles />
    <Contact />
  </React.Fragment>
);

export default memo(Blog);
