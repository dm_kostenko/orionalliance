import Intro from 'components/industries/oilgas/intro';
import Description from 'components/industries/oilgas/description';
import Services from 'components/industries/oilgas/services';
import Contact from 'components/industries/oilgas/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Healthcare = () => (
  <React.Fragment>
    <PageHead
      title='Oil and Gas Industry - Industries We Serve | Orion Alliance'
      description='Orion Alliance’s bespoke software solutions help the oil & gas industry improve supply chain efficiency, worksite safety and risk mitigation.'
      keywords='enterprise software, software solution, software development, mobile development, application development'
    />
    <Intro />
    <Description />
    <Services />
    <Contact />
  </React.Fragment>
);

export default memo(Healthcare);
