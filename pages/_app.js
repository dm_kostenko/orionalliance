import React from 'react';
import NextApp, { Container } from 'next/app';
import Router from 'next/router'

import * as gtag from '../client/gtag'
import getCustomAppProps from 'shared/getCustomAppProps';
import { setUrlPathPrefix } from 'utils/withPathPrefix';
import Layout from 'components/Layout';

Router.events.on('routeChangeComplete', url => gtag.pageview(url))

export default class App extends NextApp {
  static async getInitialProps({ Component, ctx }) {
    let hostUrl;

    if (process.browser) {
      hostUrl = __NEXT_DATA__.props.hostUrl;
    } else {
      hostUrl = 'https://' + ctx.req.hostname;
    }

    let pageProps = { hostUrl };

    if (Component.getInitialProps) {
      let componentProps = await Component.getInitialProps(ctx);
      pageProps = {
        ...componentProps,
        ...pageProps,
      };
    }
    const customAppProps = getCustomAppProps(ctx);
    setUrlPathPrefix(customAppProps.urlPathPrefix);

    const canonical =
      hostUrl +
      customAppProps.urlPathPrefix +
      (ctx.pathname === '/' ? '' : ctx.pathname);

    return { pageProps, customAppProps, canonical };
  }

  render() {
    const { Component, pageProps, customAppProps, canonical } = this.props;

    return (
      <Container>
        <Layout customAppProps={customAppProps} canonical={canonical}>
          <Component {...pageProps} />
        </Layout>
      </Container>
    );
  }
}
