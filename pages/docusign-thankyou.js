import PageHead from 'components/PageHead';
import Intro from 'components/docuSign/intro'
import CenterText from 'components/docuSign/centerText';
import memo from 'utils/memo';


const DocuSign = () => (
    <React.Fragment>
      <PageHead
        title='About Us | Orion Alliance'
        description='Orion Alliance is a leading software development company who provides onsite and offshore services in Latvia, The Netherlands and Poland. Founded in 2007, Orion Alliance works closely with ambitious clients to overcome challenging tasks and scale new heights.'
        keywords='orion alliance about, software solutions, software for business, software technology, technical support, IT trends, app development, application development'
      />
      <Intro />
    </React.Fragment>
  );
  
  export default memo(DocuSign);