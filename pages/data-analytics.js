import Intro from 'components/services/dataAnalytics/intro';
import Choose from 'components/services/dataAnalytics/choose';
import Description from 'components/services/dataAnalytics/description';
import Contact from 'components/services/dataAnalytics/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Data = () => (
  <React.Fragment>
    <PageHead
      title='Data Analytics - Development Services | Orion Alliance'
      description='From automated data mining to predictive modelling and pattern matching, Orion Alliance can setup, configure and monitor the right data analytics system for you.'
      keywords='Data Analytics system, IT consulting, business process out-sorting, business process improvement, data storing'
    />
    <Intro />
    <Description />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Data);
