import Intro from 'components/services/infrastructure/intro';
import Choose from 'components/services/infrastructure/choose';
import Description from 'components/services/infrastructure/description';
import Contact from 'components/services/infrastructure/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Infrastructure = () => (
  <React.Fragment>
    <PageHead
      title='Infrastructure Support - Development Services | Orion Alliance'
      description='Orion Alliance provides 24/7 on-site and remote infrastructure support services via live chat, video meetings, email and phone. Contact us for immediate support.'
      keywords='IT support, support services, technical support, application security, database security'
    />
    <Intro />
    <Description />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Infrastructure);
