import Intro from 'components/blog/articles/legacyToMicroservices/intro';
import Text from 'components/blog/articles/legacyToMicroservices/text';
import Contact from 'components/blog/articles/legacyToMicroservices/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const LegacyToMicroservices = () => (
  <React.Fragment>
    <PageHead
      title='Blog - Legacy To Mircroservices | Orion Alliance'
      // description='Modernise your legacy system, preserve your existing data and internal processes with risk-mitigated legacy migration services from Orion Alliance.'
      // keywords='Legacy Migration services, Migration Services, data migration'
    />
    <Intro />
    <Text />
    <Contact />
</React.Fragment>
);

export default memo(LegacyToMicroservices);
