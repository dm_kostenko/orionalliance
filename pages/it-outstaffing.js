import Intro from 'components/services/itOutstaffing/intro';
import Description from 'components/services/itOutstaffing/description';
import Solutions from 'components/services/itOutstaffing/solutions';
import Choose from 'components/services/itOutstaffing/choose';
import Contact from 'components/services/itOutstaffing/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Outstaffing = () => (
  <React.Fragment>
    <PageHead
      title='Enterprise App Development - Development Services | Orion Alliance'
      description='For over 10 years our company has transformed creative enterprise app ideas into practical solutions for the Banking, Healthcare, Financial and other sectors.'
      keywords='Enterprise App Development, app solution, application solution, complete application solution, app focused management solution'
    />
    <Intro />
    <Description />
    <Choose/>
    <Solutions/>
    <Contact />
  </React.Fragment>
);

export default memo(Outstaffing);
