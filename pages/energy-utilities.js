import Intro from 'components/industries/energy/intro';
import Description from 'components/industries/energy/description';
import Services from 'components/industries/energy/services';
import Contact from 'components/industries/energy/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Healthcare = () => (
  <React.Fragment>
    <PageHead
      title='Energy and Utilities Software - Industries We Serve | Orion Alliance'
      description='Orion Alliance’s energy and utilities software solutions help business owners increase productivity and customer satisfaction across multiple channels.'
      keywords='energy software, utility software, utility enterprise software, utility asset management'
    />
    <Intro />
    <Description />
    <Services />
    <Contact />
  </React.Fragment>
);

export default memo(Healthcare);
