import Intro from 'components/blog/articles/threeObstacles/intro';
import Text from 'components/blog/articles/threeObstacles/text';
import Contact from 'components/blog/articles/threeObstacles/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const ThreeObstacles = () => (
  <React.Fragment>
    <PageHead
      title='Blog - 3 Obstacles To Overcome Towards Microservices | Orion Alliance'
      // description='Modernise your legacy system, preserve your existing data and internal processes with risk-mitigated legacy migration services from Orion Alliance.'
      // keywords='Legacy Migration services, Migration Services, data migration'
    />
    <Intro />
    <Text />
    <Contact />
  </React.Fragment>
);

export default memo(ThreeObstacles);
