import Intro from 'components/services/enterprise/intro';
import Choose from 'components/services/enterprise/choose';
import Description from 'components/services/enterprise/description';
import Contact from 'components/services/enterprise/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Enterprise = () => (
  <React.Fragment>
    <PageHead
      title='Enterprise App Development - Development Services | Orion Alliance'
      description='For over 10 years our company has transformed creative enterprise app ideas into practical solutions for the Banking, Healthcare, Financial and other sectors.'
      keywords='Enterprise App Development, app solution, application solution, complete application solution, app focused management solution'
    />
    <Intro />
    <Description />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Enterprise);
