import Intro from 'components/services/integration/intro';
import Choose from 'components/services/integration/choose';
import Description from 'components/services/integration/description';
import Contact from 'components/services/integration/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Integration = () => (
  <React.Fragment>
    <PageHead
      title='Enterprise App Integration - Development Services | Orion Alliance'
      description='Orion Alliance provides custom enterprise app integration solutions for businesses of any size in the Banking, Healthcare, Manufacturing and other sectors.'
      keywords='App Integration Services, App Integration solution, application development, app development, web app development'
    />
    <Intro />
    <Description />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Integration);
