import Intro from 'components/services/cloud/intro';
import Description from 'components/services/cloud/description';
import CloudComputingServices from 'components/services/cloud/cloudComputingServices';
import WhyMigrate from 'components/services/cloud/whyMigrate';
import Contact from 'components/services/cloud/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Data = () => (
  <React.Fragment>
    <PageHead
      title='Cloud Computing - Development Services | Orion Alliance'
      description='Take your business to soaring new heights with custom reliable and secure SaaS, IaaS and PaaS cloud computing solutions from Orion Alliance.'
      keywords='cloud computing solutions, cloud computing, DevOps,cloud hosting, cloud development'
    />
    <Intro />
    <Description />
    <CloudComputingServices />
    <WhyMigrate />
    <Contact />
  </React.Fragment>
);

export default memo(Data);
