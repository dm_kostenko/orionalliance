import Intro from 'components/services/awsCostOptimization/intro';
import Solutions from 'components/services/awsCostOptimization/solutions';
import Examples from 'components/services/awsCostOptimization/examples';
import Description from 'components/services/awsCostOptimization/description';
import Contact from 'components/services/awsCostOptimization/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const AwsCostOptimization = () => (
  <React.Fragment>
    <PageHead
      title='AWS Cost Optimization | Orion Alliance'
      description='Optimize your cloud for greater productivity.'
      keywords='AWS, AWS cost, amazon web services, cost optimization'
    />
    <Intro />
    <Description />
    <Solutions />
    <Examples />
    <Contact />
  </React.Fragment>
);

export default memo(AwsCostOptimization);
