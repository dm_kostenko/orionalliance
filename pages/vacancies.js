import Intro from 'components/vacancies/intro';
import Descrition from 'components/vacancies/description';
import Choose from 'components/vacancies/choose';
import Contact from 'components/vacancies/contact';
import getVacancies from 'components/vacancies/getVacancies';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Vacancies = memo(({ vacancies }) => (
  <React.Fragment>
    <PageHead
      title='Vacancies | Orion Alliance'
      description='Join us and create a new reality as a part of our software development family.'
      keywords='job in Orion Alliance, Join Orion Alliance, vacancies in Orion Alliance, Orion Alliance team, join us'
    />
    <Intro />
    <Descrition />
    <Choose vacancies={vacancies} />

    <Contact />
  </React.Fragment>
));

Vacancies.getInitialProps = async () => {
  const vacancies = await getVacancies();
  return { vacancies };
};

export default Vacancies;
