import React from 'react';
import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';
import PageHead from 'components/PageHead';

const isProduction = process.env.NODE_ENV === 'production';

/*
export default class Error extends React.Component {
  
  static getInitialProps(context) {
    if (context.res) {
      context.res.writeHead(303, { Location: '/'});
      context.res.end();
    } else {
      Router.replace('/');
    }
    return null;
  }

  render() {
    return null;
  }
}
*/

const backgroundImgUrl = '/img/error_bg.jpg';
const topText = '404 - Page is not found';
const bottomText =
  "We're sorry, but the page you are looking for is not found.";

const introProps = {
  backgroundImgUrl,
  topText,
  bottomText,
};

const Error = () => (
  <React.Fragment>
    <PageHead title='Page is not found | Orion Alliance' />
    <IntroSectionSimple {...introProps} />
  </React.Fragment>
);

Error.getInitialProps = ctx => {
  if (!process.browser && ctx.req.path.endsWith('/')) {
    const { path, query } = ctx.req;

    const redirectQuery = require('querystring').stringify(query);

    const location =
      path.replace(/\/$/, '') + (redirectQuery ? '?' + redirectQuery : '');

    const status = isProduction ? 301 : 303;

    ctx.res.writeHead(status, {
      Location: location,
    });
    ctx.res.end();
  }
};

export default Error;
