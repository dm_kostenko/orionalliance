import Intro from 'components/services/mobileAppDev/intro';
import Choose from 'components/services/mobileAppDev/choose';
import Description from 'components/services/mobileAppDev/description';
import Contact from 'components/services/mobileAppDev/contact';
import Systems from 'components/services/mobileAppDev/systems';
import Why from 'components/services/mobileAppDev/why';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Mobile = () => (
  <React.Fragment>
    <PageHead
      title='Mobile Application Development - Development Services | Orion Alliance'
      description='Orion Alliance designs, tests and deploys applications for mobile and tablet devices running on iOS, Android, Windows and Blackberry.'
      keywords='Mobile App Development, mobile app, mobile application, mobile application software, mobile development'
    />
    <Intro />
    <Description />
    <Choose />
    <Systems />
    <Why />
    <Contact />
  </React.Fragment>
);

export default memo(Mobile);
