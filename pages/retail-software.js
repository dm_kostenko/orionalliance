import Intro from 'components/industries/retail/intro';
import Description from 'components/industries/retail/description';
import Services from 'components/industries/retail/services';
import Contact from 'components/industries/retail/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Healthcare = () => (
  <React.Fragment>
    <PageHead
      title='Retail Software Solutions - Industries We Serve | Orion Alliance'
      description='Orion Alliance’s software solutions enhance business growth and customer experiences. From Checkout to Stock Control and Loyalty Programs – you get it all.'
      keywords='retail software, application for retail, e-commerce and retail, retail software, ecommerce application'
    />
    <Intro />
    <Description />
    <Services />
    <Contact />
  </React.Fragment>
);

export default memo(Healthcare);
