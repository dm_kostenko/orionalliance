import Intro from 'components/services/qaAndTesting/intro';
import Description from 'components/services/qaAndTesting/description';
import WhyChoose from 'components/services/qaAndTesting/whyChoose';
import Solutions from 'components/services/qaAndTesting/solutions';
import Contact from 'components/services/qaAndTesting/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Data = () => (
  <React.Fragment>
    <PageHead
      title='QA & Testing - Development Services | Orion Alliance'
      description='Orion Alliance’s QA and testing services can help you identify key issues early and release higher quality products to market sooner.'
      keywords='testing services, automation testing software, qa services, quality assurance services'
    />
    <Intro />
    <Description />
    <WhyChoose />
    <Solutions />
    <Contact />
  </React.Fragment>
);

export default memo(Data);
