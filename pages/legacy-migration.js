import Intro from 'components/services/legacyMigration/intro';
import Services from 'components/services/legacyMigration/services';
import Description from 'components/services/legacyMigration/description';
import Contact from 'components/services/legacyMigration/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Legacy = () => (
  <React.Fragment>
    <PageHead
      title='Legacy Migration - Development Services | Orion Alliance'
      description='Modernise your legacy system, preserve your existing data and internal processes with risk-mitigated legacy migration services from Orion Alliance.'
      keywords='Legacy Migration services, Migration Services, data migration'
    />
    <Intro />
    <Description />
    <Services />

    <Contact />
  </React.Fragment>
);

export default memo(Legacy);
