import Intro from 'components/services/devops/intro';
import Choose from 'components/services/devops/choose';
import Description from 'components/services/devops/description';
import Contact from 'components/services/devops/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const DevOps = () => (
  <React.Fragment>
    <PageHead
      title='DevOps - Development Services | Orion Alliance'
      description='Find out how Orion Alliance can streamline your development and release cycle with a custom automated DevOps solution. Connect with a DevOps consultant today.'
      keywords='DevOps, DevOps solutions, app maintenance, application maintenance, app development, application development'
    />
    <Intro />
    <Description />
    <Choose />
    <Contact />
  </React.Fragment>
);

export default memo(DevOps);
