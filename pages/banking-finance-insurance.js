import Intro from 'components/industries/banking/intro';
import Description from 'components/industries/banking/description';
import Services from 'components/industries/banking/services';
import Choose from 'components/industries/banking/choose';
import Contact from 'components/industries/banking/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Banking = () => (
  <React.Fragment>
    <PageHead
      title='Banking Finance and Insurance - Industries We Serve | Orion Alliance'
      description='For over 10 years our company has provided the Banking, Financial and Insurance sector with innovative, flexible and scalable software solutions'
      keywords='banking software, financial software, internet bankng, mobile banking, custom payment software'
    />
    <Intro />
    <Description />
    <Services />
    <Choose />
    <Contact />
  </React.Fragment>
);

export default memo(Banking);
