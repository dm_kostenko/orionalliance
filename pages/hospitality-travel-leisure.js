import Intro from 'components/industries/hospitality/intro';
import Descrition from 'components/industries/hospitality/description';
import Choose from 'components/industries/hospitality/choose';
import Contact from 'components/industries/hospitality/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Hospitality = () => (
  <React.Fragment>
    <PageHead
      title='Hospitality, Travel and Leisure Software- Industries We Serve | Orion Alliance'
      description='Orion Alliance provides bespoke software solutions that connect customers to service providers in the Hospitality, Travel and Leisure Industry.'
      keywords='hotel online booking applicatio, hospitality software, travelling software, restaurant management software'
    />
    <Intro />
    <Descrition />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Hospitality);
