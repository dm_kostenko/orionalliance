import Intro from 'components/industries/transport/intro';
import Description from 'components/industries/transport/description';
import Services from 'components/industries/transport/services';
import Contact from 'components/industries/transport/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Healthcare = () => (
  <React.Fragment>
    <PageHead
      title='Transport, Traffic, Car Rental and Parking Software - Industries We Serve | Orion Alliance'
      description='Orion Alliance provides custom software solutions to power the various functions of the Transport, Traffic, Car Rental, Car Sharing and Parking industry.'
      keywords='transport software, traffic software, carshring, parking application, transportation, logistics software solution'
    />
    <Intro />
    <Description />
    <Services />
    <Contact />
  </React.Fragment>
);

export default memo(Healthcare);
