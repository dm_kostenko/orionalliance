import Intro from 'components/industries/manufacturing/intro';
import Descrition from 'components/industries/manufacturing/description';
import Choose from 'components/industries/manufacturing/choose';
import Contact from 'components/industries/manufacturing/contact';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Hospitality = () => (
  <React.Fragment>
    <PageHead
      title='Industrial Manufacturing Software - Industries We Serve | Orion Alliance'
      description='Orion Alliance specialise in all aspects of industrial manufacture software development from Big Data Analysis to Automation, IoT and Product Delivery'
      keywords='industrial manufacturing software, business process optimisation, industrial app, industrial application, industrial app development, industrial application development'
    />
    <Intro />
    <Descrition />
    <Choose />

    <Contact />
  </React.Fragment>
);

export default memo(Hospitality);
