import Intro from 'components/aboutUs/intro';
import Choose from 'components/aboutUs/choose';
import Contact from 'components/aboutUs/contact';
import Addresses from 'components/aboutUs/Addresses';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const About = () => (
  <React.Fragment>
    <PageHead
      title='About Us | Orion Alliance'
      description='Orion Alliance is a leading software development company who provides onsite and offshore services in Latvia, The Netherlands and Poland. Founded in 2007, Orion Alliance works closely with ambitious clients to overcome challenging tasks and scale new heights.'
      keywords='orion alliance about, software solutions, software for business, software technology, technical support, IT trends, app development, application development'
    />
    <Intro />
    <Choose />
    <Contact />
    <Addresses />
  </React.Fragment>
);

export default memo(About);
