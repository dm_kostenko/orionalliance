import Intro from 'components/services/webAppDev/intro';
import Services from 'components/services/webAppDev/services';
import Description from 'components/services/webAppDev/description';
import Contact from 'components/services/webAppDev/contact';
import WhyMigrate from 'components/services/webAppDev/whyMigrate';
import memo from 'utils/memo';
import PageHead from 'components/PageHead';

const Webapplication = () => (
  <React.Fragment>
    <PageHead
      title='Web Application Development - Development Services | Orion Alliance'
      description='Our 100+ strong team of web developers have over 10 years of experience creating web‑based applications for small and large enterprises for many industries.'
      keywords='Web Application Development, Web App Development, web app software, web app services, web application software, web application services'
    />
    <Intro />
    <Description />
    <Services />
    <WhyMigrate />
    <Contact />
  </React.Fragment>
);

export default memo(Webapplication);
