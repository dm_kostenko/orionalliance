import NextDocument, { Main, NextScript, Head } from 'next/document';
import { ServerStyleSheet, injectGlobal } from 'styled-components';
import getConfig from 'next/config';
import companyInfo from '../data/companyInfo.json';
import { jsonLD } from '../utils/formatters';

const {
  publicRuntimeConfig: { GA_TRACKING_ID },
} = getConfig();

injectGlobal`

  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  html {
    box-sizing: border-box;
    font-family: Lato, Arial, sans-serif;
    -ms-text-size-adjust: none;
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
  }

  body,
  input,
  button,
  textarea {
    box-sizing: border-box;
    
    overflow-x: hidden;
    line-height: 1.5;
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  *:focus {
    outline: none;
  }

  a {
    text-decoration: none;
    background-color: transparent;
    &:active,
    &:hover,
    &:visited {
      outline: 0;
      color: blue;
    }
  }

  :root {
    font-size: 14px;
  }

  @media (min-width: 768px) {
    :root {
      font-size: calc(0.875rem + ((1vw - 7.68px) * 0.5208));
    }
  }
  @media (min-width: 1920px) {
    :root {
      font-size: 20px;
    }
  }
`;

export default class Document extends NextDocument {
  static getInitialProps({ renderPage, req }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    );
    const styleTags = sheet.getStyleElement();

    return { ...page, styleTags, url: 'https://' + req.hostname };
  }

  render() {
    const {
      __NEXT_DATA__: { assetPrefix = '' },
      url,
    } = this.props;

    return (
      <html lang='en'>
        <Head>
          <meta charSet='UTF-8' />
          <meta name='viewport' content='width=device-width,initial-scale=1' />

          <link
            rel='preconnect'
            href='https://fonts.gstatic.com/'
            crossOrigin='true'
          />
          <link
            href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900|Roboto:900'
            rel='stylesheet'
          />
          <link rel='icon' href={`${assetPrefix}/favicon.ico`} />
          <link
            rel='apple-touch-icon'
            sizes='180x180'
            href={`${assetPrefix}/apple-touch-icon.png`}
          />
          <link
            rel='icon'
            type='image/png'
            sizes='32x32'
            href={`${assetPrefix}/favicon-32x32.png`}
          />
          <link
            rel='icon'
            type='image/png'
            sizes='16x16'
            href={`${assetPrefix}/favicon-16x16.png`}
          />
          <link rel='manifest' href={`${assetPrefix}/site.webmanifest`} />
          <link rel='mask-icon' href={`${assetPrefix}/safari-pinned-tab.svg`} />
          <meta
            name='msapplication-config'
            content={`${assetPrefix}/browserconfig.xml`}
          />
          <meta name='msapplication-TileColor' content='#a12643' />
          <meta name='theme-color' content='#ffffff' />
          <link
            rel='image_src'
            href={`${url}${assetPrefix}/img/orion_logo.png`}
          />
          <script
            type='application/ld+json'
            dangerouslySetInnerHTML={{
              __html: JSON.stringify(
                jsonLD(companyInfo, url, assetPrefix),
                null,
                2
              ),
            }}
          />
          {this.props.styleTags}
          <link rel="stylesheet" type="text/css" href="cookieconsent.min.css" />
        </Head>
        <body>
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
          />
          <script src="cookieconsent.min.js"></script>
          <script
            dangerouslySetInnerHTML={{
              __html:`
              window.addEventListener('load', function(){
                window.cookieconsent.initialise({
                  "palette": {
                    "popup": {
                      "background": "#252e39"
                    },
                    "button": {
                      "background": "transparent",
                      "text": "#14a7d0",
                      "border": "#14a7d0"
                    }
                  },
                  revokeBtn: "<div class='cc-revoke'></div>",
                  "theme": "classic",
                  "type": "opt-in",
                  onInitialise: function (status) {
                    if(status == cookieconsent.status.allow) myScripts();
                    else {
                      if(!document.defineGetter) {
                        Object.defineProperty(document, 'cookie', {
                            get: function(){return ''},
                            set: function(){return true},
                        });
                    } 
                    else {
                        document.defineGetter("cookie", function() { return '';} );
                        document.defineSetter("cookie", function() {} );
                      }
                    }
                  },
                  onStatusChange: function(status) {
                    if (this.hasConsented()) myScripts();
                    else {
                      if(!document.defineGetter) {
                        Object.defineProperty(document, 'cookie', {
                            get: function(){return ''},
                            set: function(){return true},
                        });
                    } 
                      else {
                          document.defineGetter("cookie", function() { return '';} );
                          document.defineSetter("cookie", function() {} );
                      }
                    }
                  }
                });
              });

              function myScripts() {
             
                   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                   ga('create', '${GA_TRACKING_ID}', 'auto');
                   ga('send', 'pageview');

                   window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());
                  gtag('config', '${GA_TRACKING_ID}');
             
             }
              `
            }}
          />
          <Main />
          <NextScript />
          {/*<div id='ind' />*/}
        </body>
      </html>
    );
  }
}
