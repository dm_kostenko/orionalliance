import PageHead from 'components/PageHead';
import Intro from 'components/index/intro';
import Overview from 'components/index/overview';
import Solutions from 'components/index/solutions';
import Industries from 'components/index/industries';
import Advantage from 'components/index/advantage';
import Contact from 'components/contact';
import memo from 'utils/memo';
import withPathPrefix from 'utils/withPathPrefix';

const title = 'Software Development Company | Orion Alliance';
const description =
  'Orion Alliance team provides a full spectrum of software services in accordance with your wishes and strategies. We provide complex analysis and management of IT projects with 24/7 availability.';
const keywords =
  'software development company, orion alliance, application support, app support, application maintenance, mobile app, mobile application, legacy migration, data analytics, cloud computing, software solutions, DevOps, web application development';

const Home = ({ hostUrl }) => (
  <React.Fragment>
    <PageHead title={title} description={description} keywords={keywords}>
      <meta property='og:title' content={title} />
      <meta property='og:description' content={description} />
      <meta
        property='og:image'
        content={`${hostUrl}${withPathPrefix('/img/orion_logo.png')}`}
      />
      {/*<meta property='og:image:type' content='image/png' />*/}
      <meta property='og:url' content={`${hostUrl}`} />
      <meta property='og:site_name' content='Orion Alliance' />
    </PageHead>
    <Intro />
    <Overview />
    <Solutions />
    <Industries />
    <Advantage />
    <Contact />
  </React.Fragment>
);

export default memo(Home);
