
function getCustomAppProps(context) {
  if (process.browser) {
    return window.__NEXT_DATA__.props.customAppProps;
  } else {
    return context.req.customAppProps;
  }
}

module.exports = getCustomAppProps;
