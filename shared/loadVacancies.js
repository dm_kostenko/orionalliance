const fs = require('fs');
const path = require('path');

function readFile(filePath) {

  try {
    const stats = fs.statSync(filePath);
    if (!stats.isFile()) {
      console.error(`given file is not a regular file: ${filePath}`);
      return null;
    }
  } catch (e) {
    console.error(`error reading file stats: ${filePath}`);
    return null;
  }

  try {
    const content = fs.readFileSync(filePath, 'utf8');
    return content;
  } catch (e) {
    console.error(`error reading file content: ${filePath}`);
    return null;
  }
}


module.exports = (rootDir = '.') => {

  const vacanciesPath = path.resolve(rootDir, 'data/vacancies/index.json');

  try {
    const index = JSON.parse(fs.readFileSync(vacanciesPath));

    const result = []

    for (const fileName of index) {
      const filePath = path.resolve(rootDir, 'data/vacancies', fileName);
      const content = readFile(filePath);
      if (content) {
        result.push(content);
      }
    }
    return { data: result };
  } catch (e) {
    console.log('cannot read index file')
    console.log(e)
  }
  return { data: [] }
  /*try {
    const data = JSON.parse(fs.readFileSync(vacanciesPath));
    return { data };
  } catch (e) {
    return { error: 'Cannot load vacancies: server error' };
  }*/
}