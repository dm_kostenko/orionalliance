const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validateContactForm(values) {
  const { name, email, message, agree } = values;

  const errors = {};

  if (!name) {
    errors.name = 'Name is required';
  }

  if (!message || message.length < 50) {
    errors.message = 'Message should contain 50 or more characters';
  }

  if (!email) {
    errors.email = 'Email is required';
  } else if (!EMAIL_REGEX.test(email.toLowerCase())) {
    errors.email = 'Email is incorrect';
  }

  if (!agree) {
    errors.agree = 'You should agree';
  }

  return errors;
}

module.exports = validateContactForm;