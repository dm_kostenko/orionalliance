#!/bin/bash

pid=`ps -ef | grep -v grep | grep PORT=3000 | awk '{print $2}'`
echo "target node PID is $pid"

[[ ! -z $pid ]] && echo "killing node PID" && sudo kill $pid || echo "nothing to kill"

rm -rf .next

#npm ci 

npm run build
