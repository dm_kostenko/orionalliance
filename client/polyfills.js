import smoothscroll from 'smoothscroll-polyfill';
import 'whatwg-fetch';

if (typeof Object.values === 'undefined') {
  require('core-js/modules/es7.object.values');
}

if (typeof Array.prototype.find === 'undefined') {
  require('core-js/modules/es6.array.find');
}

if (typeof String.prototype.repeat === 'undefined') {
  require('core-js/modules/es6.string.repeat');
}

if (typeof Array.from === 'undefined') {
  require('core-js/modules/es6.array.from');
}

if (typeof Object.is === 'undefined') {
  require('core-js/modules/es6.object.is');
}

require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');

if (typeof Number.isNaN === 'undefined') {
  require('core-js/fn/number/is-nan');
}

smoothscroll.polyfill();
