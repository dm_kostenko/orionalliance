# Orion

Web site Orion alliance

## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ```npm install```
5. Then: ```npm run dev```
6. Navigate to `http://localhost:3000/`
7. (optional) Run in terminal `npm i -g cross-env`

## Vacancies

You may edit vacancies in data/vacancies folder. To be published vacancy file should be declared in data/vacancies/index.json