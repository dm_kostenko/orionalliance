
const isProduction = process.env.NODE_ENV === 'production';

const prodUrlPathPrefix = '';
const devUrlPathPrefix = '';
const GA_TRACKING_ID = 'UA-144897615-1';

module.exports = {
  urlPathPrefix: isProduction ? prodUrlPathPrefix : devUrlPathPrefix,
  GA_TRACKING_ID: isProduction ? GA_TRACKING_ID : '',
}