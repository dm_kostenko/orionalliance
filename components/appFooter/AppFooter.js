import React from 'react';
import styled from 'styled-components';

import Copyright from './Copyright';

const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  position: relative;
  width: 100%;
  background-color: ${props => props.theme.footerBackgroundColor};
  color: ${props => props.theme.footerTextColor};
  font-size: 0.8em;
  padding: 0 8vw;

  @media (min-width: 768px) {
    position: fixed;
    z-index: -1000;
    bottom: 0;
  }

  @media (min-width: 992px) {
    padding: 0 21.875vw;
  }
`;

const AppFooter = ({ currentYear }) => (
  <Footer>
    <Copyright currentYear={currentYear} />
  </Footer>
);

export default AppFooter;
