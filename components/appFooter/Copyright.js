import styled from 'styled-components';


//© Orion Alliance. All Rights Reserved.

const CopyrightTextElement = styled.div`
  text-align: center;
  align-self: stretch;
  padding: 2rem 0;
`

const Copyright = ({currentYear}) => (
  <CopyrightTextElement>
    {`© All Rights Reserved. ${currentYear}. Orion Alliance.`}
  </CopyrightTextElement>
)

export default Copyright;