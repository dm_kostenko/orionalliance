import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem 0;
  align-items: center;
  border-bottom: 1px solid ${props => props.theme.dividerColor};

  @media (min-width: 576px) {
    flex-direction: row;
    justify-content: space-between;
    
  }
`

const Address = styled.address`
  margin: 1rem 0;

  @media (min-width: 576px) {
    max-width: 11em;
  }
`

const Phone = styled.a`
  &, &:hover{
    color: ${props => props.theme.footerTextColor};
    text-decoration: underline;
  }
`

const Addresses = () => (
  <Container>
    <Address>
      Bargelaan 200, 2333CW, Leiden, The Netherlands, {' '}
      <Phone href='tel:+31858880256'>
        +31&nbsp;858&nbsp;880&nbsp;256
        </Phone>
    </Address>
    <Address>
      Bruņinieku iela 12, Rīga, LV-1001, Latvia,{' '}
      <Phone href='tel:+37166090579'>
        +371&nbsp;66&nbsp;090&nbsp;579
        </Phone>
    </Address>
    <Address>
      Slaska 53/B506, 81&#8209;304, Gdynia, Poland,{' '}
      <Phone href='tel:+48587435942'>
        +48&nbsp;587&nbsp;435&nbsp;942
      </Phone>
    </Address>
  </Container>
)

export default Addresses;