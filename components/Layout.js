import React from 'react';
import { StickyContainer } from 'react-sticky';
import { ThemeProvider } from 'styled-components';
import { ParallaxProvider } from 'react-scroll-parallax';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import memoize from 'memoize-one';
import debounce from 'lodash/debounce';

import AppHeader from './appHeader';
import AppFooter from './appFooter';
import MainWrapper from './MainWrapper';
import AppPropsContext from './AppPropsContext';
import PageHead from './PageHead';

import { themeBlue } from '../theme';
import WindowEvents, { WindowEventsContext } from './WindowEvents';

const getAppProps = memoize((customAppProps, minWindowWidth, touchDevice) => ({
  ...customAppProps,
  minWindowWidth,
  touchDevice,
}));

const allowBodyScroll = memoize((minWindowWidth, headerExpanded) =>
  minWindowWidth >= 768 ? true : !headerExpanded
);

const calcMinWindowWidth = innerWidth =>
  innerWidth >= 992 ? 992 : innerWidth >= 768 ? 768 : NaN;

const Layout = ({ customAppProps, canonical, children }) => {
  //windowEvents context
  const windowEvents = React.useContext(WindowEventsContext);

  //instance props
  const headerRef = React.useRef();
  //const clickTimeout = React.useRef();
  //const clickSubscription = React.useRef();
  //const mouseoverSubscription = React.useRef();

  //state
  const [headerExpanded, setHeaderExpanded] = React.useState(false);
  const [minWindowWidth, setMinWindowWidth] = React.useState(
    AppPropsContext.defaultAppProps.minWindowWidth
  );
  const [touchDevice, setTouchDevice] = React.useState(
    AppPropsContext.defaultAppProps.touchDevice
  );

  const toggleHeaderExpanded = React.useCallback(() => {
    setHeaderExpanded(!headerExpanded);
  }, [headerExpanded]);

  /*const unsubscribeMouseover = React.useCallback(() => {
    windowEvents.unsubscribe(WindowEvents.RESIZE, resizeSubscription);
  }, [mouseoverSubscription.current]);

  const handleMouseover = React.useCallback(() => {
    clickTimeout.current = setTimeout(() => {
      setTouchDevice(false);
    }, 50);
    debugger;
  }, []);

  const handleClick = React.useCallback(() => {
    clearTimeout(clickTimeout.current);
  }, []);*/

  React.useEffect(() => {
    const handleResize = debounce(() => {
      setMinWindowWidth(calcMinWindowWidth(window.innerWidth));
    }, 100);
    handleResize();

    let clickTimeout;
    let resizeSubscription;
    let clickSubscription;
    let mouseoverSubscription;

    const handleClick = () => {
      clearTimeout(clickTimeout);
      windowEvents.unsubscribe(WindowEvents.MOUSEOVER, mouseoverSubscription);
      windowEvents.unsubscribe(WindowEvents.CLICK, clickSubscription);
    };

    const handleMouseover = () => {
      clickTimeout = setTimeout(() => {
        setTouchDevice(false);
      }, 50);
      windowEvents.unsubscribe(WindowEvents.MOUSEOVER, mouseoverSubscription);
    };

    resizeSubscription = windowEvents.subscribe(
      WindowEvents.RESIZE,
      handleResize
    );

    clickSubscription = windowEvents.subscribe(WindowEvents.CLICK, handleClick);

    mouseoverSubscription = windowEvents.subscribe(
      WindowEvents.MOUSEOVER,
      handleMouseover
    );

    return () => {
      windowEvents.unsubscribe(WindowEvents.RESIZE, resizeSubscription);
      windowEvents.unsubscribe(WindowEvents.MOUSEOVER, mouseoverSubscription);
      windowEvents.unsubscribe(WindowEvents.CLICK, clickSubscription);
      clearTimeout(clickTimeout);
    };
  }, []);

  React.useEffect(() => {
    allowBodyScroll(minWindowWidth, headerExpanded)
      ? enableBodyScroll(headerRef.current)
      : disableBodyScroll(headerRef.current);
  }, [minWindowWidth, headerExpanded, headerRef.current]);

  return (
    <WindowEvents throttleWait={1000}>
      <ThemeProvider theme={themeBlue}>
        <AppPropsContext.Provider
          value={getAppProps(customAppProps, minWindowWidth, touchDevice)}
        >
          <PageHead
            title='Software Development Company | Orion Alliance'
            canonical={canonical}
          />
          <ParallaxProvider>
            <StickyContainer>
              <MainWrapper hideScroll={headerExpanded}>
                <AppHeader
                  key='header'
                  onToggle={toggleHeaderExpanded}
                  expanded={headerExpanded}
                  ref={headerRef}
                />
                {children}
                <AppFooter
                  key='footer'
                  currentYear={customAppProps.currentYear}
                />
              </MainWrapper>
            </StickyContainer>
          </ParallaxProvider>
        </AppPropsContext.Provider>
      </ThemeProvider>
    </WindowEvents>
  );
};

export default Layout;
