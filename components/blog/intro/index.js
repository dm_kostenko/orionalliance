import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/blog_page/h1.jpg'; 
const topText = 'Blog';
// const bottomText = 'Take your business to soaring new heights with custom reliable and secure SaaS, IaaS and PaaS cloud computing solutions from Orion Alliance.'

const props = {
  backgroundImgUrl,
  topText,
  // bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;