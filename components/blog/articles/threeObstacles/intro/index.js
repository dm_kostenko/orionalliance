import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/blog_page/h1.jpg'; 
const topText = '3 Obstacles To Overcome Towards Microservices';
const bottomText = '...any engineering decision is a set of trade-offs'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
  bottomRight: true
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;