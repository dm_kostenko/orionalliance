import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const Image = styled.div`
  background: url(${props => withPathPrefix(props.url)}) center;
  background-repeat: no-repeat;
  margin: auto;
  background-size: calc(25vw + 15rem) calc(17vw + 7rem);
  height: calc(25vw + 10rem);
  width: calc(33vw + 15rem);                                                                                        
`;

const BoldText = styled.span`
  color: ${props => props.theme.elementColor};
  font-weight: 700;
`

const Text = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding textIndent>
      Microservices architecture is gaining popularity and is being employed in a great number of software projects. 
      The reason for this is that microservices provide a cohesive set of functionalities and bring important business benefits.
    </P>
    <P noTopPadding textIndent>
      However, let’s not forget that any engineering decision is a set of trade-offs. 
      The difference between a good engineering decision and an ordinary one consists of the optimal balance of compromises. 
      The more universal the decision is, the less effective it is in the general case. This is true with software architecture as well. 
      Significant part of overall system architecture is needed to carefully weigh all pros and cons. 
      Today we are going to discuss three main obstacles on the path of implementing microservices pattern.
    </P>
    <P noTopPadding textIndent>
      The first one – <BoldText>complexity</BoldText>. In a monolithic system, there is only one or a few components to manage. 
      When the microservice architecture has significant number of components, that greatly complicates the development and deployment processes. 
      As a result it will cost time and money. In addition to the deployment complexity, 
      a determining of where the code operates also becomes a cause of increased complexity and costs for system maintenance as a whole.  
      If your way to production is not a simple case of packaging and deployment, 
      but instead a complicated process, walk lightly with microservices, unless you are willing to update your process.
    </P>
    <Image url='/img/blog_page/three_obstacles/image1.png' />
    <P noTopPadding textIndent>
      Another obstacle to overcome is the so-called <BoldText>tax of distribution</BoldText>. 
      In microservices architecture there may be delays associated with the interaction of individual microservices with each other. 
      Finally, the load on the network as whole increases total latency of individual calls across the network.
    </P>
    <Image url='/img/blog_page/three_obstacles/image2.png' />
    <P noTopPadding>
      Worth noticing is <BoldText>reduction of reliability</BoldText>. When the number of simple components of microservices increases, 
      there is a risk of transferring unreliability of a single component to the area of overall system ​​interaction between multiple components. 
      In other words, if one microservice is sick, it reflects on client calls. 
      And evaluation of core services and determination of reliability becomes a matter of the first importance.
    </P>
    <P noTopPadding textIndent>
      In any case if you are looking from the angle if microservices can address all your pain point, 
      or very sceptical about benefits microservices can provide. 
      Please keep in mind that microservices is just an architectural pattern which addresses isolated set of problems and 
      it can be as great as solving most of pain points and allow your organization to focus on product as well 
      as create nightmare without clear benefits to the users and developers of your product.
    </P>
  </BaseContentSection>
)

export default Text;
