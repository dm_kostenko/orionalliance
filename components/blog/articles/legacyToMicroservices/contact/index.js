import Contact from 'components/contact';

const headerProps = {
  text2: 'Contact Us',
  reverse: true,
}

const text = 'Contact us  for information on how our software can help your business grow, be more productive and profitable.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}    
  />
)