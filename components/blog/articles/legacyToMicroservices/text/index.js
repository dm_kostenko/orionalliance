import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const Image = styled.div`
  background: url(${props => withPathPrefix(props.url)}) center;
  background-repeat: no-repeat;
  margin: auto;
  margin-bottom: 2rem;
  margin-top: 2rem;
  background-size: calc(33vw + 15rem) calc(25vw + 10rem);
  height: calc(25vw + 10rem);
  width: calc(33vw + 15rem);                                                                                        
`;

const Text = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding textIndent>
      More recently in modern software development community we are hearing that a lot of software modernization efforts fail after 2-3 years 
      and large investments, and this article is trying to address common patterns between those failures. 
      Most of the projects we found start from a big idea of improving an existing business through implementing brand new system 
      which will satisfy modern scalability, flexibility, provide all new features and frameworks and will not have any drawbacks. 
      But after years and millions of investments it becomes a dramatic ending to the project, and in some cases pretty large organization 
      and business reorganization are required... 
    </P>
    <P noTopPadding textIndent>
      It is worth taking a brief look at where it has come from, remembering the keystone of software evolution. 
      As far back as 1974 Meir Lehman and László Bélády had been formulating eight laws of software evolution. 
      Today these series of laws seem obvious. For example, the Law of Declining Quality states that the quality of a 
      system will appear to be declining unless it is rigorously maintained and adapted to operational environment changes. 
      And the Law of “Continuing Change” says that a system must be continually adapted or it becomes progressively less satisfactory. 
      Nevertheless, today their statements serve as a guidance that help us define a software evolution process for legacy systems and a way 
      to improve and modernize existing application without dramatic and costly change.
    </P>
    <P noTopPadding textIndent>
      And as it happened, a small Internet cinema company (due to become our client) was hard-pressed to withstand the strain. 
      The number of requests were growing and users received operating delays and system crash. 
      The monolithic system was developed for a small audience and was able to process only about 100 rps.
    </P>
    <Image url='/img/blog_page/legacy_to_microservices/image1.png' />
    <P noTopPadding textIndent>
      In general, we understood that the legacy system was necessary to be changed. 
      Based on the results of the analysis and laws of software evolution, 
      it was decided to modernize the Internet cinema platform through the use of modern scaling methods.
    </P>
    <P noTopPadding textIndent>
      As even in a monolithic system, it was possible to subselect logically separate parts, 
      the choice turned next to microservice pattern with horizontal scaling of individual components. 
      Organizing components in the form of microservices, each part of the system is independently scalable and load balanced. 
      When new content is released and user demand increases, the playback options or any other component can be scaled 
      to increase the load without each component scaling. It may increase system performance and reduce the number of failures.
    </P>
    <Image url='/img/blog_page/legacy_to_microservices/image2.png' />
    <P noTopPadding>
      To sum up, after migration to microservices and problem parts scaling, the performance has increased up to 1000 rps. 
      The previous development process organization was based on a pool of virtual machines managed by kvm on 
      the company's servers where each microservice was located. 
      As a result, after the scaling and the migration to the new system organization, the performance increases 10 times on the same servers.
    </P>
    <P noTopPadding textIndent>
      The client was ready to spend plenty of time and money but our team managed to solve the problem within a short time and at a reasonable price. 
      Our work further demonstrates one concept: software applications are always required to be maintained and improved. 
      There will always be a need to implement new requirements and to enhance existing features.
    </P>
  </BaseContentSection>
)

export default Text;
