import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/blog_page/h1.jpg'; 
const topText = 'Legacy To Microservices';
const bottomText = 'Based on the results of the analysis and laws of software evolution, it was decided to modernize the Internet cinema platform through the use of modern scaling methods.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;