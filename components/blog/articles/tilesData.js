export default [
  {
    imgUrl: '/img/blog_page/legacy_to_microservices/b_1.jpg',
    heading: 'Legacy to microservices',
    text: 'Orion Alliance shares experience: from monolithic architecture to microservices. We tell what it was like.',
    href: '/legacy-to-microservices'
  },
  {
    imgUrl: '/img/blog_page/three_obstacles/b_1.jpg',
    heading: '3 obstacles to overcome towards microservices',
    text: 'We’ll be continuing with microservices. Is it always right solution? Deal with it together. ',
    href: '/three-obstacles-to-overcome-towards-microservices'
  }
]