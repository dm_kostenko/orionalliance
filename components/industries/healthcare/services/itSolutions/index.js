import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';

import Exchange from './Exchange';
import Record from './Record';
import Telehealth from './Telehealth';

const Section = styled.div`
  padding-top: 3rem;
`



const ItSolutions = () => (
  <Section>
    <BaseSectionHeader
      text2='Medical IT'
      text1='Solutions'
      reverse
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      Orion Alliance takes advantage of industry-leading IT solutions to empower healthcare providers with more convenient, scalable and efficient ways of communicating with staff and patients while keeping tabs on medical records, test results, personal data, medical equipment status and much more.
    </P>
    <P>
      Learn more about the kind of medical software solutions you can make part of your healthcare facility
    </P>
    <Exchange />
    <Record />
    <Telehealth />
  </Section>
)

export default ItSolutions;