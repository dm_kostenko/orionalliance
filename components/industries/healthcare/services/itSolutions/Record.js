import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Development and integration of EHR applications to record key info such as demographics, medical history, past appointments, major illnesses and surgeries, allergies, chronic diseases and history of family illness',
  'Cloud-based mobile PHR applications for interconnectivity with EHR devices and medical experts, insurance companies and payer systems',
  'Data migration from legacy systems',
  'Identity management and Role-Based Access Control (RBAC) systems',
  'Data analysis of patients’ medical history, demographic, vitals, allergies, immunizations and lab results'
]

const Record = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/healthcare/i2.png'
      heading='Electronic/Personal Health Record (EHR/PHR)'
    />
    <P>
      We can develop custom electronic and personal health record applications that allow patients and staff to access, manage and track their health in a secure, convenient and flexible environment. 
    </P>
    <P>
      Whether for a tablet or mobile device, we can develop a medical IT service that’s compatible with the desired platform and can be easily scaled to provide security updates, extra features and services, UI redesigns and performance enhancements.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Record;