import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Exchange data between service providers using WEB/Mobile applications',
  'Health Level 7 (HL&) International standards support',
  'Data encryption, single sign-on, role-base access controls to secure sensitive data',
  'Document exchange software'
]




const Exchange = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/healthcare/i1.png'
      heading='Health Information Exchange'
    />
    <P>
      We can develop convenient applications that allow patients and medical experts to appropriately access and share secure information – even between healthcare providers. 
    </P>
    <P>
      From medical history to appointment records and prescribed medication, we design intelligent apps that comply with the Continuity of Care Documents (CCD) and American Society for Testing and Materials (ASTM) so sensitive data is handled with care and accessed by approved personnel.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Exchange;