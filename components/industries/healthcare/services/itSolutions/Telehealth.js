import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Telehealth medicine charts, digital conference solutions and applications for telemedicine peripherals',
  'Real-time data gathering from monitoring systems and storing into databases (HER/EMR/PHR)',
  'Custom web applications for patients to manage medical history, lab requests and results',
  'Software development for scheduling, managing appointments and invoicing customers for doctors’ visits and lab results'
]

const Telehealth = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/healthcare/i3.png'
      heading='Telehealth'
    />
    <P>
      For patients who live in remote locations and cannot gain immediate access to onsite medical care. Telehealth is an effective way to provide quality care via telecommunication technology, without the patient having to travel long distances to get specialist advice. 
    </P>
    <P>
      Orion Alliance's designs and implements Telehealth solutions for popular platforms such as iOS, Android and Windows, which are compatible with certain medical devices and telemedicine platforms.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Telehealth;