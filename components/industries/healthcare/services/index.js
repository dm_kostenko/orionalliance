
import BaseContentSection from 'elementals/BaseContentSection';
import Better from './Better';
import ItSolutions from './itSolutions';

const Choose = () => (
  <BaseContentSection lightGray>
    <Better />
    <ItSolutions />
  </BaseContentSection>
)

export default Choose;