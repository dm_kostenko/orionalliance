import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';


const Section = styled.div`

`








const Migration = () => (
  <Section>
    <BaseSectionHeader
      text2='Better Technology, '
      text1='Better Care'
      reverse
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Our specialists provides a range of medical IT services for Clinics, Hospitals, Assisted Living Facilities and Specialist Healthcare Providers. 
    </P>
    <P>
      From Mobile Personal Health Records to Identity Management Solutions, Document Exchange Software and Telemedicine Platforms – we expertly manage your IT needs to deliver positive outcomes for staff and patients. 
    </P>
    <P>
      Our friendly and experienced team will take the time to address your specific needs and find a solution that gets the results you desire. No matter how simple or complex the job is, we are ready to help you.
    </P>
  </Section>
)

export default Migration;