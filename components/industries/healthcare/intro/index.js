import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/industries_page/healthcare/intro_bg.jpg'; 
const topText = 'Medical IT Services and Support';
const bottomText = 'Orion Alliance provides a range of medical IT services to improve patient incomes, support remote medical delivery, and streamline internal business processes.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;