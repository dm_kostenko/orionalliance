import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';




const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      When it comes to managing people’s health, there is no room for technical errors.
    </P>
    <P>
      That’s why for the last 10 years, the healthcare industry has turned to cloud-based technology, mobile applications and bespoke IT solutions for medical devices. This way providers can improve patient outcomes, provide better remote care and easily track patients’ medical history.
    </P>
    <P>
      Of course, you still need essential medical IT systems to run a successful practice. Whether it be an online booking system, Electronic Medical Record (EMR) or medical billing platform – Orion Alliance has you covered. 
    </P>
  </BaseContentSection>
)

export default Description;
