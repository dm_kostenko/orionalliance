import Contact from 'components/contact';


const headerProps = {
  text2: 'contact Us',
  reverse: true,
}

const text = 'Let us know if you have any questions about our medical IT services and solutions.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)