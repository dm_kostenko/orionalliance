import Contact from 'components/contact';

const headerProps = {
  text2: 'Contact Us',
  reverse: true,
}

const text = 'Find out how Orion Alliance can enhance the quality of your banking system with a custom application solution.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)