import styled from 'styled-components';

import P from 'elementals/P';
import BaseContentSection from 'elementals/BaseContentSection';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      With the rise of mobile connectivity, emerging cyber-security 
      threats and personal data breaches, and adopting of cloud-based 
      technology. The banking, financial and insurance sector is submerged 
      in a whirlwind of social and technological changes on a scale 
      never seen before.
    </P>
    <P>
      To stay ahead of the competition, instil trust in a sceptic 
      consumer-base and keep up with the latest financial technology, 
      you need to partner with a company who understands the unique 
      challenges of this fast-paced industry.
    </P>
    <P>
      For over 10 years Orion Alliance has been a preferred partner 
      and service provider for some of the world’s leading financial 
      institutes. From internet and mobile banking solutions to bank 
      management software, automated solutions and custom payment 
      systems – we deliver world-class results across the financial sector.
    </P>
    <P>
      Whether you need to upgrade your current infrastructure or develop 
      a new system from the ground-up, you get ongoing support from a team 
      of 100+ strong industry experts who have the skills and capabilities 
      to meet your business needs and objectives.
    </P>
  </BaseContentSection>
)

export default Description;