import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/industries_page/banking/intro_bg.jpg'; 
const topText = 'Software Solutions for the Banking, Financial ' 
  + 'and Insurance Sector';
const bottomText = 'For over 10 years our company has provided ' 
  + 'the Banking, Financial and Insurance sector with innovative, ' 
  + 'flexible and scalable software solutions';

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;