import styled from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ParallaxWrapper from 'elementals/ParallaxWrapper';
import withPathPrefix from 'utils/withPathPrefix';

import tilesData from './tilesData';
import BaseTile from './BaseTile';

const Section = styled(BaseSection)`
  padding-top: 3rem;
  z-index: 10;
`;

const TopContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap-reverse;
`;

const TopItem = styled.div`
  flex: 0 0 100%;

  padding-right: 3rem;
  @media (min-width: 768px) {
    height: auto;
    flex-basis: 50%;
  }
`;

const TopItemImgContainer = styled(TopItem)`
  display: none;
  @media (min-width: 768px) {
    display: block;
    position: absolute;
    height: 32rem;
    top: -1.5rem;
    right: 0;
    left: 50%;
    overflow: hidden;
  }
  @media (min-width: 992px) {
    height: 30rem;
  }
`;

const TopItemImg = styled.div`
  display: block;
  position: absolute;
  top: -50%;
  width: 100%;
  height: 200%;
  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center;
  background-size: cover;
`;

const TilesContainer = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  top: 1.5rem;
  &::after {
    content: '';
    position: absolute;
    z-index: -10;
    bottom: 1rem;
    left: 2rem;
    right: 2rem;
    box-shadow: 0 0 12rem 2rem #000;
  }
`;

const Choose = () => (
  <Section>
    <TopContainer>
      <TopItem>
        <BaseSectionHeader
          text2='Why Choose Orion Alliance'
          wrapText
          textAlign='left'
        />
        <HeaderSeparator />
        <P>
          It’s simple. Orion Alliance works with you from the very early stages
          to define the purpose of your software solution, how it benefits the
          end-user, and what you hope to achieve in the long-term.
        </P>
        <P>
          Whether your desire is to boost productivity, improve the customer
          experience, stay compliant with strict industry or government
          regulation – we offer a range of solutions to meet your needs.
        </P>
      </TopItem>
      <TopItemImgContainer>
        <ParallaxWrapper
          offsetYMin='-50%'
          offsetYMax='50%'
          styleOuter={{
            position: 'relative',
            height: '100%',
          }}
          styleInner={{
            position: 'relative',
            height: '100%',
          }}
        >
          <TopItemImg url='/img/industries_page/banking/bg.jpg' />
        </ParallaxWrapper>
      </TopItemImgContainer>
    </TopContainer>
    <TilesContainer>
      {tilesData.map((props, index) => (
        <BaseTile key={index} index={index} {...props} />
      ))}
    </TilesContainer>
  </Section>
);

export default Choose;
