import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';
import H6 from 'elementals/H3';
import P from 'elementals/P';
import TileIcon from 'elementals/TileIcon';

const BaseTileElement = styled.div`
  flex: 1 0 100%;
  display: flex;
  flex-direction: column;
  align-items:stretch;
  padding: 3rem;
  background-color: ${props => props.theme.chooseTilesColors[props.index]};
  
  @media (min-width: 768px) {
    align-items:stretch;
    flex: 1 0 45%;
  }
  @media (min-width: 992px) {
    align-items:stretch;
    flex: 1 0 26%;
  }
`

const HeadingElement = styled.div`
  display: flex;
  align-items: center;
  @media (min-width: 768px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const ButtonElement = styled(TileIcon)`
  margin: 0;
  background-image: url(${props => withPathPrefix(props.imgUrl)});
  background-position: center center;
  background-repeat: no-repeat;
  height: 6rem;
  padding: 0 3rem;
  margin-right: 2rem;
`

const HeaderElement = styled(H6)`
  text-transform: uppercase;
  font-weight: 700;
  color: ${props => props.theme.elementColor};

  @media (min-width: 768px) {
    margin-top: 1.5rem;
  }
`

const TextElement = styled(P)`
  color: ${props => props.theme.textColorGray};
`

const BaseTile = ({imgUrl, heading, text, index}) => (
  <BaseTileElement index={index}>
    <HeadingElement>
      <ButtonElement
        imgUrl={imgUrl}
      />
      <HeaderElement>{heading}</HeaderElement>
    </HeadingElement>
    <TextElement>
      {text}
    </TextElement>
  </BaseTileElement>
)

export default BaseTile;