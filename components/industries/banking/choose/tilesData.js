export default [
  {
    imgUrl: '/img/industries_page/banking/t_fast.png',
    heading: 'Fast Delivery Times',
    text: 'Our team of industry experts’ work around-the-clock to complete ' + 
      'the specified tasks and meet key objectives within a precise schedule ' + 
      'to ensure your product is delivered on time and on-budget with minimal fuss.',
  },
  {
    imgUrl: '/img/industries_page/banking/t_security.png',
    heading: 'Advanced Security',
    text: 'Protect your customers from potential security threats, acts of ' + 
      'fraud, and personal data breaches with the latest online banking security ' + 
      'systems. We can apply a range of security measures such as multi-factor ' + 
      'authentication, firewalls, fraud detection systems, automatic time-outs ' + 
      'and 128-bit SSL encryption technology.',
  },
  {
    imgUrl: '/img/industries_page/banking/t_support.png',
    heading: 'Post-Release Support',
    text: 'Even after your application has left the door we can keep you up-to-date ' + 
      'with regular maintenance, patches, bug fixes and performance enhancements.',
  },
  {
    imgUrl: '/img/industries_page/banking/t_pm.png',
    heading: 'Project Management',
    text: 'From start to end, we will keep you in the loop and ensure the ' + 
      'development cycle is carried out based on your unique project requirements. ' + 
      'We understand just how fast the financial sector moves. That’s why we stick ' + 
      'to a fixed schedule for each task and allow you the freedom to request ' + 
      'changes in the event of new industry guidelines, government regulation ' + 
      'and legislation.',
  },
  {
    imgUrl: '/img/industries_page/banking/t_qa.png',
    heading: 'Quality Assurance',
    text: 'With over 10 years of experience Orion Alliance has perfected the ' + 
      'art and science of software quality assurance. Our approach involves ' + 
      'reviewing all aspects of the software that define the user-experience ' + 
      'from performance to UI design, mobile capability and flexibility. By ' + 
      'stress testing the software under different conditions, it’s fine-tuned ' + 
      'to deliver a quality experience under all reasonable circumstances.',
  },
]