import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';
import P from 'elementals/P';
import IconedHeading from 'elementals/IconedHeading';

const BaseTileElement = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 3rem 0 0;
`

const TileIconedHeading = styled(IconedHeading)`
  flex: 1 0 100%;
`

const DescriptionElement = styled.div`
  flex: 1 0 100%;
  margin: 1rem 0 2rem 0;
  @media (min-width: 768px) {
    flex: 1 0 35%;
    padding-right: 3rem;
  }
`

const ListElement = styled.ul`
  flex: 1 0 100%;
  align-self: flex-start;
  list-style: none;
  padding: 2rem 3.25rem 2rem 3rem;
  margin-top: 1rem;
  color: ${props => props.theme.textColor};
  font-style: italic;
  background-color: ${props => props.theme.indBankingServicesListBackgroundColor};
  @media (min-width: 768px) {
    flex: 1 0 35%;
  }
  @media (min-width: 992px) {
    padding-right: 7rem;
  }
`


const ListItemElement = styled.li`
  &:not(:first-child) {
    margin-top: 1rem;
  }
  padding-left: 2rem;
  background: url(${withPathPrefix('/img/industries_page/banking/ul_icon.png')}) no-repeat left top;
  background-size: 1rem 1rem;
  background-position-y: 0.3rem;
  font-weight: 700;
`

const BaseTile = ({
  iconSrc,
  heading,
  descriptions,
  keyItems,
}) => (
  <BaseTileElement>
    <TileIconedHeading 
      heading={heading}
      iconSrc={iconSrc}
    />
    <DescriptionElement>
      {
        descriptions.map((desc, index) => <P key={index}>{desc}</P>)
      }
    </DescriptionElement>
    <ListElement>
      {
        keyItems.map((item, index) => <ListItemElement key={index}>{item}</ListItemElement>)
      }
    </ListElement>
  </BaseTileElement>
)

export default BaseTile;