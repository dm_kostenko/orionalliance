import styled from 'styled-components';
import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';

import ServicesBaseTile from './ServicesBaseTile';
import tilesData from './tilesData';
import BaseContentSection from 'elementals/BaseContentSection';



const SectionHeader = styled(BaseSectionHeader)`
  max-width: 90%;
  font-weight: 900;
  & > span {
    text-align: left;
  }
`

const Services = () => (
  <BaseContentSection lightGray>
    <SectionHeader
      text2='Banking, Financial and Insurance Software Services'
      wrapText
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Orion Alliance offers a variety of turn-key solutions for 
      banking and financial service providers.
    </P>
    {
      tilesData.map((item, index) => <ServicesBaseTile key={index} {...item}/>)
    }
  </BaseContentSection>
)

export default Services;