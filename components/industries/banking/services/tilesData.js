export default [
  {
    iconSrc: '/img/industries_page/banking/t_ico_software.png',
    heading: 'Custom Payment Software',
    descriptions: [
      'Allow your customers to pay for what they want – anytime, anywhere – in a ' + 
      'convenient, secure and reliable online payment system.',

      'Powered by 128-bit encryption with multi-currency support and automated ' + 
      'record-keeping, your customers can buy with confidence and you’ll know ' + 
      'exactly when, what and where an online transaction occurs.',

      'Without having to change your IT infrastructure, we can easily ' + 
      'implement WorldPay, Google Checkout or PayPal into your existing workflow.',
    ],
    keyItems: [
      'PCI-DSS, PA-DSS, EMV, Check-21 and other industry-standard payment options',
      'Intuitive eCommerce payment integration',
      'Support for multiple payment options like Visa, MasterCard and PayPal',
      'Self-service customer management portals',
    ]
  },
  {
    iconSrc: '/img/industries_page/banking/t_ico_doc_manage.png',
    heading: 'Bank Document Management',
    descriptions: [
      'Take the stress out of dealing with busy paperwork like forms, documents, ' + 
      'bank statements, payslips, invoices and ledgers. Stay compliant with data ' + 
      'privacy laws and record-keeping regulations with an all-in-one document management system.',

      'With Orion Alliance you can consolidate all of your crucial banking ' + 
      'documents into a secure, well-organised and easy to use online platform. ' + 
      'By using the latest cloud-based technology, we can build, design and ' + 
      'implement a custom architecture that makes it easy for staff to upload, ' + 
      'manage and access all kinds of documents.',

      'From digitizing paper records to inputting advanced security measures ' + 
      'and folder organisation – we do it all.',
    ],
    keyItems: [
      'Mobile/WEB banking app development',
      'Accounting and financial management',
      'Billing and invoicing solutions',
      'Integration to existing ERP and accounting systems.',
    ]
  },
  {
    iconSrc: '/img/industries_page/banking/t_ico_inet_bank.png',
    heading: 'Internet and Mobile Banking',
    descriptions: [
      'These days it has become standard to give your customers the freedom to ' + 
      'do banking on the go. Whether it’s to transfer money, take control of ' + 
      'your finances or renew an insurance policy – the possibilities grow each year.',

      'For over 10 years Orion Alliance has led the way in internet and mobile ' + 
      'banking by providing a range of secure, convenient and reliable applications. ' + 
      'Our aim is to put the power back into the consumer and make their lives easier ' + 
      'while ensuring their personal data is secure. So as mobile becomes the preferred ' + 
      'way to engage with customers, Orion Alliance can develop a custom mobile app to ' + 
      'satisfy your business needs.',

      'Orion Alliance also specialise in the custom integration of digital wallets, ' + 
      'which include a range of different currency and token options.',
    ],
    keyItems: [
      'E-wallet development and integration',
      'End-to-end/Point-to-point encryption',
      'NFC standard integration',
    ]
  },
]