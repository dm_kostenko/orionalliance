import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';
import styled from 'styled-components';
import IntroSectionSimpleBanner from 'elementals/introSection/IntroSectionSimpleBanner';
import Bold from 'elementals/Bold';


const backgroundImgUrl = '/img/industries_page/oil_and_gas/intro_bg.jpg';
const bottomText = 'Orion Alliance’s bespoke software solutions help the oil & gas industry improve supply chain efficiency, worksite safety and risk mitigation.'

const props = {
  backgroundImgUrl,
  bottomText,
}

const BannerH2 = styled(IntroSectionSimpleBanner.BannerH2)`
  max-width: 84vw;
  font-weight: 300;
`

const Banner = (props) => {
  const { bottomText } = props;

  return (
    <IntroSectionSimpleBanner.Banner>
      <BannerH2>
        <Bold>Enterprise Software Solutions </Bold>|<br/>Oil and Gas Industry
      </BannerH2>
      <IntroSectionSimpleBanner.BannerH6>
        {bottomText}
      </IntroSectionSimpleBanner.BannerH6>
    </IntroSectionSimpleBanner.Banner>
  )
}

const Intro = () => (
  <IntroSectionSimple {...props}>
    {
      ({ topText, bottomText }) =>
        <Banner
          topText={topText}
          bottomText={bottomText}
        />
    }
  </IntroSectionSimple>
)

export default Intro;