import Contact from 'components/contact';

 
const headerProps = {
  text1: ' With the Latest Digital Solutions',
  text2: 'Better Manage Your Onshore and Offshore Facilities',
  reverse: true,
  wrapText: true,
  
}

const text = 'Contact us to discuss your project requirements with experienced developer today.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)