import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import styled from 'styled-components';

const items = [
  'Real-time pipeline tracking system for pressure and metering auditing mitigating risks to safeguard your personnel and asset',
  'Minimize operational costs and enhance capacity scheduling and optimizing of pipeline',
  'Software development including pipeline management services, interactive dashboards, systems integrations and legacy migration',
  'Trade execution acceleration and mitigation of operational and regulatory risks'
]

const StyledList = styled(ULSimple)`
  @media (min-width: 992px) {
    padding-right: 7rem;
  }
`

const Midstream = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/oil_and_gas/i2.png'
      heading='Midstream Services'
    />
    <StyledList items={items}/>
  </Article>
)

export default Midstream;