import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Software development for managing upstream processes',
  'Workflow optimization, data storing and real-time analysis',
  'Monitoring and control systems using an integrated SCADA',
  'Mobile development for real-time tracking of GPS data, well and equipment histories and life-cycles',
  'Real-time report systems for forecasting'
]

const Upstream = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/oil_and_gas/i1.png'
      heading='Upstream Services'
    />
    <ULSimple items={items}/>
  </Article>
)

export default Upstream;