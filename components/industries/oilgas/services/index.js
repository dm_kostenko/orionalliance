import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import BaseContentSection from 'elementals/BaseContentSection';
import Upstream from './Upstream';
import Midstream from './Midstream';
import Downstream from './Downstream';


const Services = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Bespoke'
      text1='Digital Solutions'
      reverse
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      From Enterprise Resource Planning (ERP) to Asset Management, Risk Mitigation and Real-Time Data Tracking, we provide all of the software solutions you need to be more efficient and leap ahead of the competition.
    </P>
    <P>
      Regardless of your current operational setup, it’s easy to incorporate the latest technology into your workflow, without compromising your existing legacy system or valuable customer data. 
    </P>
    <P>
      Rest assured you’re guided each step of the way – from consultation to testing and deployment – and you receive regular updates from our friendly and supportive team. So you can be confident the development cycle will be simple and stress-free.
    </P>
    <Upstream />
    <Midstream />
    <Downstream />
  </BaseContentSection>
)

export default Services;