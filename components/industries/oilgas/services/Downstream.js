import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Development of robust supply chain and trading management systems',
  'Development of powerful Energy Reading and Risk Management systems',
  'Document and retail management systems including invoicing '
]


const Downstream = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/oil_and_gas/i3.png'
      heading='Downstream Services'
    />
    <ULSimple items={items}/>
  </Article>
)

export default Downstream;