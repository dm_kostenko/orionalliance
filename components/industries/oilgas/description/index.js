import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Whether you run an onshore or offshore site, our team offers an array of bespoke software solutions designed to help you improve operational efficiency and worksite safety, reduce environmental impact, mitigate risk, and gain a competitive edge on the market. 
    </P>
    <P>
      While there are many unique challenges facing the oil and gas industry – from global warming to tightening budgets and stringent government regulations – the power of cloud technology, automation, and real-time data analysis makes it easy to efficiently manage your site – even in remote locations. 
    </P>
    <P>
      No matter the size or complexity of your supply chain, you can trust in our highly skilled team to focus on the unique challenges you face and supply a cost-effective solution that meets your needs and budget.
    </P>
  </BaseContentSection>
)

export default Description;
