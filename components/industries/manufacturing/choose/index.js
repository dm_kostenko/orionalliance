import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const Section = styled(BaseContentSection)`
  z-index: 10;
`

const items = [
  'Visualise the bigger picture among your global supply chain and outsourced business partners',
  'Streamline existing internal processes and business practices',
  'Improve stock inventory management', 
  'Implement Internet of Things (IoT) devices',
  'Migrate legacy systems to newer IT infrastructures',
  'BPM application development, testing and management',
  'Reduce order processing time to boost customer loyalty',
  'Quickly and easily create electronic quotes, pay and send invoices',
  'Define, monitor and assess measurable KPIs in real-time',
  'Automate collection and collation of customer data to predict future market trends and meet customer expectations',
  'Schedule maintenance appointments for equipment, tools and machinery'
]


const Choose = () => [
  <Section key='1' lightGray>
    <BaseSectionHeader
      text1='Advantage'
      text2='The Orion Alliance'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      For over 10 years we have built strong long-term relationships with small and large industrial manufacturing enterprises. We are committed to the complete satisfaction of our customers and work closely with you to achieve your business objectives within a fixed budget and timeframe.
    </P>
    <P>
      Our 100+ strong team of software developers understand the unique challenges of the manufacturing industry and will work closely with you to provide solutions that delivery measurable outcomes whether it be increased productivity, security and outsourcing of business practices.
    </P>
  </Section>,
  <Section key='2'>
    <BaseSectionHeader
      text1='Services'
      text2='Industrial Manufacturing'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Orion Alliance is recognised as an industry leader in the development of bespoke industrial applications.
    </P>
    <P>
      Rest assured we take a caring and personal approach to understanding your needs. Our team will assess your current internal processes and identify key areas that you wish to address. From there, we help you choose the right software solutions, IT tools and infrastructure that best supports your business goals and objectives.
    </P>
    <P>
      Below are the following advantages of upgrading your IT systems:
    </P>
    <ULSimple items={items}/>
  </Section>
]

export default Choose;