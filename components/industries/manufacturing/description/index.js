import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Orion Alliance provides a range of software applications to cover all aspects of the industrial manufacturing industry. From Big Data Analysis to Automation, Quality Control, Ground Floor and Supply Chain Management – our goal is to drive business growth, productivity and customer satisfaction.
    </P>
  </BaseContentSection>
)

export default Description;
