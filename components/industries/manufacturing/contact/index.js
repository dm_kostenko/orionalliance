import Contact from 'components/contact';

const headerProps = {
  text2: 'Contact US',
  reverse: true,
}

 



const text = 'To learn more about how we can drive your business towards success, contact us today.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)