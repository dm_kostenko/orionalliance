import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';



const backgroundImgUrl = '/img/industries_page/manufacturing/intro_bg.jpg'; 
const topText = 'Industrial Manufacturing Software';
const bottomText = 'Orion Alliance specialise in all aspects of industrial manufacture software development from Big Data Analysis to Automation, IoT and Product Delivery'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;