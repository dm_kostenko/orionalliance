import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'Gift Card Redemption, Gift Registry, Plus Promotions and Discounts',
  'Integration With Peer-To-Peer Platforms, Social Networks and Group Buying Platforms',
  'Analysis User Profiles, Location, History and Behaviour to Give Discounts'
]










const Loyality = ({noTopPadding}) => (
  <Article noTopPadding={noTopPadding}>
    <IconedHeading
      iconSrc='/img/industries_page/retail_software/i3.png'
      heading='Loyalty Program Management'
    />
    <P>
      Offer your customers lucrative loyalty programs. Track their spending habits and reward them with tailored discounts, offers and prizes. With our automated data mining and processing, it’s easy to keep track of repeat-customers and provide them with valuable incentives so they remain loyal.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Loyality;