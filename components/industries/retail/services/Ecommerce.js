import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'E-Commerce Shopping Carts and Hosted Checkout Solutions',
  'Integration for Google Wallet, Apple Pay, Android Pay and Other Payment Systems',
  'Easy to Use Data Mining, Tracking and Visualisation Software',
  'Custom Mobile Applications'
]

const Ecommerce = ({noTopPadding}) => (
  <Article noTopPadding={noTopPadding}>
    <IconedHeading
      iconSrc='/img/industries_page/retail_software/i1.png'
      heading='E-Commerce Solutions'
    />
    <P>
      Competition in the online retail space is growing. Consumers of today expect to be able to browse and purchase the items they want quickly, easily and efficiently. Especially when they’re away from their desk and on-the-go.
    </P>
    <P>
      Orion Alliance can implement retail software solutions and automated processes so you can quickly localise prices, streamline the checkout process, and give customers the freedom to purchase in their preferred currency and platform.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Ecommerce;