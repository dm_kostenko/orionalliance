import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'Back-Office Portals With Role-Based Access Controls for Analytics and Reporting',
  'Integration With Existing ERP Systems',
  'SSL/TLS Encryptions and Firewall Protections'
]







const Backend = ({noTopPadding}) => (
  <Article noTopPadding={noTopPadding}>
    <IconedHeading
      iconSrc='/img/industries_page/retail_software/i2.png'
      heading='Back End Operations and Security'
    />
    <P>
      Keep track of your back end operations with easy and intuitive software. 
    </P>
    <P>
      From managing stock inventory to recording transaction history, supply chain logistics and custom shipping solutions – we have you covered. 
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Backend;