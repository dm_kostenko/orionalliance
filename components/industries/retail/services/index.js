import BaseContentSection from 'elementals/BaseContentSection';
import Ecommerce from './Ecommerce';
import Backend from './Backend';
import Loyality from './Loyality';


const Services = () => (
  <BaseContentSection lightGray>
    <Ecommerce noTopPadding/>
    <Backend />
    <Loyality />
  </BaseContentSection>
)

export default Services;