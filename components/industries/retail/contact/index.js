import Contact from 'components/contact';


const headerProps = {
  text1: 'Retail Business',
  text2: 'Expand Your',
  reverse: true,
}

const text = 'On top of all of these fantastic features, we are available 24/7 to provide immediate technical support when you need it most.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)