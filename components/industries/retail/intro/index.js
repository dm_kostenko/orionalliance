import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';
import styled from 'styled-components';
import IntroSectionSimpleBanner from 'elementals/introSection/IntroSectionSimpleBanner';
import Bold from 'elementals/Bold';


const backgroundImgUrl = '/img/industries_page/retail_software/intro_bg.jpg';
const bottomText = 'Orion Alliance’s software solutions enhance business growth and customer experiences. From Checkout to Stock Control and Loyalty Programs – you get it all.'

const props = {
  backgroundImgUrl,
  bottomText,
}

const BannerH2 = styled(IntroSectionSimpleBanner.BannerH2)`
  max-width: 84vw;
  font-weight: 300;
`

const Banner = (props) => {
  const { bottomText } = props;

  return (
    <IntroSectionSimpleBanner.Banner>
      <BannerH2>
        <Bold>Retail Software Solutions </Bold>|<br />Instore and Online
      </BannerH2>
      <IntroSectionSimpleBanner.BannerH6>
        {bottomText}
      </IntroSectionSimpleBanner.BannerH6>
    </IntroSectionSimpleBanner.Banner>
  )
}

const Intro = () => (
  <IntroSectionSimple {...props}>
    {
      ({ topText, bottomText }) =>
        <Banner
          topText={topText}
          bottomText={bottomText}
        />
    }
  </IntroSectionSimple>
)

export default Intro;