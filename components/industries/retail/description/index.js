import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Provide a better shopping experience for your customers. Be in control of every aspect of your retail store. From Point of Sale to Inventory Management, Checkout and Customer Loyalty Programs, we provide custom retail software to increase sales, maintain customer loyalty and stand out from the crowd.
    </P>
    <P>
      In our company we help retailers rethink their business strategy and quickly adapt to the dynamic retail environment across all channels. Whether you run a physical store or e-commerce site, we can 
    </P>
  </BaseContentSection>
)

export default Description;
