import Contact from 'components/contact';


const headerProps = {
  text2: 'Get Ahead ',
  text1: 'in the Energy and Utilities Sector',
  reverse: true,
  wrapText: true,
  inline: true,
}

const text = 'Find out how the latest software and automation solutions can propel your business to success.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)