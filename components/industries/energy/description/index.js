import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Today the key challenges facing the energy and utilities sector is the adoption of renewable energy resources, increasing demand for mobile applications, and minimising environmental impact.
    </P>
    <P>
      To overcome these global challenges utility companies need to adopt the latest cloud-based technology and automation which powers mobile and web-based applications, Internet of Things (IoT) devices, and enterprise software. 
    </P>
    <P>
      By meeting the needs of energy-conscious consumers with user-friendly and intuitive software – and using automated technology to streamline day-to-day business practices – utility companies can gain a competitive edge, increase customer satisfaction and reduce costs.
    </P>
  </BaseContentSection>
)

export default Description;
