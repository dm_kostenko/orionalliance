import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/industries_page/energy/intro_bg.jpg'; 
const topText = 'Energy and Utilities Software';
const bottomText = 'Orion Alliance’s energy and utilities software solutions help business owners increase productivity and customer satisfaction across multiple channels.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;