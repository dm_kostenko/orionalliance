import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Manage Documents',
  'Calculators and Estimators',
  'Dashboards',
  'Conversion Tools'
];

const Mobile = () => (
  <BaseContentSection>
    <BaseSectionHeader
      text2='Mobile'
      text1='Development'
      reverse
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      Provide a simple, intuitive and user-friendly mobile application that lets your customers monitor their utility usage, receive and pay bills, request emergency services, and be advised on routine maintenance
    </P>
    <P>
      Through the use of 2D/3D computer rendering software, you can conceptualise and test the viability of new utilities before they are built. You can assess the impact of new developments in urban areas and identify any potential conflicts with pre-existing utilities in the vicinity.
    </P>
    <ULSimple items={items} />
  </BaseContentSection>
)

export default Mobile;
