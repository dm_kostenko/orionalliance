import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';




const Asset = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Utility Asset'
      text1='Management'
      reverse
      noTopPadding
      />
    <HeaderSeparator />
    <P>
      Stay informed on the status and condition of your utility systems, respond quickly to maintenance requests, and increase the lifespan of your ageing infrastructure. 
    </P>
    <P>
      By keeping on top of your asset management, you will continue to provide a consistently reliable service to your customers, reduce costly downtime, and stay compliant with industry regulations.
    </P>
  </BaseContentSection>
)

export default Asset;
