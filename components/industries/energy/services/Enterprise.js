import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';

const Enterprise = () => (
  <BaseContentSection>
    <BaseSectionHeader
      text2='Utility Enterprise'
      text1='Software'
      reverse
      noTopPadding
      />
    <HeaderSeparator />
    <P>
      Our team utilises the latest development tools and procedures to create innovative products that help you gain the competitive edge, deliver a user-friendly experience, and streamline internal business practices.
    </P>
    <P>
      Our business-oriented tools are fully scalable to meet the changing demands of the energy, water, gas and electricity sector. With a strong focus on accessibility and functionality, you can be confident your enterprise software is designed to satisfy the end user and achieve specific business outcomes.
    </P>
  </BaseContentSection>
)

export default Enterprise;
