import BaseContentSection from 'elementals/BaseContentSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Real-time data gathering, analysis and reporting using customizable dashboards, geospatial integrations, and other features',
  'SCADA systems development',
  'Development of algorithms for calculating and monitoring electricity functions',
  'Billing software development for metered and non-metered energy services, automated invoicing and payments processing and other features'
];

 


const Additional = () => (
  <BaseContentSection>
    <BaseSectionHeader
      text2='Additional'
      text1='Services'
      reverse
      noTopPadding
    />
    <HeaderSeparator />
    <ULSimple items={items} />
  </BaseContentSection>
)

export default Additional;
