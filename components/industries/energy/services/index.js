import Why from './Why';
import Enterprise from './Enterprise';
import Asset from './Asset';
import CAD from './CAD';
import Mobile from './Mobile';
import Additional from './Additional';


const Services = () => [
  <Why key='why' />,
  <Enterprise key='ent' />,
  <Asset key='asset' />,
  <Mobile key='mobile' />,
  <CAD key='cad' />,
  <Additional key='add' />
]

export default Services;