import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';




const CAD = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='CAD Software'
      text1='Management'
      reverse
      noTopPadding
      />
    <HeaderSeparator />
    <P>
      Before new utilities can be installed or upgraded, measures must be taken to ensure the service doesn’t interfere with existing infrastructure and surrounding buildings.
    </P>
    <P>
      Through the use of 2D/3D computer rendering software, you can conceptualise and test the viability of new utilities before they are built. You can assess the impact of new developments in urban areas and identify any potential conflicts with pre-existing utilities in the vicinity.
    </P>
  </BaseContentSection>
)

export default CAD;
