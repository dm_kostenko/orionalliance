import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';



const Why = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Why Choose'
      text1='Orion Alliance'
      reverse
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      For over 10 years we have worked closely with utility companies around the world to provide flexible, scalable and cost-effective software solutions for the energy and utility sector.
      From Utility Asset Management to Performance Monitoring, Customer Billing Software and Geospatial Integration – everything you need to manage your business is right here.
    </P>
    <P>
      Whether you need to upgrade your legacy systems or setup a new IT infrastructure, you’ll be amazed how simple it is to integrate the latest technology into your existing business practices.
    </P>
  </BaseContentSection>
)

export default Why;
