import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/industries_page/hospitality/intro_bg.jpg'; 
const topText = 'Hospitality, Travel and Leisure Software';
const bottomText = 'Orion Alliance provides bespoke software solutions that connect customers to service providers in the Hospitality, Travel and Leisure Industry.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;