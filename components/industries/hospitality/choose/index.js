import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const Section = styled(BaseContentSection)`
  z-index: 10;
`

const items = [
  'Hotel, Travel and Leisure Online Booking Sites and Applications',
  'Restaurant Food and Bar Inventory Management Software',
  'Integration of Custom Restaurant HCM Software for Payroll, Timesheet and Schedule Management',
  'Kiosk Ticketing Solutions to Support Flexible Payment Options',
  'CMMS Software for Amusement Park Maintenance Scheduling, Employee Work Planning and Tracking',
  'Development of Admission Control Systems for Turnstiles and Doors With Support Of RFID, Cards, Biometric, Mobile Ticket Scanning',
  'Development of Security Software for Real-Time Surveillance and Analytics, Smart Locks, Restricted Access Management'
]

 

const Choose = () => [
  <Section key='1' lightGray>
    <BaseSectionHeader
      text1='Customer Experience'
      text2='Bringing a Personal Touch to the'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      We provide bespoke software solutions to connect customers and service providers quickly, easily and seamlessly. We have over 100+ application developers who understand the unique challenges of your industry and are able to provide the best solution for your business needs.
    </P>
    <P>
      By taking advantage of the latest cloud-based digital platforms, web and mobile applications and internal booking software, we can design and supply all kinds of automated solutions to increase consumer satisfaction, business growth and productivity.
    </P>
    <P>
      Rest assured you can expect the highest level of customer service and support while you receive constant updates on development progress.
    </P>
  </Section>,
  <Section key='2'>
    <BaseSectionHeader
      text1='Solutions'
      text2='Our Software'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Below are the following types of Hospitality, Travel and Leisure Software we have experience with:
    </P>
    <ULSimple items={items}/>
  </Section>
]

export default Choose;