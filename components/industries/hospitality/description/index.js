import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      For over 10 years Orion Alliance has been a leading developer of secure, reliable and cost-effective software solutions for the Hospitality, Travel and Leisure industry.
    </P>
    <P>
      These days, customers enjoy the convenience of booking and ordering online from the comfort of their mobile device. From booking a flight to making a reservation at a restaurant or buying tickets for an amusement park, you need a secure and reliable infrastructure to power these vital services.
    </P>
    <P>
      That’s why enterprises involved in the hospitality, travel and leisure industry need to adopt the latest technology trends to personalise the customer experience and stay ahead of the competition.
    </P>
  </BaseContentSection>
)

export default Description;
