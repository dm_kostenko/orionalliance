import Contact from 'components/contact';

const headerProps = {
  text1: 'Customers',
  text2: 'Connect With Your',
  reverse: true,
}

 



const text = 'Whether you’re in the market for a secure booking system, customer platform or kiosk system, Orion Alliance can help you.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)