import Contact from 'components/contact';

const headerProps = {
  text2: 'Contact Us',
  reverse: true,
}


const text = 'Find out how Orion Alliance can add tremendous value to your company.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)