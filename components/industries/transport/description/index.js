import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      We provide an array of custom software solutions to support the various functions that drive real results in the Transport, Logistics and Traffic industry. Our services also extend to the Rental, Car Sharing and Parking sector. 
    </P>
    <P>
      All of our bespoke software is fully customisable and scalable to meet the needs of your business. Our 100+ strong team of developers work closely with you to define your challenges and business goals then provide a cost-effective solution to ensure maximum return on your investment.
    </P>
  </BaseContentSection>
)

export default Description;
