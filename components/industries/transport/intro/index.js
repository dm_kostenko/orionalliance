import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';
import styled from 'styled-components';
import IntroSectionSimpleBanner from 'elementals/introSection/IntroSectionSimpleBanner';

const backgroundImgUrl = '/img/industries_page/transport/intro_bg.jpg';
const topText = 'Transport, Traffic, Car Rental and Parking Software';
const bottomText = 'Orion Alliance provides custom software solutions to power the various functions of the Transport, Traffic, Car Rental, Car Sharing and Parking industry.'

const props = {
  backgroundImgUrl,
  bottomText,
  topText,
}

const BannerH2 = styled(IntroSectionSimpleBanner.BannerH2)`
  max-width: 80vw;
  @media (min-width: 768px) {
    max-width: 40vw;
  }
`

const Banner = (props) => {
  const { topText, bottomText } = props;

  return (
    <IntroSectionSimpleBanner.Banner>
      <BannerH2>
        {topText}
      </BannerH2>
      <IntroSectionSimpleBanner.BannerH6>
        {bottomText}
      </IntroSectionSimpleBanner.BannerH6>
    </IntroSectionSimpleBanner.Banner>
  )
}

const Intro = () => (
  <IntroSectionSimple {...props}>
    {
      ({ topText, bottomText }) =>
        <Banner
          topText={topText}
          bottomText={bottomText}
        />
    }
  </IntroSectionSimple>
)

export default Intro;