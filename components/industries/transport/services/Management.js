import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'Real-time traffic monitoring systems development including RFID sensors, number plate recognition, weigh-in motion devices, electronic beacons and other features',
  'Neural network software for automated speed detection, traffic enforcement, incident detection',
  'Online dashboards with reporting, recording and playback, intersection control tools'
]

const Management = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/transport/i2.png'
      heading='Traffic Management'
    />
    <P>
      From monitoring dangerous driving patterns, to reviewing road lifespan and exploring ways to reduce traffic congestion, we provide road surveyors and city planners with indispensable software tools to assist motorway management and urban development. 
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Management;