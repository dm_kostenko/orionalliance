import BaseContentSection from 'elementals/BaseContentSection';
import Solutions from './Solutions';
import Management from './Management';
import Rental from './Rental';
import Parking from './Parking';


const Services = () => (
  <BaseContentSection lightGray>
    <Solutions noTopPadding/>
    <Management />
    <Rental />
    <Parking />
  </BaseContentSection>
)

export default Services;