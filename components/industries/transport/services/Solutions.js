import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'View real-time updates on delivery status',
  'Monitor driver status to quickly diagnose and address early signs of fatigue',
  'Provide accurate ETAs for clients based on real-time data on traffic congestion, accident reports; including vehicle speed, mileage and fuel consumption',
  'Colour-coded project management software lets you easily allocate and track multiple jobs at once'
]






const Solutions = ({noTopPadding}) => (
  <Article noTopPadding={noTopPadding}>
    <IconedHeading
      iconSrc='/img/industries_page/transport/i1.png'
      heading='Transportation Solutions'
    />
    <P>
      Our transport services aim to reduce stress, eliminate supply chain errors and provide the best possible end-user experience.
    </P>
    <P>
      Use the latest Transport Management Software (TMS) to deliver stock from A to B more swiftly, safely and cost-effectively. No matter how complex your supply chain is, we can implement the right software package to support your current business practices and legacy systems. 
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Solutions;