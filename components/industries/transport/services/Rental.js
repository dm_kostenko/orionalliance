import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';
import styled from 'styled-components';

const items = [
  'Mobile and web clients for online booking and payment integrated to tour operator, OTA services and online booking tools',
  'Car rental rate management software development',
  'Rental contract and rental auto park management software, insurance services',
  'Rental car tracking'
]


const StyledList = styled(ULSimple)`
  @media (min-width: 992px) {
    padding-right: 7rem;
  }
`


const Rental = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/transport/i3.png'
      heading='Rentals and Car Sharing'
      />
    <P>
      Whether you operate a car rental company or car sharing service, we develop custom web, mobile and third-party applications to power your consumer services and streamline your internal business processes.
    </P>
    <P>
      From fleet management to vehicle maintenance scheduling, online booking and payment integration – all of your needs are covered.
    </P>
    <StyledList items={items}/>
  </Article>
)

export default Rental;