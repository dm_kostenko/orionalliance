import styled from 'styled-components';

const Article = styled.div`
  padding-top: ${props => props.noTopPadding ? '0' : '3rem'};
`

export default Article;