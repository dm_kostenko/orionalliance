import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import ULSimple from 'elementals/ULSimple';
import P from 'elementals/P';

const items = [
  'Self-service mobile apps',
  'Mobile parking payments',
  'Parking access control',
  'Parking management solutions'
]


const Parking = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/industries_page/transport/i4.png'
      heading='Parking'
      />
    <P>
      Parking management doesn’t have to be complex. We make it easy.
    </P>
    <P>
      We supply custom software designed to help car park operators manage their parking spaces; stay up to date with real-time parking occupancy, compliance and parking reservations, and power intuitive and easy to use software for self-service parking machines.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Parking;