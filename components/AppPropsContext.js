import React from 'react';

const defaultAppProps = {
  minWindowWidth: NaN,
  touchDevice: true,
};

Object.freeze(defaultAppProps);

const { Provider, Consumer } = React.createContext(defaultAppProps);

const withAppProps = Component => props => (
  <Consumer>
    {appProps => <Component {...props} appProps={appProps} />}
  </Consumer>
);

export { Provider, Consumer, withAppProps, defaultAppProps };

export default {
  Provider,
  Consumer,
  withAppProps,
  defaultAppProps,
};
