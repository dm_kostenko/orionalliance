export default [
  {
    imgUrl: '/img/industries/t_banking.jpg',
    text: 'Banking, Finance and Insurance',
    href: '/banking-finance-insurance'
  },
  {
    imgUrl: '/img/industries/t_health.jpg',
    text: 'Healthcare',
    href: '/healthcare'
  },
  {
    imgUrl: '/img/industries/t_hospitality.jpg',
    text: 'Hospitality, Travel and Leisure',
    href: '/hospitality-travel-leisure'
  },
  {
    imgUrl: '/img/industries/t_manufact.jpg',
    text: 'Industrial Manufacturing',
    href: '/industrial-manufacturing-software'
  },
  {
    imgUrl: '/img/industries/t_oil.jpg',
    text: 'Oil and Gas',
    href: '/oil-gas'
  },
  {
    imgUrl: '/img/industries/t_retail.jpg',
    text: 'Retail Software',
    href: '/retail-software'
  },
  {
    imgUrl: '/img/industries/t_transport.jpg',
    text: 'Transport',
    href: '/transport'
  },
  {
    imgUrl: '/img/industries/t_energy.jpg',
    text: 'Energy and Utilities',
    href: '/energy-utilities'
  },
]
