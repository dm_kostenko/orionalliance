import styled from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';

import BaseTile from './BaseTile';
import tilesData from './tilesData';

import {
  TopText,
  BottomText,
} from './IndustriesTexts';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;

  padding-bottom: 8rem;
`

const SectionHeader = styled(BaseSectionHeader)`
  justify-content: center;
`

const TilesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  margin: 3rem 0;
`


const createTile = (props, index) => (<BaseTile key={index} {...props}/>)

const Industries = () => (
  <Section>
    <SectionHeader
      reverse
      text2='INDUSTRIES'
      text1='WE SERVE'
    />
    <TopText/>
    <TilesContainer>
      {tilesData.map(createTile)}
    </TilesContainer>
    <BottomText/>
  </Section>
)

export default Industries;