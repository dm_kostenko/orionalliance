import styled from 'styled-components';
import P from 'elementals/P';

const IndustriesTopTextP = styled(P)`
  text-align: center;
  width: 100%;
  @media (min-width: 768px) {
    width: 80%;
  }
`

const IndustriesBottomTextP = styled(P)`
  text-align: center;
  width: 100%;
  @media (min-width: 768px) {
    width: 55%;
  }
`

const TopText = () => (
  <IndustriesTopTextP>
    Since founded in 2007 Orion Alliance has developed lasting 
    relationships with clients across a broad range of industries. 
    Our talented staff has the ability to meet the unique needs of 
    any sized business in any industry.
  </IndustriesTopTextP>
)

const BottomText = () => (
  <IndustriesBottomTextP>
    Whether your priority is to improve productivity, site safety, 
    customer engagement, or cost reduction – your personal needs will be met.
  </IndustriesBottomTextP>
)

export {
  TopText,
  BottomText,
}