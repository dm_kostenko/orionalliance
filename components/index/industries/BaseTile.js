import PropTypes from 'prop-types';
import styled from 'styled-components';

import StyledPropsHelper from 'utils/StyledPropsHelper';
import URLPrefixedLink from 'elementals/URLPrefixedLink';
import withPathPrefix from 'utils/withPathPrefix';

const TileText = styled.label`
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  padding: 0 0 7.5%;
  text-align: center;
  color: ${props => props.theme.textColorAlt};
`;

const Link = styled.a`
  position: relative;

  flex: 1 0 49%;
  height: 40vw;
  overflow: hidden;
  margin: 0 ${StyledPropsHelper.getThemeProp('borderWidth')}
    ${StyledPropsHelper.getThemeProp('borderWidth')} 0;
  cursor: pointer;

  @media (min-width: 768px) {
    flex: 1 0 24%;
    height: 18.5vw;
  }

  background-image: url(${props => withPathPrefix(props.imgUrl)});

  background-position: center;
  background-repeat: no-repeat;
  background-size: 110%;

  transition: background-size 0.35s;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: white;
    transition: opacity 0.35s;
    opacity: 0;
  }

  &:hover {
    background-image: url(${props => withPathPrefix(props.imgUrl)});
    background-size: 130%;

    &::after {
      opacity: 0.5;
    }
  }
`;

const BaseTile = props => {
  const { imgUrl, text, href } = props;

  return (
    <URLPrefixedLink href={href} passHref>
      <Link imgUrl={imgUrl} title={text}>
        <TileText>{text}</TileText>
      </Link>
    </URLPrefixedLink>
  );
};

export default BaseTile;
