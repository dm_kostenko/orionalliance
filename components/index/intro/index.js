import styled from 'styled-components';
import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';
import IntroSectionSimpleBanner from 'elementals/introSection/IntroSectionSimpleBanner';

const backgroundImgUrl = '/img/intro_bg.jpg';
const topText = 'Software Development Services for ambitious companies';
const bottomText =
  'Providing software solutions to empower business growth and overcome specific challenges.';

const props = {
  backgroundImgUrl,
  topText,
  bottomText
};

const Section = styled(IntroSectionSimple)`
  min-height: 28em;
  padding-bottom: 1.5em;
  padding-right: 0;
  @media (min-width: 768px) {
    height: 28em;
  }
`;

const Banner = styled(IntroSectionSimpleBanner)`
  & h1 {
    text-transform: uppercase;
    @media (min-width: 768px) {
      max-width: 37rem;
    }
  }
  & p {
    max-width: 25rem;
  }
`;

const Intro = () => (
  <Section {...props}>
    {({ topText, bottomText }) => (
      <Banner topText={topText} bottomText={bottomText} />
    )}
  </Section>
);

export default Intro;
