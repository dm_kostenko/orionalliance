import styled from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import ParallaxWrapper from 'elementals/ParallaxWrapper';
import withPathPrefix from 'utils/withPathPrefix';

import BaseTile from './BaseTile';
import tilesData from './tilesData';
import SolutionsText from './SolutionsText';

//@media (min-width: 768px) {

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 0;
`;

const ContentWrapper = styled.div`
  position: relative;
  padding: 0 ${props => props.theme.sectionMarginLeftRight} 3rem;
`;

const SectionHeaderContainer = styled.div`
  position: relative;
  @media (min-width: 768px) {
    position: static;
    padding-top: 0;
  }
`;

const SectionHeader = styled(BaseSectionHeader)`
  flex-direction: column;
  align-items: center;
  background-color: ${props => props.theme.mainBackgroundColor};
  z-index: 10;

  @media (min-width: 768px) {
    flex-direction: row;
    max-width: 50%;
    align-items: flex-start;
    padding: ${props => props.theme.solutionsHeadingPadding};
  }
`;

const ContentContainer = styled.div`
  flex: 1 0 auto;
  display: -ms-grid;
  display: grid;
  position: relative;
  font-size: 0.9rem;

  -ms-grid-columns: 1fr 1fr 1fr;
  grid-template-columns: repeat(3, 1fr);

  -ms-grid-rows: 28vw 28vw;
  grid-template-rows: auto auto 10em 10em 10em 10em 10em;

  @media (min-width: 768px) {
    left: -16.9% !important;
    -ms-grid-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-columns: repeat(7, 16.75%);

    -ms-grid-rows: 12vw 12vw 12vw;
    grid-template-rows: 11em 11em 11em;
  }
`;


const TextContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;

  z-index: 50;

  -ms-grid-row-span: 2;
  grid-row: 1 / span 2;

  -ms-grid-column-span: 3;
  grid-column: 1 / span 3;

  width: 100%;
  height: 100%;
  padding: ${props => props.theme.solutionsTextPaddingMobile};

  @media (min-width: 768px) {
    -ms-grid-row-span: 2;
    grid-row: 1 / span 2;

    -ms-grid-column-span: 4;
    grid-column: 1 / span 4;
    padding-bottom: 0;
    // padding: ${props => props.theme.solutionsTextPadding};
    padding-left: calc(2.5rem + 24%);
    padding-right: 3rem;
  }

  ::after {
    content: '';
    position: absolute;
    z-index: -10;

    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${props => props.theme.solutionTextBackgroundColor};

    @media (min-width: 768px) {
      left: auto;

      width: 50vw;
    }
  }
`;

const FakeTile = styled.div`
  position: relative;
  z-index: 50;
  -ms-grid-row: 3;
  -ms-grid-column: 1;
  background-color: ${props => props.theme.solutionTextBackgroundColor};
  @media (min-width: 768px) {
    display: none;
  }
`;

const createTile = (tileProps, i) => {
  const desktop = {};
  if (i < 7) {
    desktop.col = ((i + 3) % 3) + 5;
    desktop.row = Math.floor((i + 3) / 3);
  } else {
    desktop.col = ((i + 1) % 7) + 1;
    desktop.row = 2 + Math.ceil((i - 5) / 6);
  }
  const mobile = {};
  if (i < 2) {
    mobile.col = i + 2;
    mobile.row = 3;
  } else {
    mobile.col = ((i - 2) % 3) + 1;
    mobile.row = 3 + Math.ceil((i - 1) / 3);
  }

  return <BaseTile key={i} desktop={desktop} mobile={mobile} {...tileProps} />;
};

//const tiles = ;

const ImgContainer = styled.div`
  position: absolute;
  overflow: hidden;

  top: 0;
  right: ${props => `-${props.theme.sectionMarginLeftRightMobile}`};
  bottom: 0;
  left: ${props => `-${props.theme.sectionMarginLeftRightMobile}`};

  @media (min-width: 768px) {
    right: 0;
    left: 50%;
  }
`;

const Img = styled.div`
  display: none;

  @media (min-width: 768px) {
    display: block;
    position: absolute;
    top: -50%;
    width: 100%;
    height: 200%;
    background-image: url(${props => withPathPrefix(props.url)});
    background-position: center;
    background-size: cover;
  }
`;

const Divider = styled.div`
  position: relative;
  left: 50%;
  border-left-width: ${props => props.theme.borderWidth};
  border-left-style: solid;
  display: none;
  width: 0;

  @media (min-width: 768px) {
    display: block;
  }
`;

const Divider1 = styled(Divider)`
  height: 9rem;
  border-left-color: ${props => props.theme.verticalSeparatorColor};
`;

const Divider2 = styled(Divider)`
  height: 1rem;
  border-left-color: ${props => props.theme.verticalSeparatorColorAlt};
`;

const Shadow = styled.div`
  position: absolute;
  width: 100%;
  top: 50%;
  bottom: 4rem;
  right: 0;
  box-shadow: -100px 0 6rem -2rem #000;
`;

const Solutions = () => (
  <Section>
    <ContentWrapper>
      <SectionHeaderContainer>
        <SectionHeader text1='NEXT LEVEL' text2='SOFTWARE SOLUTIONS' />
        <Shadow />
        <ImgContainer>
          <ParallaxWrapper
            offsetYMin='-80%'
            offsetYMax='80%'
            styleOuter={{
              position: 'relative',
              height: '100%',
            }}
            styleInner={{
              position: 'relative',
              height: '100%',
            }}
          >
            <Img url='/img/solutions/tiles_bg.jpg' />
          </ParallaxWrapper>
        </ImgContainer>
      </SectionHeaderContainer>
      <ContentContainer>
        <TextContainer>
          <SolutionsText />
        </TextContainer>
        <FakeTile />
        {tilesData.map(createTile)}
      </ContentContainer>
    </ContentWrapper>
    <Divider1 />
    <Divider2 />
  </Section>
);

export default Solutions;
