import React from 'react';
import styled from 'styled-components';
import P from 'elementals/P';

const SolutionsTextP = styled(P)`
  color: ${props => props.theme.solutionTextColor};
  &:first-child {
    padding-top: 0;
  }
`

const SolutionsText = () => (
  <React.Fragment>
    <SolutionsTextP>
      At Orion Alliance we understand keeping up with new technology is tough. 
      That’s why we provide world class software solutions across a range of 
      industries to keep you productive, competitive, and focused on your 
      business objectives.
    </SolutionsTextP>
    <SolutionsTextP>
      By adopting the latest software tools, Orion Alliance streamlines the 
      development cycle and helps realise your vision sooner.
    </SolutionsTextP>
    <SolutionsTextP>
      Here are the key areas we focus on to generate real results for your business&nbsp;&nbsp;&#8594;
    </SolutionsTextP>
  </React.Fragment>
)

export default SolutionsText;