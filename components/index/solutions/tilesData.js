export default [
  {
    imgUrl: '/img/solutions/t_app_maint.png',
    imgUrlHover: '/img/solutions/t_app_maint_hov.png',
    text: 'Application Support&nbsp;& Maintenance',
    href: '/application-support-maintenance'
  },
  {
    imgUrl: '/img/solutions/t_app_maint.png',
    imgUrlHover: '/img/solutions/t_app_maint_hov.png',
    text: 'AWS Cost Optimization',
    href: '/aws-cost-optimization'
  },
  {
    imgUrl: '/img/solutions/t_ccd.png',
    imgUrlHover: '/img/solutions/t_ccd_hov.png',
    text: 'Cloud Computing & Development',
    href: '/cloud-computing-development'
  },
  {
    imgUrl: '/img/solutions/t_dat_analitycs.png',
    imgUrlHover: '/img/solutions/t_dat_analitycs_hov.png',
    text: 'Data<br/>Analytics',
    href: '/data-analytics'
  },
  {
    imgUrl: '/img/solutions/t_dev_ops.png',
    imgUrlHover: '/img/solutions/t_dev_ops_hov.png',
    text: 'DevOps',
    href: '/devops'
  },
  {
    imgUrl: '/img/solutions/t_ead.png',
    imgUrlHover: '/img/solutions/t_ead_hov.png',
    text: 'Enterprise Application Development',
    href: '/enterprise-appication-development'
  },
  {
    imgUrl: '/img/solutions/t_ent_int.png',
    imgUrlHover: '/img/solutions/t_ent_int_hov.png',
    text: 'Enterprise Application Integration',
    href: '/enterprise-application-integration'
  },
  {
    imgUrl: '/img/solutions/t_inf_sup.png',
    imgUrlHover: '/img/solutions/t_inf_sup_hov.png',
    text: 'Infrastructure<br/>Support',
    href: '/infrastructure-support'
  },
  {
    imgUrl: '/img/solutions/t_it_out.png',
    imgUrlHover: '/img/solutions/t_it_out_hov.png',
    text: 'IT Outstaffing',
    href: '/it-outstaffing'
  },
  {
    imgUrl: '/img/solutions/t_leg_mig.png',
    imgUrlHover: '/img/solutions/t_leg_mig_hov.png',
    text: 'Legacy<br/>Migration',
    href: '/legacy-migration'
  },
  {
    imgUrl: '/img/solutions/t_mobile_dev.png',
    imgUrlHover: '/img/solutions/t_mobile_dev_hov.png',
    text: 'Mobile Application<br/>Development',
    href: '/mobile-application-development'
  },
  {
    imgUrl: '/img/solutions/t_web_dev.png',
    imgUrlHover: '/img/solutions/t_web_dev_hov.png',
    text: 'Web Application<br/>Development',
    href: '/web-application-development'
  },
  {
    imgUrl: '/img/solutions/t_qa.png',
    imgUrlHover: '/img/solutions/t_qa_hov.png',
    text: 'QA & Testing',
    href: '/qa-testing'
  }
]
