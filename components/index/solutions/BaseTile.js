import PropTypes from 'prop-types';
import styled from 'styled-components';

import StyledPropsHelper from 'utils/StyledPropsHelper';
import withPathPrefix from 'utils/withPathPrefix';
import URLPrefixedLink from 'elementals/URLPrefixedLink';

const PositionedBaseTileElement = styled.a`
  position: relative;
  z-index: 10;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;

  font-size: 0.9rem;
  font-weight: 700;

  -ms-grid-row: ${props => props.mobile.row};
  -ms-grid-column: ${props => props.mobile.col};

  &:hover {
    z-index: 25;
  }

  @media (min-width: 768px) {
    z-index: 10;
    -ms-grid-row: ${props => props.desktop.row};
    -ms-grid-column: ${props => props.desktop.col};

    :nth-child(3) {
      grid-column-start: 5;
      grid-row-start: auto;
    }

    :nth-child(4) {
      grid-column-start: auto;
      grid-row-start: auto;
    }

    :nth-child(7n + 9) {
      grid-column-start: 1;
    }
  }
`;

const ColoredBaseTileElement = styled(PositionedBaseTileElement)`
  border: ${StyledPropsHelper.getThemeProp('borderWidth')} solid
    ${StyledPropsHelper.getThemeProp('verticalSeparatorColor')};

  background-color: ${props => props.theme.solutionsTileBackgroundColorAlt};

  :nth-child(odd) {
    background-color: ${props => props.theme.solutionsTileBackgroundColorAlt};
  }
  :nth-child(even) {
    background-color: ${props => props.theme.solutionsTileBackgroundColor};
  }

  :nth-child(2n + 9) {
    background-color: ${props => props.theme.solutionsTileBackgroundColor};
  }
  :nth-child(2n + 10) {
    background-color: ${props => props.theme.solutionsTileBackgroundColorAlt};
  }
  &:hover {
    background-color: ${props => props.theme.solutionsTileBackgroundColorHover};
    border: none;
  }
  @media (min-width: 768px) {
    border: none;
    :nth-child(odd) {
      background-color: ${props => props.theme.solutionsTileBackgroundColorAlt};
    }
    :nth-child(even) {
      background-color: ${props => props.theme.solutionsTileBackgroundColor};
    }

    :nth-child(2n + 8) {
      background-color: ${props => props.theme.solutionsTileBackgroundColor};
    }
    :nth-child(2n + 9) {
      background-color: ${props => props.theme.solutionsTileBackgroundColorAlt};
    }
    :hover {
      background-color: ${props =>
        props.theme.solutionsTileBackgroundColorHover};
      border-color: ${props => props.theme.solutionsTileBackgroundColorHover};
      box-shadow: 0 0 10rem -2.2rem #000;
    }
  }
`;

const BaseTileElement = styled(ColoredBaseTileElement)`
  &:hover {
    & > i:nth-child(1) {
      display: none;
    }
    & > i:nth-child(2) {
      display: inline;
    }
    & > div {
      display: block;
    }
    & > label {
      color: ${StyledPropsHelper.getThemeProp('textColorAlt')};
    }
  }
`;

const BaseTileImage = styled.i`
  flex: 0 0 auto;
  width: 100%;
  height: 100%;
  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center 95%;
  background-repeat: no-repeat;
  background-size: auto;
  @media (max-width: 768px) {
    width: 80%;
    height: 35%;
    background-size: contain;
  }
`;

const BaseTileImageHover = styled(BaseTileImage)`
  display: none;
  
`;

const BaseTileText = styled.label`
  flex: 0 0 auto;
  white-space: pre-wrap;
  max-width: 100%;
  text-align: center;
  padding: 2.5% 0 2.5%;
  line-height: 1.3;
  color: ${props => props.theme.textColor};
  height: 50%;
  display: flex;
  align-items: center;
  cursor: pointer;
`;

const HoverMark = styled.div`
  display: none;
  position: absolute;
  border-top: 1.8px solid ${StyledPropsHelper.getThemeProp('textColorAlt')};
  width: 0.5rem;
  top: ${props => props.top};
  right: ${props => props.right};
  bottom: ${props => props.bottom};
  left: ${props => props.left};
  box-shadow: 0 0 1em 0.05em ${StyledPropsHelper.getThemeProp('textColorAlt')};
`;

const BaseTile = props => {
  const {
    mobile,
    desktop,
    imgUrl,
    imgUrlHover,
    text,
    className,
    href,
  } = props;

  return (
    <URLPrefixedLink href={href} passHref>
      <BaseTileElement
        className={className}
        mobile={mobile}
        desktop={desktop}
        
        title={text.replace('<br/>', ' ').replace('&nbsp;', ' ')}
      >
        <BaseTileImage url={imgUrl} />
        <BaseTileImageHover url={imgUrlHover} />
        <BaseTileText dangerouslySetInnerHTML={{ __html: text }} />
        <HoverMark top='0.5rem' left='0.5rem' />
        <HoverMark top='0.5rem' right='0.5rem' />
        <HoverMark bottom='0.5rem' left='0.5rem' />
        <HoverMark bottom='0.5rem' right='0.5rem' />
      </BaseTileElement>
    </URLPrefixedLink>
  );
};

BaseTile.contextTypes = {
  _containerProps: PropTypes.any,
};

export default BaseTile;
