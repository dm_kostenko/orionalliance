import styled from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import FullSectionBackgroundContainer from 'elementals/FullSectionBackgroundContainer';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ParallaxWrapper from 'elementals/ParallaxWrapper';
import withPathPrefix from 'utils/withPathPrefix';

import BaseTile from './BaseTile';
import tilesData from './tilesData';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 5rem;
  padding-bottom: 5rem;
`;

const SectionHeader = styled(BaseSectionHeader)`
  justify-content: center;
  & > span:nth-child(1) {
    color: #c3c3c3;
  }
`;

const TilesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  margin-top: 3rem;
`;

const Img = styled.div`
  display: block;
  position: absolute;
  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center;
  background-size: cover;
  width: 100%;
  height: 100%;

  @media (min-width: 768px) {
    top: -50%;
    width: 100%;
    height: 200%;
  }
`;

const Advantage = () => (
  <Section>
    <FullSectionBackgroundContainer>
      <ParallaxWrapper
        offsetYMin='-100%'
        offsetYMax='100%'
        styleOuter={{
          position: 'relative',
          height: '100%',
        }}
        styleInner={{
          position: 'relative',
          height: '100%',
        }}
      >
        <Img url='/img/advantage/background.jpg' />
      </ParallaxWrapper>
    </FullSectionBackgroundContainer>
    <SectionHeader
      color1='#c3c3c3'
      text2='ALLIANCE ADVANTAGE'
      text1='THE ORION'
    />
    <HeaderSeparator />
    <TilesContainer>
      {tilesData.map((props, index) => (
        <BaseTile key={index} {...props} />
      ))}
    </TilesContainer>
  </Section>
);

export default Advantage;
