export default [
  {
    imgUrl: '/img/advantage/t_cust.png',
    heading1: '24/7',
    heading2: 'Customer&nbsp;Support',
    text: 'No matter where you are – or what time it is in' + 
      ' your part of the world – our 100+ strong team of expert' + 
      ' staff is ready to consult your business needs via ' + 
      'phone, email, live chat and live video.',
  },
  {
    imgUrl: '/img/advantage/t_tec.png',
    heading1: 'World&nbsp;Class',
    heading2: 'Technology',
    text: 'From Cloud Computing to Blockchain Technology and ' + 
      'Automation, it’s easy to equip your business with the ' + 
      'latest software tools and technology to gain the ' + 
      'competitive edge.',
  },
  {
    imgUrl: '/img/advantage/t_exp.png',
    heading1: '10+&nbsp;Years',
    heading2: 'of&nbsp;Experience',
    text: 'Our years of experience mean we are efficient ' + 
      'in all aspects of software development, up-to-date on ' + 
      'the latest technology advancements, and always looking ' + 
      'forward to provide solutions that exceed your expectations.',
  },
  {
    imgUrl: '/img/advantage/t_pers.png',
    heading1: 'Personalised',
    heading2: 'to&nbsp;Your&nbsp;Needs',
    text: 'Orion Alliance provides bespoke software solutions ' + 
      'designed to identify, address, and overcome industry-specific ' + 
      'matters relevant to you. We take the time to focus on your pain ' + 
      'points and guide you towards a better way.',
  },
]