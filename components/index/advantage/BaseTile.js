import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

import H3 from 'elementals/H3';
import TileIcon from 'elementals/TileIcon';
import P from 'elementals/P';

const BaseTileElement = styled.div`
  position: relative;
  flex: 1 0 80%;
  overflow: visible;
  display: flex;

  padding: 2rem;

  @media (min-width: 768px) {
    flex: 1 0 40%;
  }
`;

const ButtonElement = styled(TileIcon)`
  margin: 0;
  background-image: url(${props => withPathPrefix(props.imgUrl)});
  background-position: center center;
  background-repeat: no-repeat;
  height: 8rem;
  padding: 0 2.875rem;
`;

const TextContainer = styled.div`
  margin-left: 2rem;
`;

const Header = styled(H3)`
  font-weight: 400;
`;

const Heading1 = styled.span`
  color: ${props => props.theme.elementColorAlt};
`;

const Heading2 = styled.span`
  color: ${props => props.theme.elementColor};
  &::before {
    content: ' ';
  }
`;

const Heading = ({ heading1, heading2 }) => (
  <Header>
    <Heading1 dangerouslySetInnerHTML={{ __html: heading1 }} />
    <Heading2 dangerouslySetInnerHTML={{ __html: heading2 }} />
  </Header>
);

const TileText = styled(P)`
  color: ${props => props.theme.textColorGray};
`;

const BaseTile = ({ imgUrl, heading1, heading2, text }) => (
  <BaseTileElement>
    <ButtonElement imgUrl={imgUrl} />
    <TextContainer>
      <Heading heading1={heading1} heading2={heading2} />
      <TileText>{text}</TileText>
    </TextContainer>
  </BaseTileElement>
);

export default BaseTile;
