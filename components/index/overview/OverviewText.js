import styled from 'styled-components';
import P from 'elementals/P';
import TextLink from 'elementals/TextLink';
import StyledPropsHelper from 'utils/StyledPropsHelper';
import scrollToContact from 'utils/scrollToContact';

const SolutionsTextP = P;

const Wrapper = styled.div`
  

  @media (min-width: 768px) {
    padding: ${StyledPropsHelper.getThemeProp('overviewTextPadding')};
  }
`



const SolutionsText = () => (
  <Wrapper>
    <SolutionsTextP>
      Orion Alliance supports businesses across a range of industries to harness 
      the full potential of information technology. Based in Europe we have 3 
      development centres located in Latvia, The Netherlands and Poland.
    </SolutionsTextP>
    <SolutionsTextP>
      Since 2007 we have employed over 100 industry experts who provide onsite 
      and offshore services in software design, testing, support, outsourcing, 
      legacy system migration, enterprise application integration and block 
      chain technology.
    </SolutionsTextP>
    <SolutionsTextP>
      Our software solutions are designed to empower business growth, inspire 
      customer engagement and overcome industry-specific challenges. Whether your 
      enterprise is big or small, Orion Alliance is fully equipped to handle even 
      the most complex tasks, yet still offer a high level of customer service 
      with a personal touch.
    </SolutionsTextP>
    <SolutionsTextP>
      Get the support you need now.<br/>
      <TextLink href='#contact-us' onClick={scrollToContact}>
        &#8594;&nbsp;&nbsp;Contact our 24/7 support line to get started.
      </TextLink>
    </SolutionsTextP>
  </Wrapper>
)

export default SolutionsText;