import styled, { css } from 'styled-components';
import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ImageLoadDetector from 'elementals/ImageLoadDetector';
import withPathPrefix from 'utils/withPathPrefix';

import StyledPropsHelper from 'utils/StyledPropsHelper';

import OverviewText from './OverviewText';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  padding-top: 2rem;
  padding-bottom: 3rem;
  @media (min-width: 768px) {
    flex-direction: row;
    padding-bottom: 0;
  }
`;

const SectionHeader = styled(BaseSectionHeader)`
  flex: 0 0 auto;
  background-color: ${StyledPropsHelper.getThemeProp('mainBackgroundColor')};
  @media (min-width: 768px) {
    padding-left: ${StyledPropsHelper.getThemeProp(
      'sectionItemPaddingToCenter'
    )};
  }
`;

const TextContainer = styled.div`
  position: relative;
  flex: 0 0 auto;
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
    flex: 1 0 10em;
    border-left: ${props => props.theme.borderWidth} solid
      ${props => props.theme.verticalSeparatorColor};
  }
`;

const OverviewHeaderSeparator = styled(HeaderSeparator)`
  @media (min-width: 768px) {
    margin-left: ${StyledPropsHelper.getThemeProp(
      'sectionItemPaddingToCenter'
    )};
  }
`;

const ImgContainer = styled.div`
  position: relative;
  flex: 0 0 auto;
  top: -3.5em;
  height: 80vw;
  ${props =>
    props.imageLoaded &&
    css`
      &::before {
        content: '';
        position: absolute;
        z-index: -1;
        width: 37vw;
        top: 0;
        bottom: 1.5em;
        left: 0;
        box-shadow: 0 2rem 2rem -1rem #000;
      }
    `}
  @media (min-width: 768px) {
    flex: 1 0 10em;
    height: 30em;
    z-index: 10;
  }
`;

const Img = styled.div`
  width: 100%;
  height: 100%;

  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center;
  background-size: 100% auto;
`;

const Overview = () => (
  <Section>
    <ImageLoadDetector url='/img/overview/overview.jpg'>
      {imageLoaded => (
        <ImgContainer imageLoaded={imageLoaded}>
          <Img url='/img/overview/overview.jpg' />
        </ImgContainer>
      )}
    </ImageLoadDetector>
    <TextContainer>
      <SectionHeader text1='ORION ALLIANCE' text2='OVERVIEW' />
      <OverviewHeaderSeparator />
      <OverviewText />
    </TextContainer>
  </Section>
);

export default Overview;
