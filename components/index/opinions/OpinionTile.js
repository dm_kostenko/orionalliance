import styled from 'styled-components';
import H3 from 'elementals/H3';
import P from 'elementals/P';

const OpinionTileElement = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  background-color: ${props => props.theme.mainBackgroundColor};
  border-radius: 0.5rem;
  padding: 1rem;
  margin: ${props => props.odd ? 
    props.theme.opinionTileMarginOdd :
    props.theme.opinionTileMargin
  };
  box-shadow: 0 0.5rem 2.5rem 0.01rem #777;

  width: 80%;
  
  @media (min-width: 768px) {
    width: 45%;
  }

  @media (min-width: 992px) {
    width: 30%;
  }
`

const Heading = styled(H3)`
  color: ${props => props.theme.opinionTileHeadingColor};
  font-weight: 400;
`

const RatingElement = styled.div`
  color: ${props => 
    props.rating ? 
    props.theme.opinionTileRatingColor :
    props.theme.textColor
  };
  font-style: ${props => props.rating ? 'normal' : 'italic'};
`

const AddressElement = styled.address`
  color: ${props => props.theme.textColor};
  padding-top: 2rem;
`

const TextElement = styled(P)`
  position: relative;
  height: 10rem;
  overflow: hidden;
  ::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 2rem;
    background-image: linear-gradient(to top, rgba(255, 255, 255 , 1), rgba(255, 255, 255 , 0));
  }
`

const OpinionTile = ({heading, rating, text, name, company, odd}) => (
  <OpinionTileElement odd={odd}>
    <Heading>{heading}</Heading>
    <RatingElement 
      rating={rating}
      dangerouslySetInnerHTML={{ __html: rating ?
        '&#9733;'.repeat(rating) :
        'no rating' }}
    />
    <TextElement>
      <i>
        {'"'}
        {text}
        {'"'}
      </i>
    </TextElement>
    <AddressElement>
      <b>{name}</b><br/>
      {company}
    </AddressElement>
  </OpinionTileElement>
)

export default OpinionTile;