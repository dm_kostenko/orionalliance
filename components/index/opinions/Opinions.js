import styled from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import AssetPrefixedImage from 'elementals/AssetPrefixedImage';

import Carousel from './Carousel';
import OpinionTile from './OpinionTile';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${props => props.theme.opinionsBackgroundColor};
  padding: 4rem 0 3rem;
`

const SectionHeader = styled(BaseSectionHeader)`
  justify-content: center;
  max-width: 65%;
  @media (min-width: 992px) {
    max-width: 35%;
  }
`

const OpinionsContainer = styled.div`
  position: relative;
  width: 100%;
  padding-bottom: 3rem;
`

const BackgroundContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
  
`

const Background = styled(AssetPrefixedImage)`
  
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const Opinions = () => (
  <Section>
    <SectionHeader
      text1='Learn How&nbsp;'
      text2='Orion Alliance Has Helped Clients Around the World'
      wrapText={true}
      inline={true}
    />
    <HeaderSeparator/>

    <OpinionsContainer>
      <BackgroundContainer>
        <Background
          src='/img/opinions/background.png'
        />
      </BackgroundContainer>
      <Carousel
      
        responsive={{
          0: 1,
          768: 2,
          992: 3,
        }}
      >
        <OpinionTile
          heading='"Great service and support."'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='John Doe'
          company='Howard & Partners Co.'
        />
        <OpinionTile
          heading='"Thank you for helping!"'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='Karen Smith'
          company='KS Consultancy'
          odd
        />
        <OpinionTile
          heading='"A great experience."'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='Woode'
          company='Acer Inc.'
        />
        <OpinionTile
          heading='"Great service and support."'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='John Doe'
          company='Howard & Partners Co.'
          odd
        />
        <OpinionTile
          heading='"Thank you for helping!"'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='Karen Smith'
          company='KS Consultancy'
        />
        <OpinionTile
          heading='"A great experience."'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='Woode'
          company='Acer Inc.'
          odd
        />
        <OpinionTile
          heading='"A great experience."'
          rating={5}
          text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          name='Woode'
          company='Acer Inc.'
        />
      </Carousel>
      
    </OpinionsContainer>
  </Section>
)

export default Opinions;