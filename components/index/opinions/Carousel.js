import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Swipe from 'react-easy-swipe';
import autobind from 'autobind-decorator';

import { withAppProps } from 'components/AppPropsContext';

const MainContainer = styled.div`
  display: flex;
  width: 100%; 
`

const ContentContainer = styled.div`
  position: relative;
  display: flex;
  flex: 0 0 ${props => props.theme.carousel.contentWidth};
  padding: ${props => props.theme.carousel.contentPadding};
  
  overflow: hidden;
`

const Arrow = styled.nav`
  flex: 0 0 ${props => props.theme.carousel.arrowsWidth};
  position: relative;

  cursor: ${props => props.disabled ? 'default' : 'pointer'};
  opacity: ${props => props.disabled ? 0.4 : 1};

  ::before {
    content: '';

    position: absolute;
    top: 50%;
    left: 50%;
    

    width: ${props => props.theme.carousel.arrowsSize};
    height: ${props => props.theme.carousel.arrowsSize};
    border-top: 
      ${props => props.theme.carousel.arrowsLineWidth}
      solid
      ${props => props.theme.carousel.arrowsColor};
    border-left: 
      ${props => props.theme.carousel.arrowsLineWidth}
      solid
      ${props => props.theme.carousel.arrowsColor};
  }
`

const ArrowLeft = styled(Arrow)`
  ::before {
    transform: translate(-30%, -50%) rotate(-45deg);
  }
`

const ArrowRight = styled(Arrow)`
  ::before {
    transform: translate(-30%, -50%) rotate(135deg);
  }
`



const Transporter = styled.div`
  
  display: flex;
  height: 100%;
  width: 100%;
  transform: ${props => `translateX(-${props.offsetX}%)`};
  transition: transform ${props => `${props.duration/1000}s`} ease-in-out;
`



const Viewport = styled(Swipe)`
  position: relative;
  flex: 0 0 100%;
  display: flex;
  justify-content: space-around;
  width: 100%;
  height: 100%;
  
  @media (min-width: 768px) {
    justify-content: space-between;
  }
`

class Carousel extends React.Component {

  static getDerivedStateFromProps(props, prevState) {
    //responsive
    let itemsPerPage = 0;
    if (isNaN(props.appProps.minWindowWidth)) {
      itemsPerPage = props.responsive[0];
    } else {
      Object.keys(props.responsive).sort().forEach(
        function(key) {
          if(props.appProps.minWindowWidth >= key) {
            itemsPerPage = props.responsive[key]
          }
        }
      )
    }
    if (itemsPerPage === prevState.itemsPerPage) {
      return null;
    }

    const numChildren = React.Children.count(props.children)
    
    let keyItem = 0;
    if (prevState.itemsPerPage && !isNaN(prevState.currentPage)) {
      keyItem = 
        prevState.itemsPerPage * prevState.currentPage + 
        ((prevState.itemsPerPage / 2) | 0) - 1;
    }
    if (keyItem >= numChildren) {
      keyItem = numChildren - 1;
    }
    const currentPage = (keyItem / itemsPerPage) | 0;
    const pages = Math.ceil(numChildren / itemsPerPage);
    
    return {
      itemsPerPage,
      currentPage,
      pages,
    }
  }

  constructor(props, ...rest) {
    super(props, ...rest);


    this.handleMoveLeft = this.handleMoveLeft.bind(this)
    this.handleMoveRight = this.handleMoveRight.bind(this)
    this.handleSwipeStart = this.handleSwipeStart.bind(this)
    this.handleSwipeEnd = this.handleSwipeEnd.bind(this)

    this.state = {
      itemsPerPage: 1,
      currentPage: 0,
      pages: 1,
    }
  }

  // @autobind
  handleMoveLeft() {
    if (this.state.currentPage > 0) {
      this.setState({ currentPage: this.state.currentPage - 1 });
    }
  }

  // @autobind
  handleMoveRight() {
    if (this.state.currentPage < this.state.pages - 1) {
      this.setState({ currentPage: this.state.currentPage + 1 });
    }
  }

  // @autobind
  handleSwipeStart(e) {
    this.startTouch = e.changedTouches;
  }

  // @autobind
  handleSwipeEnd(e) {
    if (this.startTouch && this.startTouch[0]) {
      const diffX = e.changedTouches[0].clientX - this.startTouch[0].clientX;
      const diffY = e.changedTouches[0].clientY - this.startTouch[0].clientY;
      if (Math.abs(diffX) / e.view.innerWidth > Math.abs(diffY) / e.view.innerHeight) {
        if (diffX < 0) {
          this.handleMoveRight();
        } else {
          this.handleMoveLeft();
        }
      }
    }
  }

  renderViewports() {
    const { children } = this.props;
    const {
      pages,
      itemsPerPage,
    } = this.state;

    const childrenArray = React.Children.toArray(children);
    const viewports = [];
    for (let page = 0; page < pages; page++) {
      viewports.push(
        <Viewport key={page}
          onSwipeStart={this.handleSwipeStart}
          onSwipeEnd={this.handleSwipeEnd}
          /*onSwipeLeft={this.handleMoveRight}
          onSwipeRight={this.handleMoveLeft}*/
        >
          {childrenArray.slice(page * itemsPerPage, (page + 1) * itemsPerPage)}
        </Viewport>
      )
    }
    
    return viewports;
  }

  render() {
    const {
      duration,
    } = this.props;
    const {
      currentPage,
      pages,
    } = this.state;

    const offsetX = currentPage * 100;
    const viewports = this.renderViewports();
    
    return (
      <MainContainer>
        <ArrowLeft disabled={currentPage===0} onClick={this.handleMoveLeft}/>
        
          <ContentContainer>
            <Transporter offsetX={offsetX} viewportCount={viewports.length} duration={duration}>
              {viewports}
            </Transporter>
            
          </ContentContainer>
        
        <ArrowRight disabled={currentPage===pages-1} onClick={this.handleMoveRight}/>
      </MainContainer>
    )
  }
}

const propTypes = {
  responsive: PropTypes.object.isRequired,
  duration: PropTypes.number,
}

const defaultProps = {
  responsive: {[0]: 1},
  duration: 500,
}

Carousel.propTypes = propTypes;
Carousel.defaultProps = defaultProps;

export default withAppProps(Carousel);