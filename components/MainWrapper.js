import styled, { css } from 'styled-components';

const MainWrapper = styled.main`
  ${props =>
    props.hideScroll &&
    css`
      overflow: hidden;
    `}
  @media (min-width: 768px) {
    padding-bottom: calc(4rem + 0.8 * 1.4rem);
  }
`;

export default MainWrapper;
