import React from 'react';
import Head from 'next/head';

const PageHead = ({ title, description, canonical, keywords, children }) => (
  <Head>
    {title && <title key='title'>{title}</title>}

    {description && (
      <meta key='desc' name='description' content={description} />
    )}

    {canonical && <link key='canonical' rel='canonical' href={canonical} />}

    {keywords && <meta key='keywords' name='keywords' content={keywords} />}

    {children}
  </Head>
);

export default PageHead;
