import React from 'react';
import styled, { keyframes } from 'styled-components';

import createChainedFunction from 'utils/createChainedFunction';
import MainButton from 'elementals/MainButton';
import validateContactForm from 'shared/validateContactForm';

const getBorderWidth = props => props.theme.borderWidth;

const FormElement = styled.form`
  display: ${props => props.submitting ? 'none' : 'flex'};
  flex-direction: column;
`

const FormInputsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const BaseFieldSet = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0 10em;

  border-color: ${props => props.theme.controlsBorderColor};
  border-style: solid;
  border-width: 0;
`


const PersonalsFieldSet = styled(BaseFieldSet)`
  border-top-width: ${getBorderWidth};
  border-left-width: ${getBorderWidth};
  border-right-width: ${getBorderWidth};

  @media (min-width: 768px) {
    border-right-width: 0;
  }
`

const MessageFieldSet = styled(BaseFieldSet)`
  border-left-width: ${getBorderWidth};
  border-right-width: ${getBorderWidth};

  @media (min-width: 768px) {
    border-top-width: ${getBorderWidth};
  }
`

const FieldElement = styled.div`
  flex: 1 0 2.5em;
  position: relative;

  align-self: stretch;
  border-bottom:
    ${getBorderWidth}
    solid ${props => props.focused ?
      props.theme.controlsBorderColorActive :
      props.theme.controlsBorderColor
    };
`

const LabelElement = styled.label`
  position: absolute;
  display: block;
  box-sizing: border-box;

  top: ${props => props.move ? '0.1rem' : '0.75rem'} ;
  left: ${props => props.move ? '0.5rem' : '1rem'};
  padding: 0;
  line-height: 1.225;
  color: ${props =>
    props.error ?
      props.theme.errorTextColor :
      props.theme.textColorAlt
  };
  opacity: ${props => props.move ? 0.4 : (props.focused ? 1 : 0.4)};
  font-weight: 300;
  font-size: ${props => props.move ? '0.7em' : 'inherit'} ;
  transition: all 0.3s;

`

const createControlCreator = (Component) =>
  ({ name, value = '', onChange, error }) =>
    (
      <Component
        name={name}
        id={name}
        value={value}
        error={error}
        onChange={onChange}
      />
    )

class Field extends React.Component {
  constructor(...props) {
    super(...props);
    this.state = {
      focused: false,
    }
    this.handleFocus = this.handleFocusBlur.bind(this, true);
    this.handleBlur = this.handleFocusBlur.bind(this, false);
  }

  handleFocusBlur(focused) {
    this.setState({ focused })
  }

  render() {
    const {
      children,
      name,
      values,
      onChange,
      label,
      errors,
    } = this.props;
    const { focused } = this.state;

    let child = React.Children.only(
      children({
        name,
        value: values[name],
        onChange,
        error: errors[name],
      })
    );

    child =
      React.cloneElement(
        child,
        {
          onFocus: createChainedFunction(child.onFocus, this.handleFocus),
          onBlur: createChainedFunction(child.onBlur, this.handleBlur),
        }
      );

    return (
      <FieldElement focused={focused}>
        {child}
        <LabelElement
          htmlFor={name}
          move={!!values[name] || focused}
          focused={focused}
        >
          {label}
        </LabelElement>
      </FieldElement>
    )
  }
}

const autofillFrames = keyframes`
  to {
    color: #fff;
    background: transparent;
  }
`

const InputControl = styled.input.attrs({
  type: 'text',
  autoComplete: 'nope',
})`
  display: block;
  box-sizing: border-box;
  width: 100%;
  border: none;
  background: transparent;
  padding: 0.75rem 1rem;
  font-size: 1rem;
  color: ${props =>
    props.error ?
      props.theme.errorTextColor :
      props.theme.textColorAlt
  };
  line-height: 1;
  font-weight: 300;

  &:-webkit-autofill {
    -webkit-animation-name: ${autofillFrames};
    -webkit-animation-fill-mode: both;
  }
`

const TextAreaControl = styled.textarea`
  position: absolute;
  width: 100%;
  height: calc(100% - 1.2rem);
  font-size: 1rem;
  color: ${props =>
    props.error ?
      props.theme.errorTextColor :
      props.theme.textColorAlt
  };
  line-height: 1.5;
  font-weight: 300;
  padding: 0 1rem;
  margin: 0.6rem 0;
  resize: none;
  border: none;
  background: transparent;
  overflow: hidden;
`

const AgreeCheckbox = styled.input.attrs({
  type: 'checkbox',
})`
  flex: 1 0 auto;
  margin-right: 1rem;
`

const AgreeLabel = styled.label`
  display: flex;
  align-items: center;
  width: calc(100% - 2rem);
  margin-top: 2rem;
  align-self: center;
  color: ${props => props.theme.elementColorAlt};
  font-weight: 300;
  cursor: pointer;
  @media (min-width: 768px) {
    width: 70%;
  }
  
`

const createInputControl = createControlCreator(InputControl);
const createTextAreaControl = createControlCreator(TextAreaControl);

const SubmitButton = styled(MainButton)`
  width: 12rem;
  align-self: center;
`

const FailedLabel = styled.label`
  z-index: 10;
  color: ${props => props.theme.errorTextColor};
  font-weight: 300;
  align-self: center;
  padding-top: 1em;
`

class ContactForm extends React.Component {

  constructor(...args) {
    super(...args);
    this.state = {
      values: {},
      errors: {},
      disableSubmit: true,
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitClick = this.handleSubmitClick.bind(this);
  }
  static sender = 'skulbaev@klta.net';

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    const values = {
      ...this.state.values,
      [name]: value,
    }

    const disableSubmit = !values.name || 
      !values.email || 
      !values.message ||
      !values.agree;

    this.setState({
      ...this.state,
      values,
      errors: {},
      disableSubmit,
    });
  }

  validate() {
    const { values } = this.state;
    return validateContactForm(values);
  }

  handleSubmitClick(e) {
    e.preventDefault();
    const errors = this.validate();

    if (Object.keys(errors).length) {
      this.setState({ errors });
    } else {
      this.props.onSubmit(this.state.values);
    }
  }

  render() {
    const { className, submitting, submitError } = this.props;

    const { values, errors, disableSubmit } = this.state;

    return (
      <FormElement
        className={className}
        submitting={submitting}
        autoComplete='off'
      >
        <input type='hidden' value='prayer' autoComplete='nope'/>
        <FormInputsWrapper>
          <PersonalsFieldSet>
            <Field
              name='name'
              label='Your Name'
              onChange={this.handleInputChange}
              values={values}
              errors={errors}
            >
              {createInputControl}
            </Field>
            <Field
              name='email'
              label='Your Email'
              onChange={this.handleInputChange}
              values={values}
              errors={errors}
            >
              {createInputControl}
            </Field>
            <Field
              name='contactNumber'
              label='Your Contact Number'
              onChange={this.handleInputChange}
              values={values}
              errors={errors}
            >
              {createInputControl}
            </Field>
          </PersonalsFieldSet>
          <MessageFieldSet>
            <Field
              name='message'
              label='Your Message (min 50 characters)'
              onChange={this.handleInputChange}
              values={values}
              errors={errors}
            >
              {createTextAreaControl}
            </Field>
          </MessageFieldSet>
        </FormInputsWrapper>
        
        <AgreeLabel htmlFor='agree'>
          <AgreeCheckbox
            id='agree'
            name='agree'
            checked={!!values.agree}
            onChange={this.handleInputChange}
          />
          <span>
            {'I agree to be contacted by our support team at Orion Alliance by the email address and telephone number I have provided '}
          </span>
        </AgreeLabel>
        
        <SubmitButton
          onClick={this.handleSubmitClick}
          disabled={disableSubmit}
        >
          Send Now
        </SubmitButton>
        {!submitting && submitError &&
          <FailedLabel>{submitError}</FailedLabel>
        }
      </FormElement>
    )
  }
}

export default ContactForm;
