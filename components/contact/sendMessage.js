const processStatus = (statusCode) => {
  switch (statusCode) {
    case 400:
      return 'Can not process bad request. Please try again later.';
    case 401:
      return 'You are not authorized to send request. Please try again later.';
    case 403:
      return 'Server is refusing your request now. Please try again later.';
    case 404:
      return 'Can not process your request now. Please try again later.';
    case 500:
      return 'Server is not responding. Please try again later.';
    default:
      return 'Can not send your mail now. Please try again later.';
  }
};

export default async (url, values) => (
  fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(values),
  }).then(response => {
    if (!response.ok || response.status !== 200) {
      throw new Error(processStatus(response.status));
    } else {
      return response.json().catch(e => {
        throw new Error('Can not send your mail now. Please try again later.');
      });
    }
  }).then(json => {
    if (json.error) {
      throw new Error(json.error);
    }
  }).catch(e => e.message)
)