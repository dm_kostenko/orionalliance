import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';

import BaseContactForm from './ContactForm';
import sendMessage from './sendMessage';
import withPathPrefix from 'utils/withPathPrefix';

const Section = styled.div`
  ${BaseSection.style}
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 4rem;
  padding-bottom: 3rem;
  min-height: ${props => (props.minHeight ? props.minHeight + 'px' : 'auto')};

  background-image: url(${props => withPathPrefix(props.backgroundImgUrl)});
  background-position: center;
  background-size: cover;

  @media (min-width: 992px) {
    padding-top: 4rem;
    padding-bottom: 3rem;
  }
`;

const SectionHeader = styled(BaseSectionHeader)`
  z-index: 10;
  justify-content: center;
  display: ${props => (props.submitting ? 'none' : 'block')};
  & span {
    text-align: center;
    color: ${props => props.theme.elementColorAlt};
  }
`;

const ContactTextItem = styled(P)`
  z-index: 10;
  color: ${props => props.theme.textColorAlt};

  text-align: center;
  display: ${props => (props.submitting ? 'none' : 'block')};
  @media (min-width: 768px) {
    max-width: 100%;
    margin: 0 9.375vw;
  }
`;

const ContactForm = styled(BaseContactForm)`
  align-self: stretch;
  margin: 3rem 0;
  z-index: 10;
  @media (min-width: 992px) {
    margin: 3rem 9.375vw;
  }
`;

const ContactText = ({ text, submitting }) =>
  typeof text === 'string' ? (
    <ContactTextItem submitting={submitting}>{text}</ContactTextItem>
  ) : (
    [
      text.map((item, index) => (
        <ContactTextItem key={index} submitting={submitting}>
          {item}
        </ContactTextItem>
      )),
    ]
  );

const SUBMITTING = 'loading';
const SUBMITTED = 'loaded';

const Loader = styled.div`
  z-index: 10;
  width: 3rem;
  height: 3rem;
  background-image: url(${props => withPathPrefix(props.backgroundImgUrl)});
  background-position: center;
  background-size: cover;
  display: ${props => (props.submitting ? 'block' : 'none')};
`;

const SuccessLabel = styled.label`
  z-index: 10;
  color: ${props => props.theme.textColorAlt};
  font-weight: 300;
`;

class Contact extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitState: null,
      submitError: null,
      submitHeight: null,
    };
    this.handleSubmit = this.handleSubmit.bind(this);

    this.sectionRef = React.createRef();

    this.sendMessageUrl = withPathPrefix('/contact');
  }

  handleSubmit(values) {
    const submitHeight = ReactDOM.findDOMNode(this.sectionRef.current)
      .clientHeight;

    this.setState({
      submitHeight,
      submitState: SUBMITTING,
    });
    sendMessage(this.sendMessageUrl, values).then(submitError => {
      this.setState({
        submitState: SUBMITTED,
        submitError,
      });
    });
  }

  renderForm(headerProps, text, noText, submitError, submitting) {
    return [
      <SectionHeader key='header' {...headerProps} submitting={submitting} />,
      !noText ? <HeaderSeparator key='sep' altColor hide={submitting} /> : null,
      !noText ? (
        <ContactText key='text' text={text} submitting={submitting} />
      ) : null,
      <ContactForm
        key='form'
        onSubmit={this.handleSubmit}
        submitting={submitting}
        submitError={submitError}
      />,
      <Loader
        key='loader'
        backgroundImgUrl='/img/loader.svg'
        submitting={submitting}
      />,
    ];
  }

  renderSuccessLabel() {
    return (
      <SuccessLabel
        dangerouslySetInnerHTML={{
          __html:
            'Thank you for your message.<br/>We will return to you shortly.',
        }}
      />
    );
  }

  render() {
    const { headerProps, text, noText } = this.props;
    const { submitHeight, submitState, submitError } = this.state;

    let content;
    if (
      submitState === null ||
      submitState === SUBMITTING ||
      (submitState === SUBMITTED && submitError)
    ) {
      content = this.renderForm(
        headerProps,
        text,
        noText,
        submitError,
        submitState === SUBMITTING
      );
    } else {
      content = this.renderSuccessLabel();
    }

    return (
      <Section
        id='contact-us'
        ref={this.sectionRef}
        minHeight={submitHeight}
        backgroundImgUrl='/img/contact/background.jpg'
      >
        {content}
      </Section>
    );
  }
}

Contact.propTypes = {
  headerProps: PropTypes.object.isRequired,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
};

Contact.defaultProps = {
  headerProps: {
    text1: "LET'S",
    text2: 'GET STARTED',
  },
  text:
    'Take the first step to growing your business with a free consultation and quote.',
};

export default Contact;
