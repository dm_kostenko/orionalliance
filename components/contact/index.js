//import Contact from './Contact';
import dynamic from 'next/dynamic';

const Contact = dynamic(
  () => import(/* webpackChunkName: "contact" */ './Contact'),
  {
    delay: 5000,
  }
);


export default Contact;