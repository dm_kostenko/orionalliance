import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const AppBrand = styled.a`
  display: block;
  
  width: 16.2em;
  height: 2.8em;

  background-repeat: no-repeat;
  background-image: url(${withPathPrefix('/img/orion_logo.svg')});
  background-size: contain;
  cursor: pointer;
  @media (min-width: 768px) {
    width: 13.08em;
    height: 2.1em;
  }
`
export default AppBrand;