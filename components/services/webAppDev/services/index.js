import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import BaseContentSection from 'elementals/BaseContentSection';
import Frontend from './Frontend';
import Backend from './Backend';
import DB from './DB';

const Services = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Web Application Technology'
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      By keeping up to date with the latest web application development services, platforms and frameworks, our team is flexible enough to work within your technical scope and can provide a custom solution that delivers the most value to your business for longer.
    </P>
    <P>
      Below is a quick rundown on the technology we use to power your ideas:
    </P>
    <Frontend />
    <Backend />
    <DB />
  </BaseContentSection>
)

export default Services;