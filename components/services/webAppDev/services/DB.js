import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import Bold from 'elementals/Bold';
import Article from './Article';

const DB = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/web_app_dev/i3.png'
      heading='Database Technology'
    />
    <P>
      Whether you have an existing database you wish to preserve, or need to setup a brand new database, our team of specialists can provide a simple and effective way to keep your data secure, accessible and scalable.
    </P>
    <P>
      <Bold>Database Management Programs:</Bold> MySQL, Microsoft SQL Server, Oracle Database, Quickbase, Redis and Sybase
    </P>
    <P>
      
    </P>
  </Article>
)

export default DB;