import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import Bold from 'elementals/Bold';
import Article from './Article';

const Frontend = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/web_app_dev/i1.png'
      heading='Front End Technology'
    />
    <P>
      Orion Alliance is up-to-date with the latest front end technology and can provide a custom solution to streamline your workflow and engage with customers on a new level.
    </P>
    <P>
      Based on the current landscape of your IT infrastructure we define the following details:
    </P>
    <P>
      <Bold>Frameworks:</Bold> Bootstrap, Foundation, Backbone, Angular KS, Meteor JS, ReactJS and EmberJS
    </P>
    <P>
      <Bold>Languages:</Bold> CSS, HTML, JavaScript, Ajax and jQuery
    </P>
    <P>
      
    </P>
  </Article>
)

export default Frontend;