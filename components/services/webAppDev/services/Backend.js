import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import Bold from 'elementals/Bold';
import Article from './Article';

const Backend = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/web_app_dev/i2.png'
      heading='Back End Technology'
    />
    <P>
      We can define the type of back end technology you need to satisfy the scope of your project and technical requirements. If you’re unsure which back end solution is right for you, our team will be happy to advise you on the benefits of each one to help you make the right choice.
    </P>
    <P>
      <Bold>Frameworks:</Bold> Symfony, Zend Framework, Django, Pyramid, Spring, ASP.NET and Express
    </P>
    <P>
      <Bold>Languages:</Bold> Java, JavaScript, .NET and PHP
    </P>
    <P>
      <Bold>CMSs:</Bold> WordPress, Drupal and Pimcore
    </P>
  </Article>
)

export default Backend;