export default [
  {
    imgUrl: '/img/services_page/web_app_dev/t1.png',
    heading: 'Over 10 Years of Experience',
    text: 'Over the past 10 years Orion Alliance has built long-term relationships with enterprises across a range of industries and stayed up-to-date with the latest web application technology to provide the best possible solution to meet your business objectives. Maybe you’ll be our next partner in success?',
  },
  {
    imgUrl: '/img/services_page/web_app_dev/t2.png',
    heading: 'Project Management From Start to End',
    text: 'From the very early stages your goals and objectives are clearly defined so that focus can remain on your desired outcomes. Rest assured your project is expertly managed from start to finish and the application of knowledge, tools and skills is carried out in accordance with your project guidelines.',
  },
  {
    imgUrl: '/img/services_page/web_app_dev/t3.png',
    heading: 'Mobile Responsive',
    text: 'Every effort is made to ensure the customer experience is consistent whether they are on a mobile device, tablet or desktop. By creating web solutions that adapt to the specific needs of mobile users – from using different screen sizes, to past and current operating systems, and different performance capabilities – we guarantee your application will be fast, responsible and easy to access on any mobile device.',
  },
  {
    imgUrl: '/img/services_page/web_app_dev/t4.png',
    heading: 'Pre-Release Testing',
    text: 'Learn about possible bugs, errors, compatibility, and performance issues early before the initial release by performing different types of testing. From performance to quality assurance, usability, security and site management – every aspect of your web application is tested so you know it’ll meet the highest industry standards.',
  },
  {
    imgUrl: '/img/services_page/web_app_dev/t5.png',
    heading: 'Deployment and Post-Release Support',
    text: 'Even after your web application has launched you receive ongoing support from our highly skilled web developers. Whether you need to resolve key issues, add more features and services, or overhaul the GUI design – we can help.',
  },
]
