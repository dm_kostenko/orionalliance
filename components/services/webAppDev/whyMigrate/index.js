import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  padding-bottom: 0;
  z-index: 10;
`

const WhyMigrate = () => (
  <Section>
    <BaseSectionHeader
      text2='Why Choose Orion Alliance'
      wrapText
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <DarkTiles
      items={tilesData}
    />
  </Section>
)

export default WhyMigrate;