import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/web_app_dev/intro_bg.jpg'; 
const topText = 'Web Application Development Services';
const bottomText = 'Our 100+ strong team of web developers have over 10 years of experience creating web&#8209;based applications for small and large enterprises for many industries.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;