import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';
import styled from 'styled-components';

const AppsListsContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const AppsList = styled(ULSimple)`
  @media (max-width: 767px) {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media (min-width: 992px) {
    padding-right: 3.25rem;
  }
`

const AppsList1 = styled(AppsList)`
  
  @media (max-width: 767px) {
    margin-bottom: 0;
    padding-bottom: 0;
  }
`

const AppsList2 = styled(AppsList)`
  @media (min-width: 768px) {
    padding-left: 0;
  }
  @media (max-width: 767px) {
    margin-top: 0;
    padding-top: 0;
  }
`



const itemsApps1 = [
  'Online Booking Form',
  'Social Networking Platform',
  'eCommerce Shopping Cart and Purchase System',
  'Content Management System',
]

const itemsApps2 = [
  'Online Banking',
  'Interactive Games',
  'Online Training',
  'And much more…',

]

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      For over 10 years Orion Alliance has had the privilege of working with businesses across a range of industries – from Banking to Healthcare, Tourism and Hospitality, Industrial and Manufacturing – to provide innovative web applications development services that drive customer engagement, streamline internal business processes, and promote positive brand awareness.
    </P>
    <P>
      Whether you want to provide an interactive web-based service for customers or a practical solution for internal staff, Orion Alliance can plan, develop and release any kind of application to meet your business needs.
    </P>
    <P>
      Just think of what your business could achieve with one of these web-based applications:
    </P>
    <AppsListsContainer>
      <AppsList1 items={itemsApps1}/>
      <AppsList2 items={itemsApps2} />
    </AppsListsContainer>
    <P>
      Our 100+ strong team of expert web developers’ work closely with you to define your business objectives and the outcomes you wish to achieve with a bespoke web application. 
    </P>
    <P>
      From Planning to Development, Testing, Deployment and Release, you’re guided each step of the way and get regular status updates throughout the development cycle.
    </P>
  </BaseContentSection>
)

export default Description;
