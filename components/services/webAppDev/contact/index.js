import Contact from 'components/contact';


const headerProps = {
  text2: 'contact Us',
  reverse: true,
}

const text = 'We are available 24/7 to discuss your project requirements.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)