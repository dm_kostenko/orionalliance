import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  z-index: 10;
  padding-top: 0;
  padding-bottom: ${props => props.last ? '0' : 'inherited'};
`

const WhyMigrate = () => (
  <Section last>
    <BaseSectionHeader
      text2='What We Do'
      reverse
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Orion Alliance specialises in native and cross-platform mobile app development services for a broad range of industries. Our team of industry experts have years of experience turning innovative app ideas into reality for iPhone, iPad and Android users. We also specialise in developing for Blackberry and Windows.
    </P>
    <P>
      Whether your purpose is to help people complete tasks, entertain themselves, monitor vital business tasks or book appointments, Orion Alliance can develop and deliver a custom app from the ground-up that reflects your business needs, budget and timeline.
    </P>
    <P>
      Mobile application development is a complex multistage process. It requires the participation of many qualified specialists: 
      system analysts, interface designers, developers and testers. 
      Our company knows how to organize teamwork and produce high-quality products of any complexity. 
      Below are the following steps we take to realize your mobile app vision:
    </P>
    <DarkTiles
      items={tilesData}
      tileWidths={['80%', '40%', '40%']}
      notLast={true}
    />
    <P>
      From the choice of programming languages to the final quality assurance process, we use the latest technology in the development process. 
      Our applications are carefully developed and thoroughly tested. The technology stack that provides your application:
    </P>
  </Section>
)

export default WhyMigrate;