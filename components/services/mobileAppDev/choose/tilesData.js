export default [
  {
    imgUrl: '/img/services_page/mobile_app_dev/t1.png',
    heading: 'Establish Project Scope',
    text: 'Define key details such as the purpose of the application, target audience, native or cross-platform aspirations, defining the back-end structure, technical requirements, monetization, budget and timeline',
    preText: 'Our customer receives:',
    items: [
      'Terms of reference - requirements for mobile application development',
      'API Requirements'
    ]
  },
  {
    imgUrl: '/img/services_page/mobile_app_dev/t2.png',
    heading: 'Development',
    text: 'Our team works quickly and efficiently to develop your app. We keep you involved at each stage of development and ensure the final product is delivered on time, on budget.',
    preText: 'Our customer receives:',
    items: [
      'File to download the application on the App Store and Google Play',
      'Application source code'
    ]
  },
  {
    imgUrl: '/img/services_page/mobile_app_dev/t3.png',
    heading: 'Pre-Release Testing',
    text: 'We undergo an extensive testing process that involves running your application on different mobile and tablet devices, along with current and previous versions of iOS, Android, Blackberry OS and Windows OS. Based on the results of these tests the application is fine-tuned until full compatibility is achieved on the desired devices.',
    preText: 'Our customer receives:',
    items: [
      'Application ready for use on different devices',
      'Checklists and test cases for testing'
    ]
  },
  {
    imgUrl: '/img/services_page/mobile_app_dev/t4.png',
    heading: 'Deployment',
    text: 'Orion Alliance can deploy your application to early adopters rather than the general public. From there we gather honest and reliable feedback and work hard to resolve reports of bugs, errors and other issues. Once your application is fine-tuned to perfection we can proceed with launching to the general public and provide ongoing support after the initial release.',
    preText: 'Our customer receives:',
    items: [
      'Stable application',
      'Prompt decision of technical problems'
    ]
  },
]
