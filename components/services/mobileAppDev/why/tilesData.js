export default [
  {
    imgUrl: '/img/services_page/mobile_app_dev/ios.png',
    heading: 'Result-oriented solutions for iOS applications',
    text: 'We create result-oriented  iOS applications that meet the desires and goals of the client in various industries. Orion Alliance focuses on development process to create intuitive, functional and easy to use  application. Based on your requirements, our team of experienced developers creates and offers the best methods to make your application stand out on the market. Having extensive experience in developing mobile applications for iOS, we have helped many clients to develop and improve their business, to increase profits. We will bring any ideas to life, providing quality control at every stage of development: prototyping, application architecture or UI / UX.',
  },
  {
    imgUrl: '/img/services_page/mobile_app_dev/android.png',
    heading: 'Infinite possibilities with Android apps',
    text: [ 
      'Orion Alliance develops Android applications that are easy to use, understandable to users and based on Android recommendations. We have experience in creating applications for devices such as smartphones, tablets, wearables.', 
      'Your application can be from almost any industries: finance, banking, travel, fitness, healthcare, transport. Our mobile applications allow users to communicate, to sell, to do sports, to manage business. We can carry out projects for companies of various sizes: from startups to huge corporations. Invite our team of Android application developers to collaborate and turn your dreams into success stories.'
    ]
  }
]
