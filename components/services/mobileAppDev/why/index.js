import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  z-index: 10;
  padding-top: 0;
  padding-bottom: ${props => props.last ? '0' : 'inherited'};
`

const Why = () => (
  <Section last>
    <BaseSectionHeader
      text2='Why Choose Orion Alliance'
      reverse
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <DarkTiles
      items={tilesData}
      tileWidths={['80%', '40%', '40%']}
      wrap={true}
      colorsDefault={[
        "#2b2b2b",
        "#1d1d1d"
      ]}
    />
  </Section>
)

export default Why;