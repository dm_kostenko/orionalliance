import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Reach a new pool of potential customers, particularly the younger demographic',
  'Make it easier for users to connect with your brand and organisation',
  'Save money on marketing (releasing apps is far cheaper than traditional marketing methods)',
  'Improve internal processes of the enterprise',
  'Offer a more streamlined and positive customer experience',
  'Receive honest feedback and reliable data from application users',
  'Gain an edge over the competition'
]

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      In 2016, an estimated 62.9 percent of the global population owned a mobile phone. In response to the rising ownership of smart devices, developing applications for mobile devices and tablets has become a priority for businesses.
    </P>
    <P>
      Why? Because mobile applications allow your business to:
    </P>
    <ULSimple items={items} />
  </BaseContentSection>
)

export default Description;
