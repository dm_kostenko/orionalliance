import PropTypes from 'prop-types';
import styled from 'styled-components';

import StyledPropsHelper from 'utils/StyledPropsHelper';
import withPathPrefix from 'utils/withPathPrefix';
import ULTable from 'components/elementals/ULTable';

const PositionedBaseTileElement = styled.a`
  position: relative;
  z-index: 10;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;

  font-size: 0.9rem;
  font-weight: 700;

  -ms-grid-row: ${props => props.mobile.row};
  -ms-grid-column: ${props => props.mobile.col};

  &:hover {
    z-index: 25;
  }

  @media (min-width: 768px) {
    z-index: 10;
    -ms-grid-row: ${props => props.desktop.row};
    -ms-grid-column: ${props => props.desktop.col};

    :nth-child(4) {
      grid-column-start: 1;
      grid-row-start: auto;
    }
  }
  @media (max-width: 600px) {
    font-size: 0.7rem;
  }
`;

const ColoredBaseTileElement = styled(PositionedBaseTileElement)`

  border: ${StyledPropsHelper.getThemeProp('borderWidth')} solid
    ${StyledPropsHelper.getThemeProp('verticalSeparatorColor')};
  color: ${props => props.theme.textColor};
  background-color: ${props => props.theme.solutionsTileBackgroundColor};
  // font-style: italic;
  &:hover, &:active {
    color: ${props => props.theme.textColor};
  }
  :nth-child(3n + 1) {
    color: ${props => props.theme.elementColor};
    text-transform: uppercase;
    font-style: normal;
  }


  @media (min-width: 768px) {
    border: none;
    :nth-child(3n + 1) {
      color: ${props => props.theme.elementColor};
      text-transform: uppercase;
    }
  }
`;

const BaseTileElement = styled(ColoredBaseTileElement)`

  border: 1px solid #303030;
  justify-content: center;
  background-color: ${props => props.backgroundColor};
  :nth-child(3n + 1) {
    font-size: 1.1rem;
    border-left: none;
  } 
  :nth-child(3n) {
    border-right-width: 2px;
  } 
  :nth-child(1), :nth-child(2), :nth-child(3) {
    font-size: 1.1rem;
    border-top: none;
  }
  :nth-last-child(1), :nth-last-child(2), :nth-last-child(3) {
    border-bottom-width: 2px;
  }

  @media (max-width: 600px) {
    :nth-child(1), :nth-child(2), :nth-child(3), :nth-child(3n + 1) {
      font-size: 0.8rem;
    }
  }
  

  &:hover {
    & > i:nth-child(1) {
      display: none;
    }
    & > i:nth-child(2) {
      display: inline;
    }
    & > div {
      display: block;
    }
    // & > label {
    //   color: ${StyledPropsHelper.getThemeProp('textColorAlt')};
    // }
  }

`;


const BaseTileText = styled.label`
  flex: 0 0 auto;
  white-space: pre-wrap;
  max-width: 100%;
  text-align: left;
  padding: 2.5% 0 2.5%;
  line-height: 1.3;
  // color: ${props => props.theme.textColor};
  height: 50%;
  display: flex;
  align-items: center;
  // cursor: pointer;


  @media (max-width: 915px) {
    text-align: center;
    content: ${props => props.content.split(' ').join('\n')}
  }
`;

const Icon = styled.div`
  z-index: 10;
  width: ${props => props.backgroundImgUrl.includes('ios') ? '5rem' : '4rem'};
  height: ${props => props.backgroundImgUrl.includes('ios') ? '5rem' : '4rem'};
  background-image: url(${props => withPathPrefix(props.backgroundImgUrl)});
  background-position: center;
  background-size: cover;
`;



const BaseTile = props => {
  const {
    mobile,
    desktop,
    imgUrl,
    imgUrlHover,
    text,
    className,
    head,
    backgroundColor
  } = props;

  return (
    // <URLPrefixedLink href={href} passHref>
      <BaseTileElement
        className={className}
        mobile={mobile}
        desktop={desktop}
        backgroundColor={backgroundColor}
        title={text.replace('<br/>', ' ').replace('&nbsp;', ' ')}
      >
        {imgUrl 
        ? <Icon backgroundImgUrl={imgUrl} />
        : !head 
          ? <ULTable tiles={true} items={text.split(', ')} />
          : <BaseTileText dangerouslySetInnerHTML={{ __html: text }} content={text} />
      }

      </BaseTileElement>
    // </URLPrefixedLink>
  );
};

BaseTile.contextTypes = {
  _containerProps: PropTypes.any,
};

export default BaseTile;
