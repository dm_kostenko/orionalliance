import styled from 'styled-components';
import BaseSection from 'elementals/BaseSection';
import withPathPrefix from 'utils/withPathPrefix';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  padding-top: 2rem;
  padding-bottom: 2rem;
`;

const Image = styled.div`
  background: url(${props => withPathPrefix(props.url)});
  background-repeat: no-repeat;
  margin-bottom: 2rem;
  margin-left: calc(-2.5vw - 0.2rem);
  background-size: calc(72vw + 7rem) calc(27vw + 10rem);
  height: calc(27vw + 10rem);
  width: calc(72vw + 7rem);                                                                                        
`;


const Systems = () => (
  <Section>
    <Image url='/img/services_page/mobile_app_dev/table.png' />
  </Section>
);

export default Systems;
