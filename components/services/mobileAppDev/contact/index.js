import Contact from 'components/contact';

const headerProps = {
  text2: 'contact Us',
  reverse: true,
}

const text = 'Find out how we can transform your app idea into a reality. '

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)