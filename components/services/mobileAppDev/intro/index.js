import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/mobile_app_dev/intro_bg.jpg'; 
const topText = 'Mobile App Development Services';
const bottomText = 'Orion Alliance designs, tests and deploys applications for mobile and tablet devices running on iOS, Android, Windows and Blackberry.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;