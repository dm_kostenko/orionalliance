import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Orion Alliance provides 24/7 on-site and remote infrastructure support services via live chat, video meetings, email and phone.
    </P>
    <P>
      Our company employs over 100 dedicated IT experts located in 3 offices spread across Latvia, The Netherlands and Poland. From the moment you contact us, you can expect a fast response time and one-on-one customer support in order to reduce downtime and provide the best customer experience.
    </P>
    <P>
      Do you need onsite technical support to solve a complex issue? If an Orion Alliance branch is near you, we can arrange an IT expert to visit you onsite.
    </P>
  </BaseContentSection>
)

export default Description;
