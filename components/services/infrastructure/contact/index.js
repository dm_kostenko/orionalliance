import Contact from 'components/contact';


const headerProps = {
  text2: 'Contact Us',
  reverse: true,
}

const text = 'Message us anytime for immediate support.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)