import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Network design, auditing and architecture planning',
  'Ongoing support for TCP/IP configurations',
  'Ongoing support for Virtual Private Networks (VPNs), Storage Area Networks (SAN) and LAN/WAN',
  'Administration and troubleshooting for network devices such as routers, gateways and firewalls',
  'Database management: design, implementation, optimisation, scaling, data migration, backup and recovery',
  'Apps, databases and network security configuration',
  'And so much more…'
]

const Choose = () => [
  <BaseContentSection key='1' lightGray>
    <BaseSectionHeader
      text2='Complete'
      text1='IT Support'
      wrapText
      reverse
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator />
    <ULSimple items={items}/>
  </BaseContentSection>,
  <BaseContentSection key='2'>
    <BaseSectionHeader
      text2='Flexible Pricing'
      text1='Structure'
      noTopPadding
      reverse
      wrapText
      textAlign='left'
    />
    <HeaderSeparator />
    <P>
      Spend less time worrying about the bill. So you can focus on more important business.
    </P>
    <P>
      Our company provides a range of pricing options – from hourly to block hours and ongoing monthly infrastructure support services. You only pay for the time you need and the level of support you ask of us. This way you can easily track your spending and avoid paying too much for unnecessary customer service fees.
    </P>
    <P>
      All of our pricing structures are upfront and transparent. So you know exactly how much you pay for, every time.
    </P>
  </BaseContentSection>
]

export default Choose;