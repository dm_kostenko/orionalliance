import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/infrastructure_support/intro_bg.jpg'; 
const topText = 'Infrastructure Support Services';
const bottomText = 'Orion Alliance provides 24/7 on-site and remote ' 
  + 'infrastructure support services via live chat, video meetings, ' 
  + 'email and phone. Contact us for immediate support.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;