import Contact from 'components/contact';


const headerProps = {
  text1: 'Today',
  text2: 'Connect With Us',
  reverse: true,
}

const text = 'Trust professionals, build your dream team today.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)