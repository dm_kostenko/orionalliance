import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      The digital age is changing all the patterns of interaction.
      The creation of electronic products is not the only important aspect of building a successful business today. 
      Also it is essential to be able to optimize management schemes. 
    </P>
    <P>
      Modern business processes require the involvement of a large number of IT specialists. 
      In many cases it is needed a whole department. 
    </P>
    <P>
      Orion Alliance can help build your own dedicated team to perform projects on any level. 
    </P>
  </BaseContentSection>
)

export default Description;
