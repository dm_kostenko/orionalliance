export default [
  {
    imgUrl: '/img/services_page/it_outstaffing/t1.png',
    heading: 'Access to expertise',
    text: 'Our employees are universal specialists capable of solving any IT task with a high level of qualification.',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t2.png',
    heading: 'Reduce the burden on the Human Resources',
    text: 'As a part of the workload is reduced, it is possible to optimize and reorganize the human resources department across the organization.',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t3.png',
    heading: 'Increase the efficiency of IT infrastructure',
    text: 'Control and planning of expenses without excessive costs at the stage of launching new projects and introducing innovative solutions. As a result, the productivity of not only the information department, but the entire enterprise is growing.',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t4.png',
    heading: 'Risk reduction',
    text: 'You are guaranteed to receive the services you need exactly on time.',
  },
]
