import BaseContentSection from 'elementals/BaseContentSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import ULSimple from 'elementals/ULSimple';

const items = [
  'If you need an IT specialist with certain skills for your project',
  'If the task solution demands a whole department involvement',
  'If you have an urgent one time task to be completed and to meet the deadline of the project',
  'If you want to lighten the load on your workers and to get fresh solutions for your business'
]

const Choose = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Cooperate'
      text1='With us'
      wrapText
      noTopPadding
      reverse
    />
    <HeaderSeparator/>
    <ULSimple items={items}/>
  </BaseContentSection>
)

export default Choose;