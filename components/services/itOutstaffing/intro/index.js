import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/enterprise/intro_bg.jpg'; 
const topText = 'IT Outstaffing Services';
const bottomText = 'Rely on experts with 10 years’ experience – our knowledge will revive your business with our innovative solutions and increase productivity.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;