import Contact from 'components/contact';

const headerProps = {
  text1: 'Today',
  text2: 'Connect with Us',
  reverse: true,
}

const text = 'Find out how we can help you realise the full potential of your data.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)