import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  z-index: 10;
  padding-bottom: ${props => props.last ? '0' : 'inherited'};
`

const WhyMigrate = () => [
  <Section key='1' lightGray>
    <BaseSectionHeader
      text2='Data Analytics'
      text1='With a Difference'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      Orion Alliance makes it easy to go data-driven. No guesswork or assumptions.
      Only actionable insights based on accurate and reliable data you can really trust.
      Plus, you get a range of forecasting tools to help you analyse past, current and potential future trends.
    </P>
    <P>
      Whether you need to collect data from Internet of Things (IoT) devices, desktop or mobile clients,
      Orion Alliance can setup intelligent automated processes to search, analyse, and collect data from multiple devices into one easy location.
    </P>
  </Section>,
  <Section key='2' last>
    <BaseSectionHeader
      text2='Data Analytics'
      text1='Services'
      reverse
      wrapText
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
        Orion Alliance provide a range of data analytics services to suit the exact needs of your business.
    </P>
    <P>
      From the moment you contact us, you get expert guidance from industry experts who take the time
      to address your needs and come up with custom solution that delivers real outcomes.
      Find out below how you can maximise the potential of your data:
    </P>
    <DarkTiles
      items={tilesData}
      tileWidths={['80%', '40%', '40%']}
    />
  </Section>
]

export default WhyMigrate;