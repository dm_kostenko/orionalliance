export default [
  {
    imgUrl: '/img/services_page/data_analytics/t1.png',
    heading: 'Data Visualisation and Dashboards ',
    text: 'See the bigger picture of your consumer and businesses insights. Gain access to clear visual dashboards that make it easy to track relevant data, ' +
    'monitor progress, spot trending activity, and respond quickly to suspicious behaviour.',
  },
  {
    imgUrl: '/img/services_page/data_analytics/t2.png',
    heading: 'Research and Automation ',
    text: 'Our data analytics experts work one-on-one with you to develop a clear plan regarding what kind of data you want to collect, and what exactly you want to achieve with this data. ' +
    'From there, we can implement a series of processes and systems to collect relevant data that reflects your business needs.',
  },
  {
    imgUrl: '/img/services_page/data_analytics/t3.png',
    heading: 'Consulting and Training ',
    text: 'With help from a certified data analyst expert, you’ll learn how to manage and assess the data you collect, extract valuable insights and identify the clear signs of notable trends. ' +
    'This way you gain the skills and experience to manage your own custom data analytics platform and respond appropriately to the right data.',
  },
  {
    imgUrl: '/img/services_page/data_analytics/t4.png',
    heading: 'Ongoing Customer Support ',
    text: 'Get immediate offsite support via phone or email. ' +
    'Instantly connect with a data analytic expert to overcome a broad range of data-related issues and make sense of confusing or jarring data.',
  },
]
