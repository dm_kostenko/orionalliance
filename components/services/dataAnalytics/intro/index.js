import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/data_analytics/intro_bg.jpg'; 
const topText = 'Data Analytics Services';
const bottomText = 'From automated data mining to predictive modelling and '
 + 'pattern matching, Orion&nbsp;Alliance can setup, configure and monitor the '
  + 'right data analytics system for you.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;