import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Are you getting the most from your customer data?
    </P>
    <P>
      Whether you run a small business or large enterprise, your customers hold valuable
      data that could be the key to your success.
    </P>
    <P>
      By understanding the needs, wants and desires of your customers, you have a better chance of anticipating future trends
      and providing a more personalised service that puts you ahead of the competition.
      This data can also be used to improve the operational efficiency of your enterprise and avoid potential cyber-security threats.
    </P>
    <P>
      The trick is to know where to collect this data. And how you can take advantage of it to deliver the experience your customers want.
    </P>
    <P>
      That’s where Orion Alliance comes in.
    </P>
  </BaseContentSection>
)

export default Description;
