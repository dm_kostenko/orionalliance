import Contact from 'components/contact';


const headerProps = {
  text1: 'Today',
  text2: 'Connect with Us',
  reverse: true,
}

const text = 'Make the leap into the cloud today with expert guidance to make the transition easy.';


export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)