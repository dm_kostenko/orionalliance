import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Are you looking for an easy and cost-effective way to migrate your existing applications, systems and platforms to the cloud? Orion Alliance offer a range of services that allow you to preserve your existing IT infrastructure and utilise the power of cloud technology to its full potential. 
    </P>
    <P>
      Whether your priority is to securely store database data, host a virtual server, or operate a web-based software application – we can help you.
    </P>
    <P>
      Everything you need to enhance the potential of your cloud capabilities is here – from third-party application development to service integration, mobile and web client apps, DevOps and much more.
    </P>
  </BaseContentSection>
)

export default Description;
