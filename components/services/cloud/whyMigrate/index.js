import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  padding-bottom: 0;
  z-index: 10;
`

const WhyMigrate = () => (
  <Section>
    <BaseSectionHeader
      text2='Why Migrate to the Cloud?'
      wrapText
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Regardless of your industry, there are many good reasons to offer at least one or more cloud-based services to your customers. You can provide a better experience for your customers, improve workplace productivity, cash-flow and much more:
    </P>
    <DarkTiles
      items={tilesData}
    />
  </Section>
)

export default WhyMigrate;