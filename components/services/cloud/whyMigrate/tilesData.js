export default [
  {
    imgUrl: '/img/services_page/cloud_services/t1.png',
    heading: 'Be More Flexible',
    text: 'Enable staff and customers to access a range of secure cloud applications on desktop and mobile interfaces. Make rapid changes to the software, perform updates and monitor in-house processing in real-time.',
  },
  {
    imgUrl: '/img/services_page/cloud_services/t2.png',
    heading: 'Save Money on Maintenance',
    text: 'Without the need to store expensive hardware, you can save money on routine maintenance and performance upgrades. Also, with automatic software updates from Orion Alliance, your cloud system will always be secure, fast and compliant with industry standards.',
  },
  {
    imgUrl: '/img/services_page/cloud_services/t3.png',
    heading: 'Scale to Meet Business Needs',
    text: 'You have the freedom to grow or shrink your cloud platform based on your changing needs. Whether you need to boost the capacity of your existing hardware, add more servers or increase storage space, you can quickly scale up or down on-demand so you only pay for the resources you need.',
  },
  {
    imgUrl: '/img/services_page/cloud_services/t4.png',
    heading: 'Gain Competitive Advantage',
    text: 'With real-time automatic software and hardware updates you’ll always be on the cutting edge of cloud technology. Careful measures are taken to ensure each update is compatible with your current infrastructure.',
  },
  {
    imgUrl: '/img/services_page/cloud_services/t5.png',
    heading: 'Access Real-Time Analytics',
    text: 'With the use of SaaS business intelligence, hosted data warehouses and social media analytics powered by the cloud, you have the ability to analyses, compare, consolidate and clean data on a secure cloud platform that’s faster and more efficient than relying on on-site computing power.',
  },
]
