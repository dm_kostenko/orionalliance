import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Public, private and hybrid cloud hosting and development',
  'Cloud application migration',
  'Third-party application development and service integration',
  'Internet of Things (IoT) development',
  'DevOps'
]

const CloudComputingServices = () => (
  <BaseContentSection lightGray>
    <P noTopPadding >
      By becoming familiar with your existing platform – and understanding the needs of your business – Orion Alliance can develop a custom cloud solution that delivers an enhanced user-experience, a more streamlined workflow and an undeniable competitive edge.
    </P>
    <P>
      We provide the following cloud computing services:
    </P>
    <ULSimple items={items} />
    <P>
      With over 10 years of experience working with the most popular public and private cloud services (Amazon Web Services, Microsoft Azure, IBM, Salesforce, Oracle, and more), you can be confident in the technical knowledge of our 100+ strong team and the value they can add to your business.
    </P>
  </BaseContentSection>
)

export default CloudComputingServices;