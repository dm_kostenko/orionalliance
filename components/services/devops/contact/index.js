import Contact from 'components/contact';

const headerProps = {
  text1: ' Consultation Session',
  text2: 'Start Today With a Free DevOps',
  reverse: true,
  wrapText: true,
  inline: true,
}

const text = 'Find out how Orion Alliance can streamline your workflow and help you innovate faster.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)