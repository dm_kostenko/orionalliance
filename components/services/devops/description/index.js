import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
    Do you want to speed up your development cycle and beat your competitors to the punch with innovative and exciting new products? Do you have ongoing release issues? 
    When deploying production, are you worried about possible errors? Are you tired of testing manually? We have a solution for you! Our professionals will help you set up Continuous Integration, Continuous Development, set up automatic testing. We are sure, you’ll be glad to know just how cost-effective, quick and easy it is to implement the DevOps framework into your existing business practices.
    </P>
    {/* <P>
      Since DevOps was first coined back in 2009, Orion Alliance has helped many businesses – just like yours – become more productive and profitable.
    </P>
    <P>
      How? By taking the time to address your current development cycle, identify your key measurable business goals,
      and put into practice a structured DevOps work environment that’s right for you.
    </P> */}
  </BaseContentSection>
)

export default Description;
