import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/devops/intro_bg.jpg'; 
const topText = 'DevOps Services';
const bottomText = 'Find out how Orion Alliance can streamline your development and release cycle with a custom automated DevOps solution.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;