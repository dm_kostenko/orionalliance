import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';
import withPathPrefix from 'utils/withPathPrefix';

const Section = styled(BaseContentSection)`
  z-index: 10;
`

const items = [
  'Development of systems for the automatic preparation and deployment of the host environment for various products. For example, scripts development for terraforms that allows to create and configure all the necessary resources in AWS, Azure and other cloud services automatically. It makes easy to deploy several independent environments for each individual task, be it development, testing or production.',
  'A system that allows to collect automatically Docker containers, run unit tests and deploy the build in one of the AWS environments (dev/UAT/prod) when automatically committing to certain branches of GIT repositories. Through privilege and access settings, it is possible to flexibly configure the hardware system to deploy the build in a particular environment.',
]

const items2 = [
  'Continuous Integration',
  'Continuous Deployment',
  'Configuration Management',
  'Streamline repeatable process',
  'Continuous improvement'
]

const Image = styled.div`
  background: url(${props => withPathPrefix(props.url)}) center;
  background-repeat: no-repeat;
  margin: auto;
  // margin-bottom: 2rem;
  // margin-top: 2rem;
  background-size: calc(33vw + 15rem) calc(19vw + 8rem);
  height: calc(19vw + 8rem);
  width: calc(33vw + 15rem);                                                                                        
`;

const Choose = () => [
  <Section key='1' lightGray>
    <BaseSectionHeader
      text1=''
      text2='What is DevOps?'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      DevOps can be understood as development cycle in which we aim to bridge the gap between the development and operations team. By promoting a strong collaborative approach, our goal is to help your team members share equal responsibility and combine their workflow in order to work more productively.
    </P>
    {/* <P>
      Throughout the development cycle, we aim to bridge the gap between the development and operations team. By promoting a strong collaborative approach,
      our goal is to help your team members share equal responsibility and combine their workflow in order to work more productively.
    </P>
    <P>
      We can also implement custom automated systems to help reduce the workload of your team and let you focus on key development matters.
    </P>
    <P>
      Even after your product launch we can assist you with software updates, bug fixes, and general maintenance
      so your applications will function beautifully and work properly on the latest mobile devices, tablets, web browsers and operating systems.
    </P> */}
    <ULSimple items={items2}/>
    <Image url='/img/services_page/devops/t.jpg' />
  </Section>,
  <Section key='2'>
    <BaseSectionHeader
      text1='Solutions Examples'
      text2='Our DevOps'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator/>
    {/*<P>
        Orion Alliance provide a range of data analytics services to suit the exact needs of your business.
    </P>
    <P>
      From the moment you contact us, you get expert guidance from industry experts who take the time
      to address your needs and come up with custom solution that delivers real outcomes.
    </P>
    <P>
      Find out below how you can maximise the potential of your data:
    </P>*/}
    <ULSimple items={items}/>
  </Section>,
  <Section key='3' lightGray>
    <BaseSectionHeader
      text1='Orion Alliance Guides you Each Step of the Way'
      text2='From Start to End —'
      reverse
      wrapText
      marginLeft='5vw'
    />
  </Section>

]

export default Choose;