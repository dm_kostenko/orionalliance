import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/qa_and_testing/intro_bg.jpg'; 
const topText = 'QA & Testing Services';
const bottomText = 'Orion Alliance’s QA and testing services can help you identify key issues early and release higher quality products to market sooner.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;