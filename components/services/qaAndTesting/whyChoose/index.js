import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';

const WhyChoose = () => (
  <BaseContentSection lightGray>
    <BaseSectionHeader
      text2='Why Choose'
      text1='Orion Alliance'
      wrapText
      noTopPadding
      reverse
    />
    <HeaderSeparator/>
    <P>
      We have over 10 years of experience in QA testing and services for clients in the Healthcare, Banking and Financial, Manufacturing, Industrial, Oil and Gas, Retail, Transport and Utilities sector. 
    </P>
    <P>
      With a deep understanding of industry specific standards, we employ best practices to help you be more competitive, productive and efficient. Each project is managed by key personnel who has experience in your field and can advise you on the best approach to meet your business needs.
    </P>
  </BaseContentSection>
)

export default WhyChoose;