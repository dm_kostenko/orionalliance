import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULWithHeader from 'elementals/ULWithHeader';
import styled from 'styled-components';

const AppsListsContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 0rem;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const AppsList = styled(ULWithHeader)`
  @media (max-width: 767px) {
    // padding-top: 9rem;
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media (min-width: 992px) {
    padding-right: 3.25rem;
  }
`

const AppsList1 = styled(AppsList)`
  
  @media (max-width: 767px) {
    margin-bottom: 0;
    padding-bottom: 0;
  }
`

const AppsList2 = styled(AppsList)`
  @media (min-width: 768px) {
    padding-left: 0;
  }
  @media (max-width: 767px) {
    margin-top: 0;
    padding-top: 0;
  }
`



const itemsApps1 = [
  'Testing requirements',
  `Test documentation compilation and approval (test policy and strategy, test plan, test metrics and etc)`,
  'Compilation and coordination of product acceptance criteria and system requirements for software development (acceptance and system test case design)',
]

const itemsApps21 = [
  'Prototype testing',
  'Usability testing',
  'Cross-browser testing',
  'Cross-platform testing' 
]

const itemsApps31 = [
  'Unit testing',
  'Integration testing',
  'Functional testing',
  'Interoperability testing',
  'Suitability testing',
  'Security testing'
]
const itemsApps32 = [
  'Non-functional testing',
  'Portability testing',
  'Reliability testing',
  'Maintainability testing',
  'Performance testing',
  'Black / white box testing'
]

const itemsApps41 = [
  'Smoke testing',
  'Acceptance testing',
  'Installation testing',
  'Post release support'
]

const Description = () => (
  <BaseContentSection showTopMarker>
    {/* <P noTopPadding>
      Orion Alliance employs over 100 staff who specialise in QA consulting and testing for mobile and web applications. With a strong focus on automation and performance testing we help you release high quality products sooner so you can meet your business objectives and be more competitive in your field. 
    </P>
    <P>
      The company uses state-of-the-art tools and automated technology to quickly assess, identify and resolve any kind of software-related issue. Our expert team will pay close attention to the specific needs of your product and provide you with detailed testing reports so you know exactly what needs to be resolved. 
    </P>
    <P>
      From performance testing to security, functionality, usability and compatibility, Orion Alliance will make sure each aspect of your software functions exactly the way it should before release.
    </P> */}
    <P noTopPadding>
      QA & testing are an integral part of any product development. Correctly configured QA & testing processes give confidence that your product works stable and in accordance with the requirements of the business.
    </P>
    <P>
      Orion Alliance employs over 100 staff who specialise in QA consulting and testing for mobile and web applications. With a strong focus on automation and performance testing we help you release high quality products sooner so you can meet your business objectives and be more competitive in your field. 
    </P>
    <P>
      We have learned to customize the testing processes of any type application, allowing to get the maximum consistent quality at minimal cost. We are ready to offer testing services separately and as part of the software development process.
    </P>
    <P>
      Following the standards of ISO 9126 and ISTQB analysis techniques our QA team offer approaches and combinations of various types of testing:  from the requirements gathering and product design to automation tools selection for the most effective testing of your product. According to the test results, we provide a full package of documentation that meets IEEE 829 standards.  
    </P>
    <P>
      Orion Alliance offers the following types of software testing, depending on the stage of application development:
    </P>
    <AppsListsContainer>
      <AppsList header="Planning and Analysis" items={itemsApps1}/>
    </AppsListsContainer>
    <AppsListsContainer>
      <AppsList1 header="UI/UX Design"  items={itemsApps21}/>
    </AppsListsContainer>
    <AppsListsContainer>
      <AppsList1 header="Development process"  items={itemsApps31}/>
      <AppsList2 items={itemsApps32} />
    </AppsListsContainer>
    <AppsListsContainer>
      <AppsList1 header="Release"  items={itemsApps41}/>
    </AppsListsContainer>
  </BaseContentSection>
)

export default Description;
