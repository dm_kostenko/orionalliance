import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  padding-bottom: 0;
  z-index: 10;
`

const tileWidths = [
  ['80%', '80%', '80%'],
  ['80%', '40%', '40%'],
  ['80%', '40%', '40%'],
  ['80%', '45%', '40%'],
  ['80%', '45%', '40%']
]

const Solutions = () => (
  <Section>
    <BaseSectionHeader
      text2='Q&A Testing'
      text1='Solutions'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator/>
    <DarkTiles
      items={tilesData}
      tileWidths={tileWidths}
    />
  </Section>
)

export default Solutions;