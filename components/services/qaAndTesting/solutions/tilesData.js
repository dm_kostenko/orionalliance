export default [
  {
    imgUrl: '/img/services_page/qa_and_testing/t1.png',
    heading: 'Automated Testing',
    text: [
      'Save time and money on manual testing with an automated testing service. Our team is fully equipped with the latest automation testing software to perform testing at multiple stages of development. ',
      'By going above and beyond the capabilities of manual testing, we vastly increase your test coverage and ensure the highest quality mobile, web and desktop application regardless of hardware configurations and operating systems. ' 
        + 'Our automated testing facility is overseen by experienced technicians who regularly maintain the testing applications and keep them up to date.'
    ],
  },
  {
    imgUrl: '/img/services_page/qa_and_testing/t2.png',
    heading: 'Compatibility Testing',
    text: 'Keep your software running the way it should in any computing environment. By putting your application through real-world tests we make sure your software is compatible with any browser, mobile device, network (3G, 4G and WIFI), hardware configuration, and operating system (Windows, OSX and Linux).',
  },
  {
    imgUrl: '/img/services_page/qa_and_testing/t3.png',
    heading: 'Performance Testing',
    text: 'Our team carries out a range of tests to determine the speed and efficiency of your application. These stress tests involve applying various loads of different capacities to find out how stable the program is, the maximum load it can handle, and how quickly it responds to user-inputs.',
  },
  {
    imgUrl: '/img/services_page/qa_and_testing/t4.png',
    heading: 'Security Testing',
    text: 'We have over 10 years of experience protecting software from current and potential future security threats. By closely reviewing the source code, the team can locate weak points or flaws in the code that may expose the software to attacks from third-parties. From there we can clean up the code – while preserving the internal processes – to remove vulnerabilities from the software.',
  },
  {
    imgUrl: '/img/services_page/qa_and_testing/t5.png',
    heading: 'Usability Testing',
    text: 'Find out how easy and intuitive it is to use your software. Our team of experts reach out to real-world users and ask them to perform various tasks to figure out any issues they have with the program. These results are given to you and you get recommendations on how we can improve the user-experience for you.',
  },
]
