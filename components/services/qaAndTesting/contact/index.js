import Contact from 'components/contact';


const headerProps = {
  text2: 'contact Us',
  reverse: true,
}

const text = 'Orion Alliance is available 24/7 to discuss your testing requirements. '

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)