import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';

const items = [
  'Integration architecture design',
  'Latest cloud-based integration solutions',
  'Compliant with stringent industry standards',
  'PoC',
  'Product estimation',
  'ESB/BPM consulting',
  'ESB/SOA implementation',
  'Business process and infrastructure optimisation',
  'Capacity planning for middleware',
  'Integration testing'
]

const Choose = () => [
  <BaseContentSection key='2' noTopPadding>
    <BaseSectionHeader
      text2='Smart App Integration Solutions'
      wrapText
      textAlign='left'
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Our highly skilled integration experts can assess your current IT 
      infrastructure and provide an intuitive EAI solution that combines 
      all of your essential services into one place. 
      By looking at the bigger picture of your business, we make sure 
      the data being sent from each platform is consistent across the board 
      and transfers smoothly from one place to another.
    </P>
    <P>
      Based on your exact business needs, we can provide the following 
      solutions:
    </P>
    <ULSimple items={items}/>
    <P>
      Even after your EAI solution is up and running, you can request 24/7 
      customer support at any time for immediate advice and support.
    </P>
  </BaseContentSection>
]

export default Choose;