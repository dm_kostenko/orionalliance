import styled from 'styled-components';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Streamline the flow of communication between departments',
  'Reduce costly and unnecessary downtime',
  'Increase response times for customer enquiries',
  'Simplify IT infrastructure',
  'Automate internal business processes between platforms',
  'And much more…',
]

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Imagine you had the power to collect your data from various systems 
      within your business – from Corporate Email to the Cloud and Third-Party 
      Applications, CRM, ECM and ERPs – and consolidate all of that data into a 
      secure dashboard your staff could access, share and adjust in real-time.
    </P>
    <P>
      With over 10 years of experience in Enterprise App Integration (EAI) 
      solutions we have the skills and knowledge to equip your business with a 
      custom EAI solution to suit your current and future business needs.
    </P>
    <P>
      We work closely with small to large enterprises across a range of 
      industries to consolidate their crucial data into a secure, scalable and 
      easy to use dashboard. We offer an end-to-end solution for all of your 
      enterprise app integration (EAI) needs from Assessment and Design to 
      Planning, Deployment, Testing and Support.
    </P>
    <P>
      By making it easier to exchange crucial information this allows you to: 
    </P>
      <ULSimple items={items} />
    <P>
      No matter what industry you’re involved in, we can ensure the migration 
      process goes smoothly and your current legacy systems are left 
      uncompromised and work as originally intended.
    </P>
  </BaseContentSection>
)

export default Description;