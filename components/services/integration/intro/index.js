import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/enterprise_integretion/intro_bg.jpg'; 
const topText = 'Enterprise App Integration Services';
const bottomText = 'Orion Alliance provides custom enterprise app integration solutions for businesses of any size in the Banking, Healthcare, Manufacturing and other sectors.';

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;