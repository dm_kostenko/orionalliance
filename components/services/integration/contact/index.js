import Contact from 'components/contact';

const headerProps = {
  text1: ' Today',
  text2: 'Streamline Your Internal Processes',
  reverse: true,
  wrapText: true,
  inline: true,
}

const text = 'Find out what an enterprise integration can do for your business.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)