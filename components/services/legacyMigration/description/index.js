import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Keep up with the technological demands of tomorrow?',
  'Meet (and exceed) the expectations of your customers?',
  'Run perfectly without the need for costly hardware maintenance?',
  'Help give your business a competitive advantage?',
  'Be compatible with the latest software applications and operating system versions?',
  'Protect itself from new, advanced and intelligent cyber-security threats?',
]

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Do you have 100% confidence your existing IT infrastructure can:
    </P>
    <ULSimple items={items} />
    <P>
      If you answered ‘No’ to any of those questions, then you need stay ahead of the competition by updating your legacy systems.
    </P>
  </BaseContentSection>
)

export default Description;
