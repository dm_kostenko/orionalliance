import Contact from 'components/contact';

const headerProps = {
  text2: 'Upgrade Your',
  text1: 'IT Infrastructure',
  reverse: true,
}

 



const text = 'Find out how Orion Alliance can upgrade your software and hardware to the latest technology.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)