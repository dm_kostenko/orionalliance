import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/legacy_migration/intro_bg.jpg'; 
const topText = 'Legacy Migration Services';
const bottomText = 'Modernise your legacy system, preserve your existing data and internal processes with risk-mitigated legacy migration services from Orion Alliance.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;