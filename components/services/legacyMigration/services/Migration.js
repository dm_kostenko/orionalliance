import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';


const Section = styled.div`

`

const Migration = () => (
  <Section>
    <BaseSectionHeader
      text2='What is Legacy Migration?'
      noTopPadding
    />
    <HeaderSeparator/>
    <P>
      Many enterprises run their business on legacy systems
      that – while they still serve their intended purpose and ‘get the job done’ – have become obsolete due to the presence
      of newer operating systems, hardware and enterprise applications.
    </P>
    <P>
      Maintaining these legacy systems can not only hinder productivity
      and fail to meet the expectations of customers, but they can be exposed to security threats and cost you a fortune in maintenance costs.
    </P>
    <P>
      Legacy migration services involve upgrading your outdated software and hardwire
      while transferring existing data to a new system without compromising the data itself or changing the internal processes of the organisation.
    </P>
  </Section>
)

export default Migration;