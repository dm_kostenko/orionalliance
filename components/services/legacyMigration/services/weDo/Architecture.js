import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Roadmap resources and services',
  'Develop code migration strategy',
  'Data mapping',
  
]

const Architecture = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/legacy_migration/i2.png'
      heading='Define New Architecture'
    />
    <P>
      Based on the results of the project scope, we define the roadmap of your new IT system and how it fits in with your current organisation processes.
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Architecture;