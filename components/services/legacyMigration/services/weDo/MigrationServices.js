import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'Build new architecture',
  'Design new data model',
  'Verify system complies with all requirements',
  'Perform testing',
  'Deploy the new architecture and maintain'
  
]

const MigrationServices = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/legacy_migration/i4.png'
      heading='Migration Services'
    />
    <P>
      Orion Alliance will make every effort to ensure the legacy migration goes smoothly and with minimal impact on your day-to-day business activities. 
    </P>
    <P>
      This process involves the following steps:
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default MigrationServices;