import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const items = [
  'The key objectives of the business',
  'Technical requirements to overcome',
  'Risk assessment',
  'Preferred time of delivery',
  'Maximum budget',
  'Other essential details in relation to the migration'

]

const Assessment = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/legacy_migration/i1.png'
      heading='Assessment'
    />
    <P>
      We take the time to plan your legacy migration strategy. By performing a comprehensive inspection of your existing system, we can identify the shortfalls of the current system and focus on key areas to be improved upon.
    </P>
    <P>
      Based on the current landscape of your IT infrastructure we define the following details:
    </P>
    <ULSimple items={items}/>
  </Article>
)

export default Assessment;