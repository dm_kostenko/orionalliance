import Article from './Article';
import IconedHeading from 'elementals/IconedHeading';
import P from 'elementals/P';

const DataMigration = () => (
  <Article>
    <IconedHeading
      iconSrc='/img/services_page/legacy_migration/i3.png'
      heading='Data Migration'
    />
    <P>
      Data preservation is an important aspect of the migration process. It can also be very complex. This is because data stored on older systems may have originated from software that is no longer compatible with newer operating systems, programs and infrastructures. 
    </P>
    <P>
      We can perform a variety of data migration types to suit your needs from storage migration to cloud migration and application migration. 
    </P>
  </Article>
)

export default DataMigration;