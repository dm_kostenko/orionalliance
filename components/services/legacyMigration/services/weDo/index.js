import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';

import Assessment from './Assessment';
import Architecture from './Architecture';
import DataMigration from './DataMigration';
import MigrationServices from './MigrationServices';

const Section = styled.div`
  padding-top: 3rem;
`

const WeDo = () => (
  <Section>
    <BaseSectionHeader
      text2='What We Do'
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      Orion Alliance can assess your current legacy system and migrate these services over to more powerful, secure and efficient IT infrastructure.
    </P>
    <P>
      Rest assured you’re guided through each step of the journey – from assessment to defining the new architecture,
      migrating and testing – so you can be confident the process will go smoothly and with minimal impact on day-to-day business activities.
    </P>
    <P>
      Below is a simply step-by-step process on how the legacy migration service works:
    </P>
    <Assessment />
    <Architecture />
    <DataMigration />
    <MigrationServices />
  </Section>
)

export default WeDo;