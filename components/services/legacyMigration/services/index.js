
import BaseContentSection from 'elementals/BaseContentSection';
import Migration from './Migration';
import WeDo from './weDo';

const Services = () => (
  <BaseContentSection lightGray>
    <Migration />
    <WeDo />
  </BaseContentSection>
)

export default Services;