import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/enterprise/intro_bg.jpg'; 
const topText = 'Enterprise App Development Services';
const bottomText = 'For over 10 years our company has transformed creative '
   + 'enterprise app ideas into practical solutions for the Banking, '
   + 'Healthcare, Financial and other sectors.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;