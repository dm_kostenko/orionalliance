import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';

const SectionHeader = styled(BaseSectionHeader)`
  & > span {
    text-align: left;
  }
  @media (min-width: 768px) {
    padding-right: 3rem;
  }
`

const itemsEnt = [
  'Manage core business processes in real-time',
  'Manage and maintain physical assets in your organisation',
  'Record and process accounting transactions',
  'Automatically send and receive electronic bills',
  'Interact with current and potential customers',
  'Securely manage and record critical data of your enterprise',
  'And so much more…'
]

const GuidingListsContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const GuidingList = styled(ULSimple)`
  @media (max-width: 767px) {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media (min-width: 992px) {
    padding-right: 3.25rem;
  }
`

const GuidingList1 = styled(GuidingList)`
  
  @media (max-width: 767px) {
    margin-bottom: 0;
    padding-bottom: 0;
  }
`

const GuidingList2 = styled(GuidingList)`
  @media (min-width: 768px) {
    padding-left: 0;
  }
  @media (max-width: 767px) {
    margin-top: 0;
    padding-top: 1rem;
  }
`

const itemsGuiding1 = [
  'Mobile application development services',
  'Cross-platform solutions',
  'Messaging and integration',
  'Product training',
  'Consulting, support and customisation'
]

const itemsGuiding2 = [
  'Service oriented architecture (SOA)',
  'User-friendly interfaces',
  'Rapid business prototyping',
  'Advanced security measures <span class="light">– multiple factor authentication, HTTPS encryption and secure firewalls etc.</span>',
]

const ListItemComponent = styled(ULSimple.ListItemElement)`
  & > span.light {
    font-weight: 300;
  }
`

const Choose = () => [
  <BaseContentSection key='1' lightGray>
    <SectionHeader
      text2='Enterprise Apps'
      text1='We Develop'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      Our 100+ strong team of expert developers have over 10 years of experience creating innovative apps for iOS, Android and Windows-based platforms.
    </P>
    <P>
      No matter how big or small your business,
      Orion Alliance can work closely with you to plan, design, integrate, and release a custom enterprise app to help your business reach soaring new heights.
    </P>
    <P>
      Imagine what your business could achieve with an enterprise application development service that lets you:
    </P>
    <ULSimple items={itemsEnt} />
  </BaseContentSection>,
  <BaseContentSection key='2'>
    <SectionHeader
      text2='Guiding You to Success'
      text1='From Start to Finish'
      reverse
      wrapText
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      From the very first meeting, our experienced mobile app developers will determine the scope of your project
      and provide a custom solution to suit your needs and budget.
      Every aspect of your project is meticulously planned, discussed and structured – from establishing business objectives to meeting user-expectations,
      identifying growth opportunities and scalability – so the development cycle goes smoothly and the project is delivered on time, on budget.
    </P>
    <P>
      Everything you need is right here, including:
    </P>
    <GuidingListsContainer>
      <GuidingList1 items={itemsGuiding1} />
      <GuidingList2 items={itemsGuiding2} itemComponent={ListItemComponent}/>
    </GuidingListsContainer>
    <P>
      Even if you’ve not had success with enterprise apps in the past, we will show you a better way.
    </P>
  </BaseContentSection>
]

export default Choose;