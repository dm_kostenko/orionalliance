import Contact from 'components/contact';


const headerProps = {
  text1: 'Today',
  text2: 'Build Better Apps',
  reverse: true,
}

const text = 'We’ll be happy to define the scope of your project and help move your vision forward in the right direction.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)