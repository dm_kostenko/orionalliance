import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Mobile technology is changing the way small and large enterprises conduct business.
    </P>
    <P>
      No longer are employees bound to working stationary on their desktop computers or laptops.
      These days it’s common for an entire workforce to be constantly connected to the world on their mobile devices.
      Why not take advantage of this opportunity and make it work in favour of your business?
    </P>
    <P>
      By giving your team access to flexible,
      easy to use and reliable enterprise applications, you have the potential to help your business grow by being more productive,
      innovative and scalable.
    </P>
    <P>
      Of course, you first need to figure out what kind of problems your staff are facing, and how an enterprise application can make their job easier.
    </P>
  </BaseContentSection>
)

export default Description;
