import styled from 'styled-components';
import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';

const Section = styled(BaseSection)`
  padding-top: 2rem;
  padding-bottom: 3rem;
  z-index: 0;
  @media (min-width: 768px) {
    padding-top: 12rem;
    padding-bottom: 8rem;
  }
`

const Choose = () => (
  <Section>
    <BaseSectionHeader
      text1='Orions Alliance'
      text2='Why Choose'
      reverse
    />
    <HeaderSeparator/>
    <P>
      At Orion Alliance, you get the technical support you need to keep your
      applications working beautifully anytime, anywhere. Our team of industry
      experts are available 24/7 to ensure your application is more effective,
      efficient and reliable.
    </P>
    <P>
      We provides onsite and offshore application support for any kind of
      enterprise, web and mobile application. From testing and debugging to
      application deployment, documentation updates, database maintenance,
      performance analysis and real-time monitoring – we do it all.
    </P>
    <P>
      Whether you have a big or small technical problem, it’s easy to connect
      with one of our IT experts and get prompt, friendly and reliable support
      when you need it most. Rest assured you can expect a fast response
      time from us as we have over 100 staff located in 3 different offices
      who are ready to help.
    </P>
  </Section>
)

export default Choose;
