import styled from 'styled-components';
import H1 from 'elementals/H1';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import StyledPropsHelper from 'utils/StyledPropsHelper';
import IntroSectionComplexImage from 'elementals/introSection/IntroSectionComplexImage';

const BannerSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  top: 1rem;
  width: 100%;
  padding: ${props => props.theme.bannerMargin};
  text-align: left;

  @media (min-width: 768px) {
    flex-direction: row;

    padding: ${props => props.theme.bannerMarginNarrowMobile};

    padding-bottom: 0;
  }

  @media (min-width: 992px) {
    padding: ${props => props.theme.bannerMarginNarrow};

    padding-bottom: 0;
  }
`;

const LeftSide = styled.div`
  position: relative;
  flex: 1 0 auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    padding-top: 0.375rem;
    align-items: stretch;
    flex: 1 0 10rem;
  }
`;

const RightSide = styled.div`
  position: relative;
  flex: 1 0 10rem;
  text-align: justify;
  padding: 0 ${props => props.theme.sectionMarginLeftRightMobile};
  @media (min-width: 768px) {
    padding: 0 0 0 3rem;
  }
`;

const BottomLeftContainer = styled.div`
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  padding-left: 8vw;
  font-size: 1rem;
  @media (min-width: 768px) {
    align-items: stretch;
    padding-left: 0;
    border-right: 1px solid rgba(255, 255, 255, 0.1);
  }
`;

const BannerH1 = styled(H1)`
  color: ${props => props.theme.introBannerColors[0]};
  font-weight: 700;
  padding-left: 8vw;
  @media (min-width: 768px) {
    padding-left: 0;
    border-right: ${StyledPropsHelper.getThemeProp('borderWidth')} solid white;
  }
`;

const LeftTextElement = styled(P)`
  color: ${props => props.theme.textColorGray};
  max-width: 84vw;

  @media (min-width: 768px) {
    max-width: 60vw;
    padding-right: 30%;
  }
`;

const DescriptionElement = styled(P)`
  color: ${props => props.theme.textColorGray};
`;

const LeftHeaderSeparator = styled(HeaderSeparator)`
  margin-top: 1rem;
`;

const BannerImg = styled(IntroSectionComplexImage)`
  @media (min-width: 768px) {
    display: none;
  }
`;

const Banner = () => (
  <BannerSection>
    <LeftSide>
      <BannerH1
        dangerouslySetInnerHTML={{
          __html:
            'Application&nbsp;Support&nbsp;&<br/>Maintenance&nbsp;Services',
        }}
      />
      <BottomLeftContainer>
        <LeftHeaderSeparator altColor />
        <LeftTextElement>
          Connect with Orion Alliance’s 24/7 support team today for reliable
          ongoing application maintenance, monitoring and support.
        </LeftTextElement>
      </BottomLeftContainer>
      <BannerImg url='/img/services_page/application_maintenance/intro.jpg' />
    </LeftSide>
    <RightSide>
      <DescriptionElement noTopPadding>
        Are you launching a new product onto the market? Do you need to update
        your existing application?
      </DescriptionElement>
      <DescriptionElement>
        While it’s important to have a great product on launch, you need to
        perform regular maintenance to be sure your application can provide the
        best possible user-experience, protect itself against security threats,
        and work flawlessly on the latest devices and operating systems.
      </DescriptionElement>
      <DescriptionElement>
        However, the cost of hiring an in-house technical support team can be
        expensive. For this reason, many businesses are turning to offshore
        application maintenance to increase productivity and reduce labours
        costs.
      </DescriptionElement>
    </RightSide>
  </BannerSection>
);

export default Banner;
