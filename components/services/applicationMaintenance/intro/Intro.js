import React from 'react';
import styled from 'styled-components';
import IntroSectionComplex from 'elementals/introSection/IntroSectionComplex';
import IntroSectionComplexImage from 'elementals/introSection/IntroSectionComplexImage';

import Banner from './Banner';

const BannerImg = styled(IntroSectionComplexImage)`
  display: none;
  @media (min-width: 768px) {
    display: block;
  }
`;

const Intro = () => (
  <IntroSectionComplex backgroundImgUrl='/img/services_page/application_maintenance/intro_bg.jpg'>
    <Banner />
    <BannerImg url='/img/services_page/application_maintenance/intro.jpg' />
  </IntroSectionComplex>
);

export default Intro;
