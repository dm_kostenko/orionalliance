import styled, { css } from 'styled-components';

import BaseSection from 'elementals/BaseSection';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ParallaxWrapper from 'elementals/ParallaxWrapper';

import StyledPropsHelper from 'utils/StyledPropsHelper';

import featuresData from './featuresData';
import withPathPrefix from 'utils/withPathPrefix';

const Section = styled(BaseSection)`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 0;
  z-index: 10;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

const LeftSide = styled.div`
  position: relative;
  flex: 0 0 auto;
  @media (min-width: 768px) {
    background-color: #f7f7f7;
    flex: 1 0 50%;
    top: 1.5rem;
  }
`;

const WindowOverlay = styled.div`
  position: absolute;
  background: rgba(0, 0, 0, 0.5);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  ${props =>
    props.withBorder &&
    css`
      border-right: ${StyledPropsHelper.getThemeProp('borderWidth')} solid
        rgba(255, 255, 255, 0.2);
      &::after {
        content: '';
        z-index: 100;
        position: absolute;
        bottom: 0;
        right: -${StyledPropsHelper.getThemeProp('borderWidth')};
        height: 1rem;
        border-right: ${StyledPropsHelper.getThemeProp('borderWidth')} solid
          white;
      }
    `}
`;

const LeftWindow = styled.div`
  position: relative;
  overflow: hidden;
  height: 15rem;
  display: none;
  @media (min-width: 768px) {
    display: block;

    height: 10rem;
  }
`;

const RightSide = styled.div`
  position: relative;
  flex: 0 0 auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    flex: 1 0 50%;
  }
`;

const RightWindow = styled.div`
  display: none;
  position: relative;
  height: 15rem;
  overflow: hidden;
  z-index: 10;

  @media (min-width: 768px) {
    display: block;
  }
`;

const Img = styled.div`
  display: none;
  position: absolute;
  width: 100%;
  height: 300%;
  transform: translateY(-40%);
  background-image: url(${props => withPathPrefix(props.url)});
  background-size: 200% auto;
  @media (min-width: 768px) {
    display: block;
  }
`;

const ImgLeft = styled(Img)`
  background-position: center left;
`;

const ImgRight = styled(Img)`
  background-position: center right;
  top: 2rem;
`;

const MaintenanceArticle = styled.div`
  padding: 1rem ${props => props.theme.sectionMarginLeftRightMobile};

  @media (min-width: 768px) {
    padding-top: 3rem;
    padding-bottom: 3rem;
    padding-right: 0;
  }
  @media (min-width: 992px) {
    padding-left: ${props => props.theme.sectionMarginLeftRight};
  }
`;

const FeatureList = styled.ul`
  position: relative;
  flex: 1 0 auto;
  top: 1.5rem;
  padding: 2rem 4vw;
  margin: 0 4vw;
  list-style: none;
  background-color: ${props => props.theme.altBackgroundColor};
  font-weight: 300;

  @media (min-width: 768px) {
    top: 0;
    margin: 0;
    padding: 2rem;
    box-shadow: 0 -1rem 10rem -3.5rem #000;
  }
`;

const FeatureListItem = styled.li`
  position: relative;
  color: ${props => props.theme.textColorAlt};
  padding-left: 2rem;
  &:not(:first-child) {
    margin-top: 1rem;
  }

  &::before {
    content: '';
    position: absolute;

    width: 1rem;
    height: 1rem;
    left: 0;
    top: 0.2rem;
    opacity: 0.7;
    background: url(${withPathPrefix('/img/services_page/application_maintenance/ul_icon.png')})
      no-repeat left top;
    background-size: 1rem 1rem;

    background-clip: 1;
  }
`;

const features = featuresData.map((featureText, index) => (
  <FeatureListItem key={index}>{featureText}</FeatureListItem>
));

const StyledP = styled(P)`
  padding-right: 2rem;
`

const AppServices = () => (
  <Section>
    <LeftSide>
      <LeftWindow>
        <ParallaxWrapper
          offsetYMin='-100%'
          offsetYMax='100%'
          styleOuter={{
            position: 'relative',
            height: '15rem',
          }}
          styleInner={{
            position: 'relative',
            height: '15rem',
          }}
        >
          <ImgLeft url='/img/services_page/application_maintenance/services_bg.jpg' />
        </ParallaxWrapper>
        <WindowOverlay withBorder />
      </LeftWindow>
      <MaintenanceArticle>
        <BaseSectionHeader
          text1='Services'
          text2='Application Maintenance'
          reverse
        />
        <HeaderSeparator />
        <StyledP>
          From the moment you get in touch with Orion Alliance, your trusted IT
          support network will take the time to understand your needs and help
          you overcome any challenge you face. With over 10 years of experience
          in application support and maintenance, we can assist you
          with&nbsp;&#8594;
        </StyledP>
      </MaintenanceArticle>
    </LeftSide>
    <RightSide>
      <RightWindow>
        <ParallaxWrapper
          offsetYMin='-100%'
          offsetYMax='100%'
          styleOuter={{
            position: 'relative',
            height: '15rem',
          }}
          styleInner={{
            position: 'relative',
            height: '15rem',
          }}
        >
          <ImgRight url='/img/services_page/application_maintenance/services_bg.jpg' />
        </ParallaxWrapper>
        <WindowOverlay />
      </RightWindow>
      <FeatureList>{features}</FeatureList>
    </RightSide>
  </Section>
);

export default AppServices;
