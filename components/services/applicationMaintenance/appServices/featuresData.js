export default [
  'Real-time troubleshooting and bug fixes',
  'Pre-release testing',
  'Source code migration',
  'Code review and deployment of patches',
  'Performance optimisation',
  'Maintenance and upgrades for Internet of Things (IoT) devices',
  'Application deployment',
  'Document management',
]