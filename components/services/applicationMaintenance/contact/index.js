import Contact from 'components/contact';


const headerProps = {
  text1: 'Today',
  text2: 'Connect with Us',
  reverse: true,
}

const text = 'For immediate advice and support, connect with one of our application experts'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)