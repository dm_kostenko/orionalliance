export default [
  {
    imgUrl: '/img/services_page/it_outstaffing/t1.png',
    heading: 'A simple analysis and optimization of the deployment scheme in AWS',
    text: 'Often during the initial deployment, excessive resources are laid for performing individual tasks as part of the software package. For example, scheduled service tasks do not need constantly raised capacities. It is possible to transfer them to separate functions launched by AWS on a schedule. Or, for example, storing metrics and logs with a low access may be significantly reduced by automated posting of them to repositories with suitable storage and access conditions. ',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t2.png',
    heading: 'An in-depth analysis of the deployment scheme in AWS, installation of profiling tool at critical points, statistics collection. Architecture optimization for specific types of load',
    text: 'We offer to perform profiling optimization – to identify redundancy points by analyzing the loads in various operating modes of the system. After collecting statistics from control points, the determination of the actual load on the machines provided by the services is possible. Then we establish the necessary power of computing nodes, taking into account reasonable redundancy. This method allows to identify bottlenecks in the system, reducing the downtime of the remaining components. ',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t3.png',
    heading: 'Adaptive changes of deployment configuration according to the modes of cloud operation',
    text: 'The load in the systems may have cyclical nature. After analyzing and identifying these modes, it is possible to adaptively change the allocated resources depending on the expected or actual mode of system operation.',
  },
  {
    imgUrl: '/img/services_page/it_outstaffing/t4.png',
    heading: 'Analysis and optimization of your software solutions for optimal deployment',
    text: 'Even different components within a single service can provide different types of system load. It is important to find a compromise between the services connection and their functionality fragmentation. A more accurate classification and splitting or merging of services allow to distinguish classes according to specific load profiles and distribute them to the corresponding AWS resources in terms of power. In addition, we can optimize the service code of the system, if there is no other way to improve system performance.',
  },
]