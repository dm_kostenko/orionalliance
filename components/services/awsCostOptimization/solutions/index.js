import styled from 'styled-components';
import BaseContentSection from 'elementals/BaseContentSection';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';

const Section = styled(BaseContentSection)`
  padding-bottom: 0;
  padding-top: 0;
  z-index: 10;
`

const tileWidths = [
  ['80%', '80%', '40%'],
  ['80%', '80%', '40%'],
  ['80%', '80%', '40%'],
  ['80%', '80%', '40%']
]

const Solutions = () => (
  <Section>
    <DarkTiles
      items={tilesData}
      tileWidths={tileWidths}
      notLast={true}
    />
  </Section>
)

export default Solutions;