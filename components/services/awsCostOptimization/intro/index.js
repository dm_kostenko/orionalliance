import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/services_page/devops/intro_bg.jpg'; 
const topText = 'AWS Cost Optimization';
const bottomText = 'Optimize your cloud for greater productivity.'

const props = {
  backgroundImgUrl,
  topText,
  bottomText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;