import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';

const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      Have you made a transition to cloud solutions? It was promised that the costs will be reduced and you will pay only for the resources that you use? But in reality, things are different: costs are rising, but productivity, flexibility and usability remain at the same level? Are you familiar with this situation? So many questions, no real solutions… But not for us!
    </P>
    <P>
      Our experts are ready to analyze your cloud architecture and to offer options for your AWS costs optimization.
    </P>
  </BaseContentSection>
)

export default Description;
