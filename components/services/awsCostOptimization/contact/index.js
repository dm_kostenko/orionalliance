import Contact from 'components/contact';

const headerProps = {
  text1: ' Today',
  text2: 'Optimize your AWS costs',
  reverse: true,
  wrapText: true,
  inline: true,
}

const text = 'Find out how Orion Alliance can help with AWS cost optimization.'

export default () => (
  <Contact
    headerProps={headerProps}
    text={text}
  />
)