export default [
  {
    imgUrl: '/img/services_page/aws_cost_optimization/t1.png',
    text: 'Our client had several RDS database instances as a part of its architecture in 4 regions. Using a peering connection, we connected one RDS instance in one region to other ones, this allowed to avoid unnecessary RDS instances. Thus, the costs of maintaining the databases in AWS were reduced without loss of productivity and stability of the whole system.',
  },
  {
    imgUrl: '/img/services_page/aws_cost_optimization/t2.png',
    text: 'Another story is about the wrong types of EC2 instances. One of our customers had many separate services running on expensive EC2 instances. After analyzing the load, it turned out that most of the series stood idle much of the time. Orion Alliance specialists decided to transfer part of the services to ECS, and completely abandon the other part, transferring their logic to AWS Lambda. Such actions have reduced costs by 40%.',
  }
]
