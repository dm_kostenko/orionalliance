import styled from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';
import HeaderSeparator from 'elementals/HeaderSeparator';
import tilesData from './tilesData';
import FlippingTiles from 'elementals/FlippingTiles';

const Section = styled(BaseContentSection)`
  z-index: 10;
  padding-top: 0;
  padding-bottom: ${props => props.last ? '0' : 'inherited'};
`

const Examples = () => (
  <Section last>
    <BaseSectionHeader
      text2="AWS Cost Optimization"
      text1='Examples of simple steps towards effective'
      textAlign='left'
    />
    <HeaderSeparator/>
    <FlippingTiles
      items={tilesData}
      tileWidths={['80%', '40%', '40%']}
      notLast
    />
    <P>
      Orion Alliance specialists know how to reduce your costs and save time. We can guide you through the appropriate strategy and analysis method for the elasticity, efficiency, and resilience of the AWS cloud.
    </P>
  </Section>
)

export default Examples;