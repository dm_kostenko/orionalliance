import BaseContentSection from 'elementals/BaseContentSection';
import P from 'elementals/P';






const Description = () => (
  <BaseContentSection showTopMarker>
    <P noTopPadding>
      At Orion Alliance we believe the key to a successful business is choosing the right people. Why? Because people are the greatest asset a company can have. After all, behind the face of every great company there are people who work every day to get results and make a real difference to people’s lives. 
    </P>
    <P>
      For this reason, we invest in people who share our passion for technology and have the desire to help others achieve great things in business.
    </P>
  </BaseContentSection>
)

export default Description;
