import ReactMarkdown from 'react-markdown';
import styled from 'styled-components';
import P from 'elementals/P';
import H1 from 'elementals/H1';
import H2 from 'elementals/H2';
import H3 from 'elementals/H3';
import H4 from 'elementals/H4';
import H5 from 'elementals/H5';
import H6 from 'elementals/H6';
import ULSimple from 'elementals/ULSimple';

const Container = styled.div`
  padding: 3rem ${props => props.theme.sectionMarginLeftRightMobile};

  @media (min-width: 992px)  {
    padding: 3rem ${props => props.theme.sectionMarginLeftRight};
  }

  color: ${props => props.theme.textColor};

  & H1,
  & H2,
  & H3,
  & H4,
  & H5,
  & H6 {
    margin-top: 1.5rem;
    font-weight: 700;
  }
  & ul {
    margin-top: 1.5rem;
  }

  & > *:first-child {
    padding-top: 0;
    margin-top: 0;
  }

  &:nth-of-type(even) {
    background-color: ${props => props.theme.lightGrayBGColor};
  }

  & ul > li {
    font-weight: 300;
  }
`

//const MD

const Heading = (props) => {
  let Component;
  switch(props.level) {
    case 1: {
      Component = H1;
      break;
    }
    case 2: {
      Component = H2;
      break;
    }
    case 3: {
      Component = H3;
      break;
    }
    case 4: {
      Component = H4;
      break;
    }
    case 5: {
      Component = H5;
      break;
    }
    case 6: {
      Component = H6;
      break;
    }
  }
  return <Component {...props}/>;
}

const renderers = {
  paragraph: P,
  heading: Heading,
  list: ULSimple,
  listItem: ULSimple.ListItemElement,
}

const Vacancy = ({source}) => (
  <Container>
    <ReactMarkdown 
      source={source}
      renderers={renderers}
    />
  </Container>
)

export default Vacancy;