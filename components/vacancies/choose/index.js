import styled, { css } from 'styled-components';
import BaseSectionHeader from 'elementals/BaseSectionHeader';
import BaseContentSection from 'elementals/BaseContentSection';
import HeaderSeparator from 'elementals/HeaderSeparator';
import P from 'elementals/P';
import ULSimple from 'elementals/ULSimple';
import VacanciesList from './VacanciesList';

const Section = styled(BaseContentSection)`
  z-index: 10;
`

const VacanciesSection = styled(Section)`
  padding: 0;
`

const List = styled(ULSimple)`
  padding: 0;
  background-color: ${props => props.theme.mainBackgroundColor};
`

const ListItemComponent = styled(ULSimple.ListItemElement)`
  font-weight: 300;
`

const Label = styled.label`
  color: ${props => props.theme.textColor};
  font-weight: 300;
  padding-top: 1em;
  font-style: italic;
`

const ErrorLabel = styled(Label)`
  color: ${props => props.theme.errorTextColor};

`
const OpportunitiesHeader = styled(BaseSectionHeader)`
  padding: 3rem ${props => props.theme.sectionMarginLeftRightMobile} 0;

  @media (min-width: 992px)  {
    padding: 3rem ${props => props.theme.sectionMarginLeftRight} 0;
  }
`



const Choose = ({ vacancies }) => [
  <Section key='1' lightGray>
    <BaseSectionHeader
      text2='Why Orion Alliance'
      reverse
      noTopPadding
    />
    <HeaderSeparator />
    <P>
      When you join us, you become part of a fast-paced dynamic environment where you will apply your skills and knowledge in the real world, overcome tough obstacles, and advance your IT career in a way that suits you. We offer generous and flexible remuneration packages to bring you financial security and the opportunity to achieve a sustainable work/life balance that suits your needs.
    </P>
    <P>
      Currently our team is made up of 100+ IT specialists who provide a range of software development skills. These include experts in software development, testing, support, outsourcing, legacy system migration and enterprise application integration.
    </P>
    <P>
      If you possess any of these skills and have the drive to succeed – we want to hear from you.
    </P>
  </Section>,
  <VacanciesSection key='2'>
    <OpportunitiesHeader
      text2='Current Opportunities'
      reverse
    />
    {/*<HeaderSeparator />*/}
    {!!(vacancies && vacancies.data && vacancies.data.length) &&
      <VacanciesList data={vacancies.data} />
      /*<List 
        items={vacancies.data}
        itemComponent={ListItemComponent} 
      />*/
    }
    {vacancies && !vacancies.error && (!vacancies.data || !vacancies.data.length) &&
      <Label>Currently there are no vacancies</Label>
    }
    {vacancies && vacancies.error &&
      <ErrorLabel>{vacancies.error}</ErrorLabel>
    }
  </VacanciesSection>
]

export default Choose;