import Vacancy from './Vacancy';


const VacanciesList = ({data}) => (
  data.map((item, index) => (
    <Vacancy key={index} source={item}/>
  ))
)

export default VacanciesList;