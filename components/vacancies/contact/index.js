import Contact from 'components/contact';

const headerProps = {
  text2: 'contact us',
  reverse: true,
}



export default () => (
  <Contact
    headerProps={headerProps}
    text={'Apply now to become part of our big family'}
  />
)