import IntroSectionSimple from 'elementals/introSection/IntroSectionSimple';

const backgroundImgUrl = '/img/vacancies_page/intro_bg.jpg'; 
const topText = 'Working for Orion Alliance';

const props = {
  backgroundImgUrl,
  topText,
}

const Intro = () => (
  <IntroSectionSimple
    {...props}
  />
)

export default Intro;