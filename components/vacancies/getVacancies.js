import loadVacancies from 'shared/loadVacancies';
import withPathPrefix from 'utils/withPathPrefix';

const error = 'Cannot load vacancies: server error';

const getErrorText = async (res) => {
  if (res.status === 500) {
    return await res.text();
  }
  return error;
}

export default async () => {
  if (process.browser) {
    try {
      const response = await fetch(withPathPrefix('/get_vacancies'), {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      });

      const { ok, status } = response;

      if (ok && status === 200) {
        const data = await response.json();
        return { data };
      } else {
        return { error: await getErrorText(response) };
      }
    } catch (e) {
      return { error };
    }
  } else {
    const res = loadVacancies();
    return res;
  }
}