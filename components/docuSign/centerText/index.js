import styled from 'styled-components';
import H1 from 'elementals/H1';
import BaseContentSection from 'elementals/BaseContentSection';


const Section = styled(BaseContentSection)`
  z-index: 10;
  text-align: center;
  margin: 0;
  margin-top: 30%;
  text-align: center;
  @media (min-width: 768px) {
    margin-top: 5%;
  }
`

const Heading = styled(H1)`
  text-transform: capitalize;
  font-weight: 700;
  color: ${props => props.theme.elementColor};
  font-size: 2.25rem;
  @media (min-width: 768px) {
    margin-top: 5rem;
  }
`

const CenterText = () => (
    <Section>
        <Heading>
            You're all done
        </Heading>
    </Section>
  )
  
  export default CenterText;