import React from 'react';
import styled from 'styled-components';
import IntroSectionComplex from 'elementals/introSection/IntroSectionComplex';
import H1 from 'elementals/H1';


const Section = styled(IntroSectionComplex)`
  height: auto;
  min-height: 30rem !important;
  text-align: center;
  align-items: center;
`;


const Heading = styled(H1)`
  text-transform: capitalize;
  font-weight: 700;
  color: white;
  font-size: 2.25rem;
  width: 100%;
  z-index: 10;
`

const Intro = () => (
  <Section backgroundImgUrl='/img/about_us_page/intro_bg.jpg'>
    <Heading>
        You're all done!
    </Heading>
  </Section>
);

export default Intro;
