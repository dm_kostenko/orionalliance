import React from 'react';
import PublishSubscribe from 'utils/PublishSubscribe';

const RESIZE = 'resize';
const SCROLL = 'scroll';
const CLICK = 'click';
const MOUSEOVER = 'mouseover';

const allEventTypes = [RESIZE, SCROLL, CLICK, MOUSEOVER];

const windowEvents = {
  subscribe: PublishSubscribe.subscribe.bind(PublishSubscribe),
  unsubscribe: PublishSubscribe.unsubscribe.bind(PublishSubscribe),
};

const WindowEventsContext = React.createContext(windowEvents);

const addListeners = eventTypes =>
  eventTypes.map(eventType => {
    const listener = PublishSubscribe.publish.bind(PublishSubscribe, eventType);
    window.addEventListener(eventType, listener);
    return () => window.removeEventListener(eventType, listener);
  });

const removeListeners = cleaners => cleaners.forEach(cleaner => cleaner());

const WindowEvents = ({ children }) => {
  React.useEffect(() => {
    const cleaners = addListeners(allEventTypes);
    return () => removeListeners(cleaners);
  }, []);

  return (
    <WindowEventsContext.Provider value={windowEvents}>
      {children}
    </WindowEventsContext.Provider>
  );
};

WindowEvents.displayName = 'WindowEvents';
WindowEvents.RESIZE = RESIZE;
WindowEvents.SCROLL = SCROLL;
WindowEvents.CLICK = CLICK;
WindowEvents.MOUSEOVER = MOUSEOVER;
/*
function withWindowEvents(WrappedComponent) {
  const WithWindowEvents = props => (
    <WindowEventsContext.Consumer>
      {windowEvents => (
        <WrappedComponent {...props} windowEvents={windowEvents} />
      )}
    </WindowEventsContext.Consumer>
  );

  const wrappedComponentName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component';

  WithWindowEvents.displayName = `withWindowEvents(${wrappedComponentName})`;
  return WithWindowEvents;
}*/

export { WindowEvents as default, WindowEventsContext };
