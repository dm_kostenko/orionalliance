import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';
import companyInfo from '../../data/companyInfo.json';
import {
  netherlandsForAboutUs,
  latviaForAboutUs,
  polandForAboutUs,
} from '../../utils/formatters';

const netherlands = netherlandsForAboutUs(
  companyInfo.address[0],
  companyInfo.email
);
const latvia = latviaForAboutUs(companyInfo.address[1], companyInfo.email);
const poland = polandForAboutUs(companyInfo.address[2], companyInfo.email);

const Section = styled.div`
  display: flex;
  flex-direction: column;
  background-image: url(${withPathPrefix('/img/about_us_page/adr_bg_mob.jpeg')});
  background-size: cover;
  background-position: 100% center;
  background-repeat: no-repeat;

  font-size: 0.8rem;

  @media (min-width: 768px) {
    flex-direction: row;
    background-image: url(${withPathPrefix('/img/about_us_page/adr_bg.jpeg')});
    background-position: center 100%;
    padding: 1.5rem 0;
  }
`;

const BaseAddressContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: stretch;

  flex: 1 0 30%;

  padding: 1.5rem ${props => props.theme.sectionMarginLeftRightMobile};

  border-top: 1px solid ${props => props.theme.addressBorders[1]};
  border-bottom: 1px solid ${props => props.theme.addressBorders[0]};
  @media (min-width: 768px) {
    padding: 1.5rem 0;

    justify-content: center;
    border-top: none;
    border-bottom: none;
    border-left: 1px solid #fbfbfc;
    border-right: 1px solid #e0e0e1;
  }
`;

const FirstAddressContainer = styled(BaseAddressContainer)`
  border-top: none;

  @media (min-width: 768px) {
    flex-basis: 33%;
    padding-left: ${props => props.theme.sectionMarginLeftRightMobile};
    border-left: none;
    border-right-color: #e6e6e7;
    justify-content: flex-start;
  }
  @media (min-width: 992px) {
    padding-left: ${props => props.theme.sectionMarginLeftRight};
  }
`;

const LastAddressContainer = styled(BaseAddressContainer)`
  border-bottom: none;
  @media (min-width: 768px) {
    flex-basis: 33%;
    padding-right: ${props => props.theme.sectionMarginLeftRightMobile};
    border-right: none;
    border-left-color: #f9f9fa;
    justify-content: flex-end;
  }
  @media (min-width: 992px) {
    padding-right: ${props => props.theme.sectionMarginLeftRight};
  }
`;

const Address = styled.address`
  display: flex;
  flex-direction: row;
  font-style: normal;
  flex: 1 0 auto;

  @media (min-width: 768px) {
    flex-direction: column;
    flex: none;
  }
`;

const Country = styled.div`
  flex: 1 0 1rem;
  text-transform: uppercase;
  color: ${props => props.theme.sectionHeaderColor1};
  font-weight: 700;
  line-height: 1.5;
`;

const Misc = styled.div`
  font-weight: 300;
  flex: 1 0 1rem;
  color: ${props => props.theme.footerTextColor};
`;

const Phone = styled.a`
  &,
  &:hover {
    color: ${props => props.theme.footerTextColor};
    text-decoration: underline;
  }
`;

const Email = Phone;

const Addresses = () => (
  <Section>
    <FirstAddressContainer>
      <Address>
        <Country>{netherlands.country}:</Country>
        <Misc>
          {netherlands.address}
          <br />
          <Phone
            href={netherlands.hrefPhoneNumber}
            dangerouslySetInnerHTML={{ __html: netherlands.displayPhoneNumber }}
          />
          <br />
          <Email
            href={poland.hrefEmail}
            dangerouslySetInnerHTML={{ __html: poland.displayEmail }}
          />
        </Misc>
      </Address>
    </FirstAddressContainer>

    <BaseAddressContainer>
      <Address>
        <Country>{latvia.country}:</Country>
        <Misc>
          {latvia.address}
          <br />
          <Phone
            href={latvia.hrefPhoneNumber}
            dangerouslySetInnerHTML={{ __html: latvia.displayPhoneNumber }}
          />
          <br />
          <Email
            href={poland.hrefEmail}
            dangerouslySetInnerHTML={{ __html: poland.displayEmail }}
          />
        </Misc>
      </Address>
    </BaseAddressContainer>

    <LastAddressContainer>
      <Address>
        <Country>{poland.country}:</Country>
        <Misc>
          {poland.address}
          <br />
          <Phone
            href={poland.hrefPhoneNumber}
            dangerouslySetInnerHTML={{ __html: poland.displayPhoneNumber }}
          />
          <br />
          <Email
            href={poland.hrefEmail}
            dangerouslySetInnerHTML={{ __html: poland.displayEmail }}
          />
        </Misc>
      </Address>
    </LastAddressContainer>
  </Section>
);

export default Addresses;
