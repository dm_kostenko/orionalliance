export default [
  {
    imgUrl: '/img/about_us_page/t1.png',
    heading: '100+ Talented Staff',
    text: 'At Orion Alliance we believe in the power of good people who work hard to achieve the best results. ' +
      'We employ over 100 staff who excel in the fields of software development, design, testing, support, outsourcing, ' +
      'legacy system migration, and enterprise application integration.' +
      'Every day our highly skilled staff works with business owners like yourself to explore new ideas, solve unique problems and deliver successful outcomes.',
  },
  {
    imgUrl: '/img/about_us_page/t2.png',
    heading: '3 Development Centres,<br/> 3 Different Countries',
    text: 'Our specialists operate across Europe with offices located in Latvia, The Netherlands and Poland. We provide 24/7 onsite and offshore services to clients around the world. ' +
      'No matter which branch you connect with, you can expect the highest level of customer service, knowledge and technical support across a range of fields.',
  },
  {
    imgUrl: '/img/about_us_page/t3.png',
    heading: '10+ Years of Experience',
    text: 'For over 10 years we have stayed informed on the latest technological trends and kept up-to-date on industry specific standards. ' +
      'With repeat business from clients across a range of industries, we know exactly what kind of methods work in specific scenarios and how to employ those practices into real-world applications.',
  },
]
