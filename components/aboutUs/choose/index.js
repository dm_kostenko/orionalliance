import styled from 'styled-components';
import BaseContentSection from 'elementals/BaseContentSection';
import tilesData from './tilesData';
import DarkTiles from 'elementals/DarkTiles';
import H2 from 'elementals/H2';

const Section = styled(BaseContentSection)`
  padding-bottom: 0;
  z-index: 10;
  @media (min-width: 768px) {
    padding-top: 10.5rem;
    
  }
`

const getColors = (start, end) => (
  DarkTiles.getColorsDefault(start, end, 5).slice(1, 4)
)

const Heading = styled(H2)`
  text-transform: uppercase;
  font-weight: 700;
  color: ${props => props.theme.elementColor};
  font-size: 1.125rem;

  @media (min-width: 768px) {
    margin-top: 1.5rem;
  }
`

const WhyMigrate = () => (
  <Section>
    <DarkTiles
      items={tilesData}
      getColors={getColors}
      headingComponent={Heading}
    />
  </Section>
)

export default WhyMigrate;