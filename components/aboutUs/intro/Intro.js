import React from 'react';
import styled from 'styled-components';
import IntroSectionComplex from 'elementals/introSection/IntroSectionComplex';
import IntroSectionComplexImage from 'elementals/introSection/IntroSectionComplexImage';

import Banner from './Banner';

const Section = styled(IntroSectionComplex)`
  height: auto;
  min-height: 30rem !important;
`;

const BannerImg = styled(IntroSectionComplexImage)`
  display: none;
  @media (min-width: 768px) {
    display: block;
  }
`;

const Intro = () => (
  <Section backgroundImgUrl='/img/about_us_page/intro_bg.jpg'>
    <Banner />
    <BannerImg url='/img/about_us_page/intro.jpg' />
  </Section>
);

export default Intro;
