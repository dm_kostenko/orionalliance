import styled from 'styled-components';
import H1 from 'elementals/H1';
import P from 'elementals/P';
import StyledPropsHelper from 'utils/StyledPropsHelper';
import IntroSectionComplexImage from 'elementals/introSection/IntroSectionComplexImage';

const BannerSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  top: -1rem;
  width: 100%;
  padding: ${props => props.theme.bannerMargin};
  text-align: left;
  padding-top: 8rem;

  @media (min-width: 768px) {
    flex-direction: row;

    padding: ${props => props.theme.bannerMarginNarrowMobile};
    padding-bottom: 0;
    padding: 8rem 8vw 2rem;
  }
`;

const LeftSide = styled.div`
  position: relative;
  flex: 1 0 auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    padding-top: 0.375rem;
    align-items: stretch;
    flex: 1 0 10rem;
  }
`;

const RightSide = styled.div`
  position: relative;
  flex: 1 0 10rem;
  text-align: justify;
  padding: 0 ${props => props.theme.sectionMarginLeftRightMobile};
  @media (min-width: 768px) {
    padding: 0 0 0 3rem;
  }
`;

const BottomLeftContainer = styled.div`
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  padding-left: 8vw;
  @media (min-width: 768px) {
    padding-left: 0;
    align-items: stretch;
    border-right: 1px solid rgba(255, 255, 255, 0.1);
  }
`;

const BannerH1 = styled(H1)`
  color: ${props => props.theme.introBannerColors[0]};
  font-weight: 700;
  padding-left: 8vw;
  @media (min-width: 768px) {
    padding-left: 0;
    border-right: ${StyledPropsHelper.getThemeProp('borderWidth')} solid white;
  }
`;

const LeftTextElement = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
  max-width: 84vw;

  @media (min-width: 768px) {
    max-width: 60vw;
    padding-right: 20%;
  }
`;

const DescriptionElement = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
`;

const BannerImg = styled(IntroSectionComplexImage)`
  @media (min-width: 768px) {
    display: none;
  }
`;

const Banner = () => (
  <BannerSection>
    <LeftSide>
      <BannerH1 dangerouslySetInnerHTML={{ __html: 'About Us' }} />
      <BottomLeftContainer>
        <LeftTextElement>
          Orion Alliance is a leading software development company who provides
          onsite and offshore services in Latvia, The Netherlands and Poland.
        </LeftTextElement>
        <LeftTextElement>
          Founded in 2007, Orion Alliance works closely with ambitious clients
          to overcome challenging tasks and scale new heights.
        </LeftTextElement>
      </BottomLeftContainer>
      <BannerImg url='/img/about_us_page/intro.jpg' />
    </LeftSide>
    <RightSide>
      <DescriptionElement noTopPadding>
        Our goal is to help businesses of any size or industry harness the full
        potential of information technology.
      </DescriptionElement>
      <DescriptionElement>
        For the past 10 years we have adopted the latest technology and industry
        practices to deliver outstanding results in the field of software
        design, testing, support, outsourcing, legacy system migration and
        enterprise application integration.
      </DescriptionElement>
      <DescriptionElement>
        Our growing team has extensive experience across a broad range of
        industries. This level of flexibility means we can overcome specific
        issues with custom solutions tailored to your needs. From Banking to
        Hospitality, Travel and Leisure, Industrial Manufacturing, Insurance,
        Oil and Gas, Retail, Transport and Utilities – we have a solution for
        you.
      </DescriptionElement>
    </RightSide>
  </BannerSection>
);

export default Banner;
