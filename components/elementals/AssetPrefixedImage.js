import React from 'react'
import omit from 'lodash/omit'
import withPathPrefix from 'utils/withPathPrefix';

export default class AssetPrefixedImage extends React.Component {
  render() {
    const {src, ...rest} = this.props;
    return (
      <img
        src={withPathPrefix(src)}
        {...omit(rest, 'submitting')}
      />
    )
  }
}
