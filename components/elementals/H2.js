import styled from 'styled-components';

const H2 = styled.h2`
  font-size: 1.45em;
  font-weight: 400;
  line-height: 1.2;
  @media (min-width: 768px) {
    font-size: 1.6em;
  }
`

export default H2;
