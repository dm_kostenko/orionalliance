import styled from 'styled-components';

const H1 = styled.h1`
  font-size: 1.7rem;
  font-weight: 400;
  line-height: 1.2;
  @media (min-width: 768px) {
    font-size: 2rem;
  }
`

export default H1;
