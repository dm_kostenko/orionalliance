import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames'

class NavbarCollapse extends React.Component {
  static contextTypes = {
    $bs_navbar: PropTypes.shape({
      bsClass: PropTypes.string,
      expanded: PropTypes.bool
    })
  };

  render() {
    const navbarProps = this.context.$bs_navbar || { bsClass: 'navbar' };
    const { children } = this.props;

    return(
      <div 
        className={
          cn(
            'navbar-collapse',
            'collapse',
            { in: navbarProps.expanded },
          )
        }
      >
        {children}
      </div>
    )
  }
}

export default NavbarCollapse;