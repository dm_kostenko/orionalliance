import { withRouter } from 'next/router';
import URLPrefixedLink from 'elementals/URLPrefixedLink';

const MenuItemLink = props => {
  const { children, router, href, onSelect, prefetch } = props;

  /*const handleClick = () => {
    onSelect();
  }*/

  const active = router.pathname === href;

  return (
    <li className={active ? 'active' : null} onClick={onSelect}>
      <URLPrefixedLink href={href} prefetch>
        <a title={children}>{children}</a>
      </URLPrefixedLink>
    </li>
  );
};

export default withRouter(MenuItemLink);
