import React from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from 'next/router';
import BootstrapNavDropdown from 'react-bootstrap/lib/NavDropdown';
import cn from 'classnames';
import autobind from 'autobind-decorator';

class NavDropdown extends React.PureComponent {

  static getDerivedStateFromProps(props, state) {
    const {
      router,
      children,
    } = props;
    
    const { pathname } = router;
    let active = false;
    for (const child of React.Children.toArray(children)) {
      const { href } = child.props; 
      if (href && pathname.indexOf(href) > -1) {
        active = true;
        break;
      }
    }
    /*const active = partialUrlActive ?
      router.pathname.indexOf(href) === 0 :
      router.pathname === href;*/
    return { active };
  }

  constructor(...args) {
    super(...args);
    this.state = {
      open: false,
      active: false,
    }

    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleMouseOut = this.handleMouseOut.bind(this)
    this.handleToggle = this.handleToggle.bind(this)
    
    this.ref = React.createRef();
  }

  removeHref() {
    if (!this.props.href) {
      const anchorElement = ReactDOM.findDOMNode(this.ref.current).firstChild;
      anchorElement.removeAttribute('href');
    }
  }

  componentDidMount() {
    if (this.props.onToggle) {
      this.props.onToggle(this.props.id, this.state.open);
    }
    this.removeHref();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.open !== prevState.open && this.props.onToggle) {
      this.props.onToggle(this.props.id, this.state.open);
    }
    this.removeHref();
  }

  // @autobind
  handleMouseOver() {
    if (this.props.openEvent === 'mouseOver') {
      this.setState({ open: true });
    }
  }

  // @autobind
  handleMouseOut() {
    if (this.props.openEvent === 'mouseOver') {
      this.setState({ open: false });
    }
  }

  // @autobind
  handleToggle(open, e, eventSource) {
    if (
      this.props.openEvent === 'click' ||
      eventSource.source === 'select'
    ) {
      this.setState({open});
    }
  }

  render() {
    const {
      children,
      href,
      router,
      partialUrlActive,
      openEvent,
      ...rest
    } = this.props;

    const {
      open,
      active,
    } = this.state;

    return (
      <BootstrapNavDropdown
        ref={this.ref}
        {...rest}
        className={cn(
          { active },
          { 'expand-menu' : openEvent === 'mouseOver'},
        )}
        onToggle={this.handleToggle}
        open={open}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseOut}
      >
        {children}
      </BootstrapNavDropdown>
    )
  }
}

export default withRouter(NavDropdown);
