import styled from 'styled-components';
import NavbarToggle from 'react-bootstrap/lib/NavbarToggle';
import omit from 'lodash/omit';
import withPathPrefix from 'utils/withPathPrefix';

const StyledNavbarToggle = styled(NavbarToggle)`
  width: 1em;
  height: 1em;
  padding: 1em;
  background-image: url(${props => withPathPrefix(props.url)});
  background-size: 1em;
  background-position: center;
  background-repeat: no-repeat;
`;

const BarsToggle = props => (
  <StyledNavbarToggle
    {...omit(props, 'expanded')}
    url={props.expanded ? '/img/close.svg' : '/img/bars.svg'}
  >
    <span />
  </StyledNavbarToggle>
);

BarsToggle.defaultProps = {
  expanded: false,
};

export default BarsToggle;
