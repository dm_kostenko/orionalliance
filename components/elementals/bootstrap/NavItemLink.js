import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';
import autobind from 'autobind-decorator';
import styled from 'styled-components';
import URLPrefixedLink from 'elementals/URLPrefixedLink';
import NavItem from './NavItem';

const StyledNavItem = styled(NavItem)`
  cursor: pointer;
`;

class NavItemLink extends React.Component {
  constructor(...args) {
    super(...args);
    this.ref = React.createRef();
    this.handleAnchorClick = this.handleAnchorClick.bind(this)
    this.handleClick = this.handleClick.bind(this)

  }

  // @autobind
  handleClick() {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick(...arguments);
    }
    this.context.$bs_navbar.onSelect();
  }

  // @autobind
  handleAnchorClick(e) {
    this.props.preventDefault && e.preventDefault();
  }

  removeHref() {
    if (!this.props.href) {
      const anchorElement = ReactDOM.findDOMNode(this.ref.current).firstChild;
      anchorElement.removeAttribute('href');
    }
  }

  componentDidMount() {
    this.removeHref();
  }

  componentDidUpdate() {
    this.removeHref();
  }

  render() {
    const { children, router, href, prefetch } = this.props;
    if (!href) {
      return (
        <StyledNavItem ref={this.ref} onClick={this.props.onClick}>
          {children}
        </StyledNavItem>
      );
    }

    const active = router.pathname === href;

    return (
      <li className={active ? 'active' : null} onClick={this.handleClick}>
        <URLPrefixedLink href={href} prefetch={prefetch}>
          <a title={children} onClick={this.handleAnchorClick}>
            {children}
          </a>
        </URLPrefixedLink>
      </li>
    );
  }
}

NavItemLink.contextTypes = {
  $bs_navbar: PropTypes.object,
};

export default withRouter(NavItemLink);
