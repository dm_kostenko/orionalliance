import Navbar from 'react-bootstrap/lib/Navbar';
import BarsToggle from './BarsToggle';
import NavbarCollapse from './NavbarCollapse';

Navbar.Toggle = BarsToggle;
Navbar.Collapse = NavbarCollapse;

export default Navbar;