import React from 'react';
import styled, { css } from 'styled-components';
import H2 from './H2';

const SectionHeaderElement = styled(H2)`
  position: relative;
  display: flex;
  flex: 1 0 auto;
  flex-wrap: wrap;
  padding: ${props => props.theme.sectionHeadingPadding};
  ${props => props.noTopPadding && css`
    & {
      padding-top: 0;
    }
  `}
  margin-left: ${props => props.marginLeft};
  text-align: 'center';
`

const Font = styled.span`
  white-space: ${props => props.wrapText ? 'normal' : 'nowrap'};
  text-transform: uppercase;

  ${props => props.addWhiteSpace &&
      css`
        ::after {
          content: ' ';
          display: inline-block;
          width: 0.3em;
        }
      `
  }
`

const Font1 = styled(Font)`
  font-weight: 300;
  color: ${props => props.theme.sectionHeaderColor2};
`

const Font2 = styled(Font)`
  font-weight: 700;
  color: ${props => props.theme.sectionHeaderColor1};
`

const BaseSectionHeader = ({ 
  text1, 
  text2, 
  reverse, 
  className, 
  children, 
  wrapText, 
  inline, 
  noTopPadding,
  marginLeft
}) => (
  <SectionHeaderElement marginLeft={marginLeft} className={className} noTopPadding={noTopPadding}>
    {
      reverse ?
        (
          <React.Fragment>
            <Font2 wrapText={wrapText} addWhiteSpace={text2 && text1}>
              {text2}

              {inline ? <Font1 wrapText={wrapText} >{text1}</Font1> : null}
            </Font2>
            {inline ? null : <Font1 wrapText={wrapText} >{text1}</Font1>}
          </React.Fragment>
        ) :
        (
          <React.Fragment>
            <Font1 wrapText={wrapText} addWhiteSpace={text1 && text2}>
              {text1}

              {inline ? <Font2 wrapText={wrapText}>{text2}</Font2> : null}
            </Font1>
            {inline ? null : <Font2 wrapText={wrapText} >{text2}</Font2>}
          </React.Fragment>
        )
    }
    {children}
  </SectionHeaderElement>
)


export default BaseSectionHeader;