import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const ListElement = styled.ul`
  padding: ${props => props.tiles ? '0 0rem' : '1.6rem 3.25rem'};
  margin-top: ${props => props.tiles ? '0rem' : '1.5rem'};
  margin-bottom: ${props => props.tiles ? '0' : '0.5rem'};
  width: 100%;
  color: ${props => props.theme.textColor};
  background-color: ${props => props.tiles ? 'transparent' : props.theme.darkGrayBGColor};
  font-style: italic;
  list-style: none;
  @media (min-width: 992px) {
    //padding-right: 7rem;
  }
`

const ListItemElement = styled.li`
  line-height: ${props => props.tiles ? '1.5rem' : '2.5rem'};
  padding-left: 2rem;
  background: url(${withPathPrefix('/img/industries_page/banking/ul_icon.png')}) no-repeat left top;
  background-size: 1rem 1rem;
  padding-top: ${props => props.tiles ? '1rem' : ''};
  background-position-y: ${props => props.tiles ? '1.2rem' : '0.75rem'};
  font-weight: ${props => props.tiles ? '400' : '700'};
  font-size: ${props => props.tiles ? '0.9em' : '' };
  color: ${props => props.tiles ? props.theme.textColorGray : ''};
  // font-style: ${props => props.tiles ? 'normal' : ''};

`

const ULSimple = (props) => {
  const {
    items,
    className,
    tiles,
    itemComponent: ListItemComponent = ListItemElement,
    children,
  } = props;

  return (
    <ListElement className={className} tiles={tiles}>
      {
        children ?
          children :
          items.map((item, index) => (
            <ListItemComponent
              key={index}
              dangerouslySetInnerHTML={{ __html: item }}
              tiles={tiles}
            />
          ))
      }
    </ListElement>
  )
}

ULSimple.ListItemElement = ListItemElement;

export default ULSimple;