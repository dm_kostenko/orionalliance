import styled from 'styled-components';

import H1 from 'elementals/H1';
import P from 'elementals/P';

const BannerH2 = styled(H1)`
  color: ${props => props.theme.introBannerColors[0]};
  font-weight: 700;
  max-width: 84vw;
  
  @media (min-width: 768px) {
    max-width: 55vw;
  }
`

const BannerH6 = styled(P)`
  padding-top: 2rem;
  color: ${props => props.theme.introBannerColors[1]};
  max-width: 84vw;
  font-size: 1.125em;
  line-height: 1.2;
  margin-left: ${props => props.bottomRight ? '30rem' : ''};
  @media (min-width: 768px) {
    max-width: 60vw;
  }
  @media (max-width: 665px) {
    margin-left: ${props => props.bottomRight ? '10rem' : ''};
    max-width: 60vw;
  }
`

const Banner = styled.div`
  z-index: 10;
  display: flex;
  flex-direction: column;
  
  padding: ${props => props.theme.simpleBannerMargin};
  @media (min-width: 768px) {
    align-items: flex-start;
    padding: ${props => props.theme.simpleBannerMarginNarrow};
    text-align: left;
  }
`

const IntroSectionSimpleBanner = (props) => {
  const { topText, bottomText, bottomRight, className } = props;
  return (
    <Banner className={className}>
      <BannerH2 dangerouslySetInnerHTML={{__html: topText}}/>
      <BannerH6 noTopPadding bottomRight={bottomRight} dangerouslySetInnerHTML={{__html: bottomText}}/>
    </Banner>
  )
}

IntroSectionSimpleBanner.Banner = Banner;
IntroSectionSimpleBanner.BannerH2 = BannerH2;
IntroSectionSimpleBanner.BannerH6 = BannerH6;

export default IntroSectionSimpleBanner;