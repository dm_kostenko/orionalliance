import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const IntroSectionElement = styled.div`
  position: relative;
  display: block;

  width: 100vw;
`;

const BackgroundContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
  background-color: #151418;
  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center;
  background-size: cover;
`;

const IntroSection = ({ children, className, backgroundImgUrl }) => (
  <IntroSectionElement className={className}>
    <BackgroundContainer url={backgroundImgUrl} />
    {children}
  </IntroSectionElement>
);

export default IntroSection;
