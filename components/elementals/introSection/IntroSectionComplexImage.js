import styled, { css } from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';
import ImageLoadDetector from 'elementals/ImageLoadDetector';

const ImgContainer = styled.div`
  height: 15rem;
  overflow: hidden;
  ${props =>
    props.imageLoaded &&
    css`
      box-shadow: ${props => props.theme.imageBoxShadow};
    `}

  position: relative;
  align-self: stretch;
  margin-right: ${props => props.theme.sectionMarginLeftRightMobile};
  margin-top: 2rem;
  margin-bottom: 2rem;
  @media (min-width: 768px) {
    margin: 0;
    position: absolute;
    right: 50%;
    left: 0;
    bottom: 0;
    transform: translateY(50%);
  }
`;

const Img = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${props => withPathPrefix(props.url)});
  background-position: center;
  background-size: cover;
`;

const IntroSectionComplexImage = ({ url, className }) => (
  <ImageLoadDetector url={url}>
    {imageLoaded => (
      <ImgContainer imageLoaded={imageLoaded} className={className}>
        <Img url={url} />
      </ImgContainer>
    )}
  </ImageLoadDetector>
);

export default IntroSectionComplexImage;
