import styled from 'styled-components';
import PropTypes from 'prop-types';

import IntroSection from './IntroSection';
import IntroSectionSimpleBanner from './IntroSectionSimpleBanner';

const Section = styled(IntroSection)`
  display: flex;
  flex-direction: row;
  align-items: center;
  min-height: 24em;
  @media (min-width: 768px) {
    height: 24em;
  }
`;

const renderChildren = (children, props) =>
  typeof children === 'function' ? children(props) : children;

const IntroSectionSimple = props => {
  const { backgroundImgUrl, topText, bottomText, bottomRight, className, children } = props;

  return (
    <Section className={className} backgroundImgUrl={backgroundImgUrl}>
      {renderChildren(children, { topText, bottomText, bottomRight })}
    </Section>
  );
};

IntroSectionSimple.propTypes = {
  backgroundImgUrl: PropTypes.string,
  topText: PropTypes.string,
  bottomText: PropTypes.string,
  bottomRight: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.any,
};

IntroSectionSimple.defaultProps = {
  children: ({ topText, bottomText, bottomRight }) => (
    <IntroSectionSimpleBanner topText={topText} bottomText={bottomText} bottomRight={bottomRight} />
  ),
};

export default IntroSectionSimple;
