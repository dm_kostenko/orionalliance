import styled from "styled-components";
import IntroSection from "./IntroSection";


const IntroSectionComplex = styled(IntroSection)`
  display: flex;
  flex-direction: row;
  align-items: center;
  z-index: 20;
  min-height: 30em;
  @media (min-width: 768px) {
    height: 32em;
  }
`

export default IntroSectionComplex;