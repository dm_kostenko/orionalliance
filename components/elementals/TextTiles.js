import styled from 'styled-components';
import PropTypes from 'prop-types';
// import TileIcon from './TileIcon';
import StyledPropsHelper from 'utils/StyledPropsHelper';
import H3 from './H3';
import P from './P';
import withPathPrefix from 'utils/withPathPrefix';
import URLPrefixedLink from 'elementals/URLPrefixedLink';
import HeaderSeparator from 'elementals/HeaderSeparator';

const TilesContainer = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  top: -5rem;
  &::after {
    content: '';
    position: absolute;
    z-index: -10;
    bottom: 1rem;
    left: 2rem;
    right: 2rem;
  }
`


const BaseTileElement = styled.div`
  flex: 1 0 ${props => props.widths[0]};
  display: flex;
  flex-direction: column;
  align-items: stretch;
  margin: ${props => props.margin};
  min-width: ${props => props.minWidth};
  max-width: ${props => props.maxWidth};
  background-color: ${props => props.backgroundColor};
  border-radius: 0.5rem;
  @media (max-width: 768px) {
    min-width: 20rem;
    max-width: 30rem;
    margin: 0 15% 10% 15%;
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (max-width: 545px) {
    min-width: 20rem;
    max-width: 30rem;
    margin: 0 15% 10% 10%;
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (max-width: 410px) {
    min-width: 20rem;
    max-width: 30rem;
    margin: 0 15% 10% 5%;
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (max-width: 1074px) {
    // margin: 0;
    align-items:stretch;
    flex: 1 0 ${props => props.widths[2]};
  }
`

const HeaderElement = styled.div`
  display: flex;
  align-items: center;
  height: 12rem;
  background: black !important;
  border: 2px solid ${props => props.borderColor};
  border-bottom: none;
  border-top-left-radius: 0.5rem;
  border-top-right-radius: 0.5rem;
  background: white;
  @media (min-width: 768px) {
    flex-direction: column;
    align-items: flex-start;
  }
  @media (max-width: 545px) {
    height: 10rem;
    flex-direction: column;
    align-items: flex-start;
  }
`

const ImgWrapper = styled.a`
  position: relative;
  line-height: 12rem;
  // flex: 1 0 49%;
  height: 100%;
  width: 100%;
  overflow: hidden;
  border-radius: 0.4rem 0.4rem 0 0;
  margin: 0 ${StyledPropsHelper.getThemeProp('borderWidth')}
    ${StyledPropsHelper.getThemeProp('borderWidth')} 0;
  cursor: pointer;

  @media (min-width: 768px) {
    flex: 1 0 24%;
    height: 18.5vw;
  }
  @media (max-width: 545) {
    flex: 1 0 50%;
    height: 30rem;
    line-height: 30rem;
    background-size: 110%;
  }
  background-image: url(${props => withPathPrefix(props.imgUrl)});
  background-position: center;
  background-repeat: no-repeat;
  background-size: 120%;
  color: transparent;
  opacity: 0.4;

  transition: background-size 0.35s, opacity 0.3s;
  text-align: center;
  vertical-align: middle;

  span {
    color: white;
    border-radius: 3rem;
    padding: 0rem;
    opacity: 0;
    transition: padding 0.3s;
  }

  &::after {
    // content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: white;
    transition: opacity 0.35s;
    opacity: 0;
  }

  &:hover {
    background-image: url(${props => withPathPrefix(props.imgUrl)});
    background-size: 150%;
    opacity: 0.8;
    span {
      background-color: ${props => props.theme.headerSeparatorColor};
      opacity: 1;
      transition: padding 0.3s, opacity 0.5s;
      padding: 1rem;
    }
    &::after {
      opacity: 0.5;
    }
  }
`

const HeadingElement = styled(H3)`
  text-transform: uppercase;
  font-weight: 700;
  color: ${props => props.theme.elementColor};

  @media (min-width: 768px) {
    margin-top: 1.5rem;
  }
`

const Text = styled.div`
  padding: 1rem;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: flex-end;
  height: 57%;
`

const TextHeader = styled(P)`
  font-size: 1.8rem;
  text-transform: uppercase;
  color: ${props => props.theme.textColorAlt};
  margin-bottom: -0.5rem;
  padding: 1rem;
`

const TextElement = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
`

const BaseTile = ({ 
  imgUrl, 
  heading, 
  href,
  text, 
  backgroundColor, 
  widths,
  minWidth,
  maxWidth,
  margin,
  headingComponent,
}) => {
  const Heading = headingComponent || HeadingElement;
  return (
    <BaseTileElement 
      backgroundColor={backgroundColor} 
      widths={widths}
      minWidth={minWidth}
      maxWidth={maxWidth}
      margin={margin}
    >
      <HeaderElement
        borderColor={backgroundColor} 
      >
        <ImgWrapper href={href} imgUrl={imgUrl} passHref>
            <span>READ MORE</span>
        </ImgWrapper>
      </HeaderElement>
      <TextHeader>
        {heading}
      </TextHeader>
      <Text>
        <HeaderSeparator/>
        {
          Array.isArray(text) ?
          text.map((par, i) => <TextElement key={i}>{par}</TextElement>) :
            <TextElement>{text}</TextElement>
        }
      </Text>
    </BaseTileElement>
  )
}

const colorStart = 43;
const colorEnd = 0;

const getColorsDefault = (start, end, length) => {
  if(length === 1)
    return [ '#' + ('00' + Math.round(start).toString(16)).slice(-2).repeat(3) ]
  else if(length === 2)
    return [
      "#2b2b2b",
      "#1d1d1d"
    ]
  const step = (end - start) / (length - 1);
  const res = Array(length);
  for (let i = 0; i < length; i++) {
    res[i] = '#' +
      ('00' + Math.round(start + step * i).toString(16)).slice(-2).repeat(3)
  }
  return res;
}

const getTilesStylesDefault = (length) => {
  let minWidths = [];
  let maxWidths = [];
  let margins = [];
  switch(length % 3) {
    case 0:
      minWidths = new Array(length).fill('10rem')
      maxWidths = new Array(length).fill('22rem')
      margins = new Array(length).fill('0 auto 1rem auto')
      break;
    case 1:
      minWidths = [ '15rem' ]
      maxWidths = [ '30rem' ]
      margins = [ '0 auto 1rem auto' ]
      break;
    case 2:
      if(length === 2) {
        minWidths = new Array(2).fill('10rem')
        maxWidths = new Array(2).fill('25rem')
      }
      else {
        minWidths = new Array(2).fill('10rem')
        maxWidths = new Array(2).fill('22rem')
      }
      margins = [
        '0 0.8rem 1rem auto',
        '0 auto 1rem 0.8rem'
      ]
      break;
    default: 
      break;
  }
  const otherLength = length - (length % 3);
  if(otherLength > 0 && (length % 3 !== 0)) {
    minWidths = [
      ...new Array(otherLength).fill('10rem'),
      ...minWidths
    ]
    maxWidths = [
      ...new Array(otherLength).fill('22rem'),
      ...maxWidths
    ]
    margins = [
      ...new Array(otherLength).fill('0 auto 1rem auto'),
      ...margins
    ]
  }
  return {
    minWidths,
    maxWidths,
    margins
  };
}

const getTileWidths = (widths, i) => (
  typeof widths[0] === 'string' ? widths : widths[i]
)

const TextTiles = (props) => {
  const { className, items, tileWidths, getColors = getColorsDefault, getTilesStyles = getTilesStylesDefault, headingComponent } = props;
  const colors = getColors(colorStart, colorEnd, items.length);
  const { maxWidths, minWidths, margins } = getTilesStyles(items.length);
  return (
    <TilesContainer className={className}>
      {
        items.map((props, index) => (
          <BaseTile
            key={index}
            backgroundColor={colors[index]}
            widths={getTileWidths(tileWidths,index)}
            headingComponent={headingComponent}
            maxWidth={maxWidths[index]}
            minWidth={minWidths[index]}
            margin={margins[index]}
            {...props}
          />
        ))
      }
    </TilesContainer>
  )
}

TextTiles.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  tileWidths: PropTypes.array,
}

TextTiles.defaultProps = {
  tileWidths: ['80%', '40%', '26%']
}

TextTiles.getColorsDefault = getColorsDefault;
TextTiles.getTilesStylesDefault = getTilesStylesDefault;

export default TextTiles;