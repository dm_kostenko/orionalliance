import styled from 'styled-components';

const H6 = styled.h6`
  font-size: 1em;
  font-weight: 300;
  line-height: 1.2;
`

export default H6;
