import Link from 'next/link';
import withPathPrefix from 'utils/withPathPrefix';


const URLPrefixedLink = (props) => {
  const { href, ...rest } = props;
  return (
    <Link
      {...rest}
      href={withPathPrefix(href)}
    />
  )
}

export default URLPrefixedLink;