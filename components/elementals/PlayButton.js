import styled from 'styled-components';

import HighlightInject from './HighlightInject';

const MainButtonElement = styled.button`
  position: absolute;
  right: 35%;
  top: 58%;
  line-height: 1.5;
  margin-top: 2em;
  padding: 0.75em 2em;
  background-color: transparent;
  border:
    ${props => props.theme.borderWidth}
    solid
    ${props => props.theme.mainButtonBorderColor};
  box-shadow:
    0 0 4em 0.3em
    ${props => props.theme.mainButtonShadowColor}
    inset;
  color: ${props => props.theme.mainButtonColor};
  font-size: 1em;
  font-family: Lato, Arial, sans-serif;
  font-weight: 300;

  &:hover {
    box-shadow:
      0 0 4em 0.3em
      ${props => props.theme.mainButtonShadowColor}
      inset,
      0 0 3.5em 0.1em
      ${props => props.theme.mainButtonShadowColor};
  }
`

const PlayButton = ({ children, ...rest }) => (
  <MainButtonElement {...rest}>
    <HighlightInject/>
    {children}
  </MainButtonElement>
)



export default PlayButton;
