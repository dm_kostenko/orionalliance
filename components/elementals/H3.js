import styled from 'styled-components';

const H3 = styled.h3`
  font-size: 1.125em;
  font-weight: 300;
  line-height: 1.2;
`

export default H3;
