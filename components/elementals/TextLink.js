import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';


const TextLink = styled.a.attrs(({href}) => ({
  href: withPathPrefix(href),
}))`
  cursor: pointer;
  &,
  &:visited,
  &:hover {
    color: ${props => props.theme.linkColor};

  }
`

export default TextLink;
