import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const ListElement = styled.ul`
  padding: 1.6rem 3.25rem;
  margin-top: 1.5rem;
  margin-bottom: 0.5rem;
  width: 100%;
  color: ${props => props.theme.textColor};
  background-color: ${props => props.theme.darkGrayBGColor};
  font-style: italic;
  list-style: none;
  @media (min-width: 992px) {
    //padding-right: 7rem;
  }
`

const ListItemElement = styled.li`
  line-height: 2.5rem;
  padding-left: 2rem;
  background: url(${withPathPrefix('/img/industries_page/banking/ul_icon.png')}) no-repeat left top;
  background-size: 1rem 1rem;
  background-position-y: 0.75rem;
  font-weight: 700;
`

const ListHeader = styled.li`
  line-height: 3.5rem;
  font-weight: 1000;
  margin-bottom: 2rem;
  font-size: 2rem;
  font-style: normal;
  white-space: pre;
`

const ULWithHeader = (props) => {
  const {
    header,
    items,
    className,
    itemComponent: ListItemComponent = ListItemElement,
    headerComponent: ListHeaderComponent = ListHeader,
    children,
  } = props;

  return (
    <ListElement className={className}>
      {
        <>
          <ListHeaderComponent>
            {header || " "}
          </ListHeaderComponent>
          {children ?
            children :
            items.map((item, index) => (
              <ListItemComponent
                key={index}
                dangerouslySetInnerHTML={{ __html: item }}
              />
            ))
          }
        </>
      }
    </ListElement>
  )
}

ULWithHeader.ListItemElement = ListItemElement;

export default ULWithHeader;