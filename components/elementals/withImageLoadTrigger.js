import React from 'react';
import autobind from 'autobind-decorator';

const withImageLoadTrigger = (Component) => (
  class ImageLoadTrigger extends React.Component {
    constructor(...args) {
      super(...args);
      this.handleImageLoad = this.handleImageLoad.bind(this)
      this.handleImageRef = this.handleImageRef.bind(this)

      this.state = {
        imageLoaded: false,
      }
    }

    // @autobind
    handleImageLoad() {
      this.setState({ imageLoaded: true })
    }

    // @autobind
    handleImageRef(image) {
      if (image && image.complete && !this.state.imageLoaded) {
        this.handleImageLoad();
      }
    }

    render() {
      return (
        <Component 
          {...this.props}
          imageLoaded={this.state.imageLoaded} 
          onImageLoad={this.handleImageLoad}
          imageRef={this.handleImageRef}
        />
      );
    }
  }

)

export default withImageLoadTrigger