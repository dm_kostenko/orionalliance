import styled from 'styled-components';
import PropTypes from 'prop-types';
import TileIcon from './TileIcon';
import H3 from './H3';
import P from './P';
import withPathPrefix from 'utils/withPathPrefix';
import ULSimple from './ULSimple';


const TilesContainer = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  top: 1.5rem;
  margin-bottom: ${props => props.notLast ? '3rem' : ''};
  text-align: center;
  background: ${props => props.backgroundColor} !important;
  &::after {
    content: '';
    position: absolute;
    z-index: -10;
    bottom: 1rem;
    left: 2rem;
    right: 2rem;
    box-shadow: ${props => props.notLast ? '' : '0 0 12rem 2rem #000'};
  }
`


const Flipper = styled.div`
  position: relative;
  transition: .6s !important;
  transform-style: preserve-3d;
  display: table;
  padding: 3rem;
  padding-top: 0;
  padding-bottom: 2rem;

`

const FlipContainer = styled.div`
  perspective: 1000;
  width: auto;
  height: auto;
  transition: .6s !important;
  margin: auto;
  &:hover ${Flipper}, .hover ${Flipper} {
    transform: rotateY(180deg);
    transition: .6s !important;
  } 
`

const BaseTileElement = styled.div`
  background-color: ${props => props.backgroundColor};

  @media (min-width: 768px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (min-width: 992px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[2]};
  }
`

const Back = styled.div`
  backface-visibility: hidden;
  height: 100%;
  text-align: left;
  vertical-align: middle;
  margin-top: auto;
  margin-bottom: auto;
  transform: rotateY(180deg);
  @media (min-width: 768px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (min-width: 992px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[2]};
  }
`

const Front = styled.div`
  flex: 0 0 auto;
  backface-visibility: hidden;
  position: absolute;
  right: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 2;
  display: table-row;
  margin: 0 auto;

  background-image: url(${props => withPathPrefix(props.imgUrl)});
  background-position: center 100%;
  background-repeat: no-repeat;
  background-size: contain;
  @media (min-width: 768px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (min-width: 992px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[2]};
  }
`



const TextElement = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
  white-space: ${props => props.wrap ? 'normal' : ''};
`


const BaseTile = ({ 
  imgUrl,
  text,
  backgroundColor, 
  widths
}) => {
  return (
    <BaseTileElement backgroundColor={backgroundColor} widths={widths}>
      <FlipContainer>
        <Flipper>
          <Front widths={widths} imgUrl={imgUrl}/>
          <Back widths={widths}>
            {
              Array.isArray(text) ?
              text.map((par, i) => <TextElement key={i}>{par}</TextElement>) :
                <TextElement>{text}</TextElement>
            }
          </Back>
        </Flipper>
      </FlipContainer>
    </BaseTileElement>

  )
}

const colorStart = 43;
const colorEnd = 0;

const getColorsDefault = (start, end, length) => {
  const step = (end - start) / (length - 1);
  const res = Array(length);
  for (let i = 0; i < length; i++) {
    res[i] = '#' +
      ('00' + Math.round(start + step * i).toString(16)).slice(-2).repeat(3)
  }
  return res;
}

const getTileWidths = (widths, i) => (
  typeof widths[0] === 'string' ? widths : widths[i]
)

const FlippingTiles = (props) => {
  const { className, notLast, items, tileWidths, getColors = getColorsDefault, colorsDefault, headingComponent } = props;
  const colors = colorsDefault || getColors(colorStart, colorEnd, items.length);
  return (
    <TilesContainer className={className} notLast={notLast} backgroundColor={colors[0]}>
      {
        items.map((props, index) => (
          <BaseTile
            key={index}
            backgroundColor={colors[index]}
            widths={getTileWidths(tileWidths,index)}
            {...props}
          />
        ))
      }
    </TilesContainer>
  )
}

FlippingTiles.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  tileWidths: PropTypes.array,
}

FlippingTiles.defaultProps = {
  tileWidths: ['80%', '40%', '26%']
}

FlippingTiles.getColorsDefault = getColorsDefault;

export default FlippingTiles;