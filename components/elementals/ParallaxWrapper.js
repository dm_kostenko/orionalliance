import { Parallax } from 'react-scroll-parallax';
import { withAppProps } from 'components/AppPropsContext';

const ParallaxWrapper = ({
  appProps,
  children,
  offsetYMin,
  offsetYMax,
  ...rest
}) => {
  if (Number.isNaN(appProps.minWindowWidth) || appProps.minWindowWidth < 768) {
    offsetYMax = 0;
    offsetYMin = 0;
  }

  return (
    <Parallax {...rest} offsetYMax={offsetYMax} offsetYMin={offsetYMin}>
      {children}
    </Parallax>
  );
};

export default withAppProps(ParallaxWrapper);
