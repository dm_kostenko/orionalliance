import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';

const ListElement = styled.ul`
  padding: 0 5rem;
  margin: 2rem 0 2rem 0;
  width: 100%;
  color: ${props => props.theme.textColor};
  background-color: transparent;
  // font-style: italic;
  list-style: none;
  @media (min-width: 992px) {
    //padding-right: 7rem;
  }
  @media (max-width: 915px) {
    padding: 0 1rem;
  }
  @media (max-width: 600px) {
    padding: 0;
  }
`

const ListItemElement = styled.li`
  line-height: 1.5rem;
  padding-left: 2rem;
  background: url(${withPathPrefix('/img/industries_page/banking/ul_icon.png')}) no-repeat left top;
  background-size: 1rem 1rem;
  background-position-y: 0.4rem;
  padding-top: 0.2rem;
  font-weight: 500;
  // color: ${props => props.tiles ? props.theme.textColorGray : ''};

`

const ULTable = (props) => {
  const {
    items,
    className,
    tiles,
    itemComponent: ListItemComponent = ListItemElement,
    children,
  } = props;

  return (
    <ListElement className={className} tiles={tiles}>
      {
        children ?
          children :
          items.map((item, index) => (
            <ListItemComponent
              key={index}
              dangerouslySetInnerHTML={{ __html: item }}
              tiles={tiles}
            />
          ))
      }
    </ListElement>
  )
}

ULTable.ListItemElement = ListItemElement;

export default ULTable;