import styled from 'styled-components';
import PropTypes from 'prop-types';
import TileIcon from './TileIcon';
import H3 from './H3';
import P from './P';
import withPathPrefix from 'utils/withPathPrefix';
import ULSimple from './ULSimple';


const TilesContainer = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  top: 1.5rem;
  margin-bottom: ${props => props.notLast ? '3rem' : ''};
  &::after {
    content: '';
    position: absolute;
    z-index: -10;
    bottom: 1rem;
    left: 2rem;
    right: 2rem;
    box-shadow: ${props => props.notLast ? '' : '0 0 12rem 2rem #000'};
  }
`

const BaseTileElement = styled.div`
  flex: 1 0 ${props => props.widths[0]};
  display: flex;
  flex-direction: column;
  align-items:stretch;
  padding: 3rem;
  background-color: ${props => props.backgroundColor};

  @media (min-width: 768px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[1]};
  }
  @media (min-width: 992px) {
    align-items:stretch;
    flex: 1 0 ${props => props.widths[2]};
  }
`

const HeaderElement = styled.div`
  display: flex;
  align-items: center;
  @media (min-width: 768px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const ButtonElement = styled(TileIcon)`
  margin: 0;
  background-image: url(${props => withPathPrefix(props.imgUrl)});
  background-position: center center;
  background-repeat: no-repeat;
  height: 6rem;
  padding: 0 3rem;
  margin-right: 2rem;
`

const HeadingElement = styled(H3)`
  text-transform: uppercase;
  font-weight: 700;
  color: ${props => props.theme.elementColor};

  @media (min-width: 768px) {
    margin-top: 1.5rem;
  }
`

const Hr = styled.div`
  background: ${props => props.theme.textColorGray};
  opacity: 0.6;
  height: 1px;
  // width: calc(100% + 3rem);
  // margin-left: -1.5rem;
`

const Br = styled.br`
  margin-top: 2rem;
`

const TextElement = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
  white-space: ${props => props.wrap ? 'normal' : ''};
`

const TextElement2 = styled(P)`
  color: ${props => props.theme.textColorGray} !important;
  white-space: ${props => props.wrap ? 'normal' : ''};
  text-transform: uppercase;
`

const BaseTile = ({ 
  imgUrl, 
  heading, 
  text, 
  preText,
  items,
  backgroundColor, 
  widths,
  headingComponent,
  wrap
}) => {
  const Heading = headingComponent || HeadingElement;
  return (
    <BaseTileElement backgroundColor={backgroundColor} widths={widths}>
      <HeaderElement>
        <ButtonElement
          imgUrl={imgUrl}
          />
        <Heading dangerouslySetInnerHTML={{__html: heading}} />
      </HeaderElement>
      {
        Array.isArray(text) ?
        text.map((par, i) => <TextElement key={i}>{par}</TextElement>) :
          <TextElement wrap={wrap}>{text}</TextElement>
      }
      {preText && items &&
        <>
          <Br/>
          {/* <Hr widths={widths}/> */}
          {
            Array.isArray(preText) ?
            preText.map((par, i) => <TextElement2 key={i}>{par}</TextElement2>) :
              <TextElement2>{preText}</TextElement2>
          }
          <ULSimple tiles={true} items={items} />
        </>
      }
    </BaseTileElement>
  )
}

const colorStart = 43;
const colorEnd = 0;

const getColorsDefault = (start, end, length) => {
  const step = (end - start) / (length - 1);
  const res = Array(length);
  for (let i = 0; i < length; i++) {
    res[i] = '#' +
      ('00' + Math.round(start + step * i).toString(16)).slice(-2).repeat(3)
  }
  return res;
}

const getTileWidths = (widths, i) => (
  typeof widths[0] === 'string' ? widths : widths[i]
)

const DarkTiles = (props) => {
  const { className, notLast, items, tileWidths, getColors = getColorsDefault, colorsDefault, headingComponent } = props;
  const colors = colorsDefault || getColors(colorStart, colorEnd, items.length);
  return (
    <TilesContainer className={className} notLast={notLast}>
      {
        items.map((props, index) => (
          <BaseTile
            key={index}
            backgroundColor={colors[index]}
            widths={getTileWidths(tileWidths,index)}
            headingComponent={headingComponent}
            {...props}
          />
        ))
      }
    </TilesContainer>
  )
}

DarkTiles.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  tileWidths: PropTypes.array,
}

DarkTiles.defaultProps = {
  tileWidths: ['80%', '40%', '26%']
}

DarkTiles.getColorsDefault = getColorsDefault;

export default DarkTiles;