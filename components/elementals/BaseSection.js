import styled, { css } from 'styled-components';

const style = css`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: ${props => props.theme.sectionMarginMobile};
  background-color: ${props => props.theme.mainBackgroundColor};

  @media (min-width: 992px)  {
    padding: ${props => props.theme.sectionMargin};
  }
`

const BaseSection = styled.div`
  ${style}
`

BaseSection.style = style;
  
export default BaseSection;