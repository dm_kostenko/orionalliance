import styled from 'styled-components';



const Link = styled.a`
  cursor: pointer;
  color: ${props => props.altColor ? props.theme.linkColorAlt : props.theme.linkColor};
  
  &:hover {
    color: ${props => props.altColor ? props.theme.linkColorAlt : props.theme.linkColor};
  }
`

export default Link;