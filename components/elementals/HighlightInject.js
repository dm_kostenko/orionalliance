import React from 'react';
import styled from 'styled-components';

const Corner = styled.span`
  
  display: ${props => props.show ? 'inherit' : 'none'};

  @media (min-width: 768px) {
    display: inherit;
  }

  position: absolute;
  
  box-shadow: 0 0 1.5em 0.3em ${props => props.theme.highlightShadowColor};
  background-color: ${props => props.theme.highlightColor};

  ::before {
    content: '';
    position: absolute;
    //background-color: ${props => props.theme.highlightColor};
  }
  ::after {
    content: '';
    position: absolute;
    //background-color: ${props => props.theme.highlightColor};
  }
`

const getSize = props => props.cornersSize;
const getColor = props => props.theme.highlightColor;
const getWidth = props => props.cornersWidth || props.theme.borderWidth;

const TopLeftCorner = styled(Corner)`
  top: 0;
  left: 0; 
  ::before {
    width: ${getSize};
    border-top: ${getWidth} solid ${getColor};
    top: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    left: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
  ::after {
    height: ${getSize};
    border-left: ${getWidth} solid ${getColor};
    top: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    left: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
`

const TopRightCorner = styled(Corner)`
  top: 0;
  right: 0; 
  ::before {
    width: ${getSize};
    border-top: ${getWidth} solid ${getColor};
    top: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    right: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
  ::after {
    height: ${getSize};
    border-right: ${getWidth} solid ${getColor};
    top: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    right: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
`

const BottomLeftCorner = styled(Corner)`
  bottom: 0;
  left: 0; 
  ::before {
    width: ${getSize};
    border-bottom: ${getWidth} solid ${getColor};
    bottom: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    left: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
  ::after {
    height: ${getSize};
    border-left: ${getWidth} solid ${getColor};
    bottom: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    left: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
`

const BottomRightCorner = styled(Corner)`
  bottom: 0;
  right: 0; 
  ::before {
    width: ${getSize};
    border-bottom: ${getWidth} solid ${getColor};
    bottom: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    right: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
  ::after {
    height: ${getSize};
    border-right: ${getWidth} solid ${getColor};
    bottom: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
    right: ${props => `-${props.cornersWidth || props.theme.borderWidth}`};
  }
`

const HighlightInject = ({ cornersSize, cornersWidth, show }) => (
  <React.Fragment>
    <TopLeftCorner 
      cornersSize={cornersSize}
      cornersWidth={cornersWidth}
      show={show}
    />
    <TopRightCorner
      cornersSize={cornersSize}
      cornersWidth={cornersWidth}
    />
    <BottomLeftCorner 
      cornersSize={cornersSize}
      cornersWidth={cornersWidth}
    />
    <BottomRightCorner
      cornersSize={cornersSize}
      cornersWidth={cornersWidth}
    />
  </React.Fragment>
)

HighlightInject.defaultProps = {
  cornersSize: '5px',
}

export default HighlightInject;