import styled from 'styled-components';

const getColor = (props) =>
  props.altColor ? 
    props.theme.headerSeparatorColorAlt :
    props.theme.headerSeparatorColor

const HeaderSeparator = styled.hr.attrs({
  noshade: undefined,
  align: props => props.align ? props.align : 'left',
})`
  width: 1rem;
  border-color: ${getColor};
  color: ${getColor};
  background-color: ${getColor};
  border: solid;
  border-top: none;
  z-index: 10;
  display: ${props => props.hide ? 'none' : 'block'};
`

export default HeaderSeparator;

