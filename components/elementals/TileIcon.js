import styled from 'styled-components';

import HighlightInject from './HighlightInject';

const TileIconElement = styled.i`
  display: block;
  position: relative;
  line-height: 1.5;
  margin-top: 2em;
  padding: 0.75em 2em;
  background-color: transparent;
  border: 
    ${props => props.theme.borderWidth} 
    solid 
    ${props => props.theme.tileIconBorderColor};
  box-shadow: 
    0 0 5em 0.2em 
    ${props => props.theme.tileIconShadowColor} 
    inset;
  
  &:hover {
    box-shadow: 
      0 0 5em 0.2em 
      ${props => props.theme.tileIconShadowColor}  
      inset, 
      0 0 3.5em 0.1em 
      ${props => props.theme.tileIconShadowColor};
  }
`

const TileIcon = ({ children, ...rest }) => (
  <TileIconElement {...rest}>
    <HighlightInject/>
    {children}
  </TileIconElement>
)



export default TileIcon;