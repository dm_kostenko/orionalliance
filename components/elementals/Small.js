import styled from 'styled-components';

const Small = styled.small`
  font-weight: 300;
`

export default Small;
