import styled from 'styled-components';
import withPathPrefix from 'utils/withPathPrefix';
import H3 from './H3';

const IconedHeadingElement = styled.div`
  display: flex;
  align-items: center;
`

const HeaderElement = styled(H3)`
  text-transform: uppercase;
  font-family: Roboto;
  font-weight: 900;
  color: ${props => props.theme.textColor};
`

const IconElement = styled.i`
  margin-right: 2rem;
  width: 3rem;
  height: 3rem;
  background-image: url(${props => withPathPrefix(props.url)});
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
`

const IconedHeading = (props) => {
  const { className, iconSrc, heading } = props;

  return (
    <IconedHeadingElement className={className}>
      <IconElement
        url={iconSrc}
      />
      <HeaderElement>{heading}</HeaderElement>
    </IconedHeadingElement>
  )
}

export default IconedHeading;