import styled from 'styled-components';

const Bold = styled.span`
  font-weight: 700;
`

export default Bold;