import styled, { css } from 'styled-components';
import BaseSection from './BaseSection';

const getBGColor = (props) => {
  if (props.lightGray) {
    return props.theme.lightGrayBGColor;
  }
  return props.theme.mainBackgroundColor;
}

const BaseContentSection = styled(BaseSection)`
  padding-top: ${props => props.noTopPadding ? '0' : '3rem'};
  padding-bottom: 3rem;

  background-color: ${getBGColor};

  ${
    props => props.showTopMarker &&
      css`
        &::before {
          content: '';
          position:absolute;
          width: 1rem;
          height: 1rem;
          transform: translateX(-50%) rotate(45deg);
          top: -0.5rem;
          left: 50%;
          background-color: ${getBGColor};
          @media (min-width: 768px) {
            left: auto;
            transform: translateX(0) rotate(45deg);
          }
        }
      `
  }
`

export default BaseContentSection;