import React from 'react';
import autobind from 'autobind-decorator';

class ImageLoadDetector extends React.Component {

  constructor(...args) {
    super(...args);
    this.handleLoadComplete = this.handleLoadComplete.bind(this)
    this.handleLoadFail = this.handleLoadFail.bind(this)

  }
  state = {
    loaded: false,
  };

  // @autobind
  handleLoadComplete() {
    this.setState({ loaded: true });
  }

  // @autobind
  handleLoadFail() {
    this.setState({ loaded: true });
  }

  clearLoader() {
    if (this.img) {
      this.img.onload = null;
      this.img.onerror = null;
      this.img = null;
    }
  }

  createLoader() {
    this.clearLoader();
    const img = new Image();
    img.src = this.props.url;

    if (img.complete) {
      this.setState({ loaded: true });
    } else {
      this.img = img;
      img.onload = this.handleLoadComplete;
      img.onerror = this.handleLoadFail;
    }
  }

  componentDidMount() {
    this.createLoader();
  }

  render() {
    return this.props.children(this.state.loaded);
  }
}

export default ImageLoadDetector;
