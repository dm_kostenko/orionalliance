import styled from 'styled-components';

const H5 = styled.h5`
  font-size: 1.05em;
  font-weight: 300;
  line-height: 1.2;
`

export default H5;
