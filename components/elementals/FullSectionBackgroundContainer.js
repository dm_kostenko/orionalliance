import styled from 'styled-components';

const FullSectionBackgroundContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
  

  /*@media (min-width: 768px) {
    right: ${props => `-${props.theme.sectionMarginLeftRight}`};
    left: ${props => `-${props.theme.sectionMarginLeftRight}`};
  }*/
`

export default FullSectionBackgroundContainer;