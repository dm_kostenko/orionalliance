import styled, { css } from 'styled-components';

const P = styled.p`
  font-size: 1em;
  font-weight: 300;
  ${props => !props.noTopPadding && css`
    & {
      padding-top: 1.5rem;
    }
  `}
  ${props => props.textIndent && css`
    & {
      text-indent: 1.5rem;
    }
  `}
  color: ${props => props.theme.textColor};

`

export default P;
