import React from 'react';
import styled, { css } from 'styled-components';
import cn from 'classnames';
import memoize from 'memoize-one';
import autobind from 'autobind-decorator';
import withPathPrefix from 'utils/withPathPrefix';
import scrollToContact from 'utils/scrollToContact';

import MenuItemLink from 'elementals/bootstrap/MenuItemLink';
import NavDropdown from 'elementals/bootstrap/NavDropdown';
import Navbar from 'elementals/bootstrap/Navbar';
import Nav from 'elementals/bootstrap/Nav';
import HighlightInject from 'elementals/HighlightInject';
import NavItemLink from 'elementals/bootstrap/NavItemLink';
import { withAppProps } from 'components/AppPropsContext';
import URLPrefixedLink from 'elementals/URLPrefixedLink';

import AppBrand from '../AppBrand';

import style from './style';

const Header = styled.header`

  ${style}

  position: absolute;

  z-index: 100;
  width: 100%;

  font-size: 1em;

  ${props =>
    props.showBg &&
    css`
      @media (min-width: 768px) {
        background-image: url(${withPathPrefix('/img/nav_bg.png')});
        background-repeat: no-repeat;
        background-size: cover;
        opacity: 0.95;
      }
      background-image: none;
      background-color: ${props => props.theme.nav.backgroundColor};
    `}

  height: ${props => (props.expanded ? '100vh' : 'inherit')};
  overflow-y: auto;
  @media (min-width: 768px) {
    overflow-y: inherit;
    min-height: auto;
    font-size: 0.8em;
  }
`;

class AppHeader extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {};
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this)
    this.handleContactUsClick = this.handleContactUsClick.bind(this)
    this.hasOpenedDropdown = memoize(state => {
      let hasOpened = false;
      for (let dropdownId in state) {
        if (state[dropdownId] === true) {
          hasOpened = true;
          break;
        }
      }
      return hasOpened;
    });
  }

  handleDropdownToggle(id, open) {
    this.setState({ [id]: open });
  }

  handleContactUsClick(e) {
    e.preventDefault();
    this.props.onToggle(false, function() {
      document
        .querySelector('#contact-us')
        .scrollIntoView({ behavior: 'smooth' });
    });
  }

  render() {
    const { className, showBg, appProps, expanded, onToggle } = this.props;

    const openEvent =
      appProps.touchDevice ||
      appProps.minWindowWidth < 768 ||
      isNaN(appProps.minWindowWidth)
        ? 'click'
        : 'mouseOver';

    const navClassName = cn('top', {
      'dropdown-open': this.hasOpenedDropdown(this.state),
    });

    return (
      <Header
        className={className}
        showBg={showBg || expanded}
        expanded={expanded}
      >
        <div style={{ background: '#0159b5', height: '10px', position: 'absolute', width: '100%', top: 0, left: 0 }}></div>
        <div style={{ background: '#f6ce01', height: '10px', position: 'absolute', width: '100%', top: '10px', left: 0 }}></div>
        <Navbar
          collapseOnSelect
          expanded={expanded}
          onToggle={onToggle}
        >
          <Navbar.Header>
            <URLPrefixedLink href='/' passHref>
              <AppBrand title='Orion Alliance' />
            </URLPrefixedLink>
            <Navbar.Toggle expanded={expanded} />
          </Navbar.Header>

          <Navbar.Collapse>
            <Nav className={navClassName} pullRight>
              <NavItemLink href='/' prefetch>
                HOME
              </NavItemLink>
              <NavItemLink href='/about-us' prefetch>
                ABOUT US
              </NavItemLink>
              <NavDropdown
                id='nav-drop-down-services'
                title='SERVICES'
                openEvent={openEvent}
                onToggle={this.handleDropdownToggle}
              >
                <MenuItemLink href='/application-support-maintenance' prefetch>
                  Application Support & Maintenance
                </MenuItemLink>
                <MenuItemLink href='/aws-cost-optimization' prefetch>
                  AWS Cost Optimization
                </MenuItemLink>
                <MenuItemLink href='/cloud-computing-development' prefetch>
                  Cloud Computing & Development
                </MenuItemLink>
                <MenuItemLink href='/data-analytics' prefetch>
                  Data Analytics
                </MenuItemLink>
                <MenuItemLink href='/devops' prefetch>
                  DevOps
                </MenuItemLink>
                <MenuItemLink
                  href='/enterprise-appication-development'
                  prefetch
                >
                  Enterprise Application Development
                </MenuItemLink>
                <MenuItemLink
                  href='/enterprise-application-integration'
                  prefetch
                >
                  Enterprise Application Integration
                </MenuItemLink>
                <MenuItemLink href='/infrastructure-support' prefetch>
                  Infrastructure Support
                </MenuItemLink>
                <MenuItemLink href='/it-outstaffing' prefetch>
                  IT Outstaffing
                </MenuItemLink>
                <MenuItemLink href='/legacy-migration' prefetch>
                  Legacy Migration
                </MenuItemLink>
                <MenuItemLink href='/mobile-application-development' prefetch>
                  Mobile Application Development
                </MenuItemLink>
                <MenuItemLink href='/web-application-development' prefetch>
                  Web Application Development
                </MenuItemLink>
                <MenuItemLink href='/qa-testing' prefetch>
                  QA & Testing
                </MenuItemLink>
                <HighlightInject />
              </NavDropdown>

              <NavDropdown
                id='nav-drop-down-industries'
                title='INDUSTRIES'
                openEvent={openEvent}
                onToggle={this.handleDropdownToggle}
              >
                <MenuItemLink href='/banking-finance-insurance' prefetch>
                  Banking, Finance and Insurance
                </MenuItemLink>
                <MenuItemLink href='/healthcare' prefetch>
                  Healthcare
                </MenuItemLink>
                <MenuItemLink href='/hospitality-travel-leisure' prefetch>
                  Hospitality, Travel and Leisure
                </MenuItemLink>
                <MenuItemLink
                  href='/industrial-manufacturing-software'
                  prefetch
                >
                  Industrial Manufacturing Software
                </MenuItemLink>
                <MenuItemLink href='/oil-gas' prefetch>
                  Oil and Gas
                </MenuItemLink>
                <MenuItemLink href='/retail-software' prefetch>
                  Retail Software
                </MenuItemLink>
                <MenuItemLink href='/transport' prefetch>
                  Transport
                </MenuItemLink>
                <MenuItemLink href='/energy-utilities' prefetch>
                  Energy and Utilities
                </MenuItemLink>
                <HighlightInject />
              </NavDropdown>
              <NavItemLink href='/vacancies' prefetch>
                VACANCIES
              </NavItemLink>
              <NavItemLink href='/blog' prefetch>
                BLOG
              </NavItemLink>
              <NavItemLink
                href='#contact-us'
                onClick={scrollToContact}
                preventDefault
              >
                CONTACT US
              </NavItemLink>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Header>
    );
  }
}

export default withAppProps(AppHeader);
