import { css } from 'styled-components';

const style = css`
  .navbar {
    position: relative;
    border: none;
    //rm
    min-height: 0;
    margin: 0;
  }

  //rm
  .navbar-default {
    background-color: transparent;
    border: none;
  }

  .container {
    display: flex;
    max-height: 100%;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
  }

  .navbar-header {
    flex: 1 0 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    @media (min-width: 768px) {
      flex: 0 0 auto;
      padding-bottom: 0;
    }
  }

  .navbar-toggle {
    display: inline-flex;
    flex: 0 0 auto;
    font-size: 2em;
    border: none;
    background-color: transparent;
    cursor: pointer;
    padding: 1rem;

    @media (min-width: 768px) {
      display: none;
    }
  }

  .container > .navbar-collapse {
    flex: 1 0 100%;
    display: flex;
    align-items: center;

    &.collapse {
      max-height: 0;
      opacity: 0;
      transition: all 0;
      overflow: hidden;
      @media (min-width: 768px) {
        display: flex;
        opacity: 1;
        max-height: inherit;
        overflow: visible;
      }

      & .navbar-nav {
        flex-direction: row;
      }

      &.in {
        opacity: 1;
        max-height: 2000px;
        transition: all 0.35s ease-in-out;

        margin-top: 1em;
        margin-left: -8vw;
        margin-right: calc(1rem - 8vw);
        overflow: visible;

        @media (min-width: 768px) {
          transition: none;
          margin: auto;
        }

        & .navbar-nav {
          flex-direction: column;
          &::before {
            content: '';
            display: block;
            width: 100%;
            height: 1px;
            background: #000;
            background: linear-gradient(to top, #777 0%, #000 100%);
          }
          @media (min-width: 768px) {
            flex-direction: row;
            &::before {
              content: none;
            }
          }
        }
      }
    }

    //font-size: 1.2em;

    @media (min-width: 768px) {
      font-size: 1em;
      flex-basis: 10em;
    }

    & .navbar-nav > li {
      &:not(:last-child)::after {
        content: '';
        display: block;
        width: 100%;
        height: 1px;
        background: #000;
        background: linear-gradient(to top, #777 0%, #000 100%);
      }

      @media (min-width: 768px) {
        padding: 0;

        &:first-child::before {
          content: none;
        }

        &:not(:last-child)::after {
          content: none;
        }
      }
    }
  }

  .nav {
    list-style: none;
  }

  .navbar-nav {
    flex: 1 0 10em;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;

    &.navbar-right {
      justify-content: flex-end;
    }
  }

  .navbar-nav > li {
    max-height: 2000px;
    transition: all 0.35s ease-in-out;
    @media (min-width: 768px) {
      transition: none;
      padding: 0;
    }
  }

  .navbar-nav {
    & > li > a {
      position: relative;
      padding: 1.25em 1.25em;
      width: 100%;
      color: ${props => props.theme.nav.ancColor};
      display: inline-block;
      white-space: nowrap;

      &:hover {
        color: ${props => props.theme.nav.ancColorHover};
      }

      @media (min-width: 768px) {
        margin: 1em 1.5em;
        padding: 0;
        width: auto;
      }
    }
    &.top > li > a {
      @media (min-width: 768px) {
        &::before {
          content: '';
          position: absolute;
          height: ${props => props.theme.borderWidth};
          width: 0;
          bottom: -0.25em;

          background-color: ${props => props.theme.highlightColor};
          transition: width 0.35s ease-in-out;
        }

        &:hover::before {
          width: 100%;
          box-shadow: 0 0 1em 0.2em ${props => props.theme.highlightShadowColor};
        }
      }
    }
  }

  .navbar-nav {
    & > li.active > a {
      color: ${props => props.theme.nav.ancColorActive};
      &:hover {
        color: ${props => props.theme.nav.ancColorActiveHover};
      }
    }

    &.top > li.active > a {
      @media (min-width: 768px) {
        &::before {
          width: 1rem;
          box-shadow: 0 0 1em 0.2em ${props => props.theme.highlightShadowColor};
        }
        &:hover::before {
          width: 100%;
        }
      }
    }
  }

  .navbar-nav.dark > li {
    > a {
      color: ${props => props.theme.nav.ancColorDark};
      &:hover {
        color: ${props => props.theme.nav.ancColorHoverDark};
      }
    }

    &.active > a {
      color: ${props => props.theme.nav.ancColorActiveDark};
      &:hover {
        color: ${props => props.theme.nav.ancColorActiveHoverDark};
      }
    }
  }

  .navbar-nav > li:last-child > a {
    @media (min-width: 768px) {
      margin-right: 0;
    }
  }

  .navbar-nav.dropdown-open > li {
    &:not(.open) {
      max-height: 0;
      opacity: 0;
    }

    @media (min-width: 768px) {
      &:not(.open) {
        max-height: 2000px;
        opacity: 1;
      }
    }
  }

  .navbar-nav.top > li.dropdown.open > a {
    @media (min-width: 768px) {
      &::before {
        content: '';
        opacity: 1;
        left: 0;
        width: 100%;
      }
    }
  }

  .dropdown {
    position: relative;
    & > a {
      transition: all 0.35s ease-in-out;
      cursor: pointer;
      &::before {
        content: '';
        position: absolute;
        height: ${props => props.theme.borderWidth};
        width: 1em;
        bottom: 50%;
        left: 1em;

        background-color: ${props => props.theme.highlightColor};
        opacity: 0;
        transition: all 0.35s ease-in-out;
      }
      @media (min-width: 768px) {
        transition: none;
        &::before {
          content: none;
        }
      }
    }

    @media (min-width: 768px) {
      &.active > a {
        &::before {
          content: '';
          opacity: 1;
          left: 0;
          width: 100%;
        }
      }
    }

    &.open > a {
      padding-left: 3em;
      &::before {
        box-shadow: 0 0 1em 0.2em ${props => props.theme.highlightShadowColor};
        opacity: 1;
      }
      @media (min-width: 768px) {
        padding-left: 0;
        &::before {
          left: 0;
        }
      }
    }

    &.open > .dropdown-menu {
      opacity: 1;
      max-height: 2000px;
      overflow: visible;
      transition: all 0.35s ease-in-out;
      @media (min-width: 768px) {
        transition: opacity 0.35s ease-in-out;
      }
    }

    @media (min-width: 768px) {
      &.expand-menu.open > .dropdown-menu {
        &::before {
          content: ' ';
          position: absolute;
          bottom: 100%;
          height: 2.25em;
          width: 100%;
        }
      }
    }
  }

  .dropdown-menu {
    display: flex;
    position: relative;
    flex-direction: column;

    list-style: none;

    overflow: hidden;
    max-height: 0;
    opacity: 0;
    transition: all 0.35s ease-in-out;

    @media (min-width: 768px) {
      position: absolute;
      top: calc(100% + 1em);
      left: 50%;
      transform: translateX(-50%);

      width: max-content;

      border: ${props => props.theme.borderWidth} solid
        ${props => props.theme.nav.borderColor};
      background-color: ${props => props.theme.nav.backgroundColor};

      &::after {
        content: '';
        position: absolute;
        z-index: -1;
        top: -0.05em;
        right: 50%;

        width: 0.70710678em;
        height: 0.70710678em;

        transform: translateY(-50%) translateX(50%) rotate(45deg);
        background-color: ${props => props.theme.nav.backgroundColor};
        border-style: solid;
        border-color: ${props => props.theme.nav.borderColor};
        border-width: 1px 0 0 1px;
      }
    }
  }

  .dropdown-menu > li {
    width: 100%;
    &:first-of-type::before {
      content: '';
      display: block;
      width: 100%;
      height: 1px;
      //background: #000;
      background: linear-gradient(to top, #777 0%, #000 100%);
    }
    &:not(:last-of-type)::after {
      content: '';
      display: block;
      width: 100%;
      height: 1px;
      background: #000;
      background: linear-gradient(to top, #777 0%, #000 100%);
    }
    @media (min-width: 768px) {
      &:first-of-type::before {
        content: none;
      }
      &:not(:last-of-type)::after {
        content: none;
      }
    }
  }

  .dropdown-menu {
    & > li > a {
      color: ${props => props.theme.nav.ancColor};

      &:hover {
        color: ${props => props.theme.nav.ancColorHover};
      }
    }

    & > li.active > a {
      color: ${props => props.theme.nav.ancColorActive};
      &:hover {
        color: #ffe;
      }
    }
  }

  .dropdown-menu > li {
    &:hover {
      background: radial-gradient(
        ellipse at center,
        rgba(51, 110, 145, 0.5) 0%,
        rgba(51, 110, 145, 0.84) 100%
      );
      a {
        color: ${props => props.theme.nav.ancColorHover};
      }
    }
    & > a {
      display: block;
      position: relative;

      padding: 1.25em 3em;
      width: 100%;

      text-align: left;

      @media (min-width: 768px) {
        padding: 0.75em 1.25em;
        width: 100%;
      }
    }
  }

  .caret {
    display: inline-block;

    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px dashed;
    border-top: 4px solid \9;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
  }
`;

export default style;
