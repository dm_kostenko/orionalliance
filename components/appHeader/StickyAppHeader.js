import styled from 'styled-components';
import { Sticky } from 'react-sticky';
import AppHeader from './AppHeader';

const StyledAppHeader = styled(AppHeader)`
  padding: 1rem calc(8vw - 1rem) 1rem 8vw;
  position: ${props => props.isSticky ? 'fixed' : 'absolute'};
  @media (min-width: 768px) {
    padding-top: ${props => props.isSticky ? '0.5em' : '1em'};
    padding-bottom: 0.5em;
    padding-left: 3em;
    padding-right: 3em;
    
  }
  @media (min-width: 992px) {
    padding-left: 3em;
    padding-right: 3em;
  }
`

const StickyAppHeader = React.forwardRef((props, ref) => (
  <Sticky
    disableCompensation={true}
    topOffset={1}
  >
    {({ isSticky }) => (
      <StyledAppHeader
        {...props}
        isSticky={isSticky}
        showBg={isSticky}
        ref={ref}
      />
    )}
  </Sticky>
))

export default StickyAppHeader;

