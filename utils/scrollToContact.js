//import Router from 'next/router';

const scrollToContact = (e) => {
  e.preventDefault();
  document.querySelector('#contact-us').scrollIntoView({ behavior: 'smooth' });
  //const href = Router.pathname + '#contact-us';
  //Router.replace(href, href, { shallow: true });
  history.replaceState(undefined, undefined, "#contact-us")
}

export default scrollToContact;