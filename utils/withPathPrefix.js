
let urlPathPrefix;

function setUrlPathPrefix (url) {
  urlPathPrefix = url
}

function withPathPrefix(path) {
  if (/^https?:\/\//.test(path)) {
    return path;
  }

  return `${urlPathPrefix || ''}${path}`;
}

export {
  withPathPrefix as default,
  setUrlPathPrefix,
}

//withPathPrefix.setUrlPathPrefix = setUrlPathPrefix;

//module.exports = withPathPrefix;