const phoneRegexp = /(\d+)/g;

const getPhoneNumberArray = str => str.match(phoneRegexp);

const getDisplayPhoneNumber = (phoneNumberArray, sep = ' ') =>
  `+${phoneNumberArray.join(sep)}`;

const getCommonInfo = (rawData, email) => {
  const phoneNumberArray = getPhoneNumberArray(rawData.telephone);
  return {
    hrefEmail: `mailto:${email}`,
    displayEmail: email,
    country: rawData.country,
    displayPhoneNumber: getDisplayPhoneNumber(phoneNumberArray, '&nbsp;'),
    hrefPhoneNumber: `tel:+${phoneNumberArray.join('')}`,
  };
};

export const netherlandsForAboutUs = (rawData, email) => ({
  ...getCommonInfo(rawData, email),
  address: [rawData.streetAddress, rawData.postalCode, rawData.city].join(', '),
});

export const latviaForAboutUs = (rawData, email) => ({
  ...getCommonInfo(rawData, email),
  address: [rawData.streetAddress, rawData.city, rawData.postalCode].join(', '),
});

export const polandForAboutUs = (rawData, email) => ({
  ...getCommonInfo(rawData, email),
  address: `${rawData.streetAddress}, ${rawData.postalCode} ${rawData.city}`,
});

export const jsonLD = (rawData, url, assetPrefix) => ({
  '@context': 'https://schema.org',
  '@type': 'Organization',
  name: rawData.name,
  email: rawData.email,
  url,
  logo: url + assetPrefix + rawData.logo,
  address: rawData.address.map(addressData => ({
    '@type': 'PostalAddress',
    addressCountry: addressData.country,
    addressLocality: addressData.city,
    postalCode: addressData.postalCode,
    streetAddress: addressData.streetAddress,
    telephone: getDisplayPhoneNumber(
      getPhoneNumberArray(addressData.telephone)
    ),
    contactType: 'customer service',
  })),
  description: rawData.description,
});

/*


    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "name": "VironIT",
      "email": "info@vironit.com",
      "numberOfEmployees": "57",
       "address": {
        "@type": "PostalAddress",
        "addressLocality": "Minsk, Belarus",
        "postalCode": "220005",
        "streetAddress": "58-34 Independence Ave"
      },
      "url": "https://www.vironit.com",
      "logo": "https://vironit-bevc00m.netdna-ssl.com/wp-content/uploads/2015/10/logo.png",
      "contactPoint": [{
        "availableLanguage" : ["English","Russian", "Belarusian"],
        "@type": "ContactPoint",
        "telephone": "+375-17-336-53-06",
        "contactType": "customer service"
      }],
      "sameAs": [
        "https://www.facebook.com/VironIT2004",
        "https://plus.google.com/109107613977774771958",
        "https://www.instagram.com/vironit_dev",
        "https://twitter.com/vironit",
      "https://www.linkedin.com/company/viron-it"
      ],
     "founder": {
          "@type": "Person",
          "name": "Maksim Osipau",
          "sameAs": "https://www.facebook.com/m.osipau"
        },
     "founder": {
          "@type": "Person",
          "name": "Aliaksei Hrakau"
        }
    }
    

*/
