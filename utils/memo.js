
const memo = (Component) => {
  const MemoComponent = React.memo(Component);

  return (props) => <MemoComponent {...props} />
}

export default memo;