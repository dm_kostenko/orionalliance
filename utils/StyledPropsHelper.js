import get from 'lodash/get';

const getThemeProp = (propPath) => (props) =>(get(props.theme, propPath))
const getProp = (propPath) => (props) =>(get(props, propPath))

export default {
  getThemeProp,
  getProp,
}