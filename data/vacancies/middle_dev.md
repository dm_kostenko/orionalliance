### Middle Software developer

We invite **Middle Software developer** to the office in Gdansk, Poland.

**Requirements for qualifications and experience:**

*	3+ years experience of developing “C” similar language (“C”, “C++”, “C#”, java, python)
*	experience with hadoop (horton works, clouderra)
*	linux administration experience (2+ years)
*	virtualization experience

**Key skills:**

C, C++, C#, java, python, shell, bash, bigdata, HDFS, KVM, hadoop, horton works, clouderra

**What we offer:**

*	relocation to Poland, Gdansk
*	sponsorship of relocation costs (visa for an employee and his family, tickets, accommodation for 2 weeks, assistance in finding accommodation)
*	formal employment under a full employment contract
*	salary according to the results of the interview and carrying out technical assignment is starting from $ 2500
*	the opportunity to obtain a residence permit in Poland, assistance in the preparation of documents
*	flexible working hours
*	Russian speaking team
*	office in a new business center in the business district of Gdansk

**Type of employment:**

Full-time, flexible hours
