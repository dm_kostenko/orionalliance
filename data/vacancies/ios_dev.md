### IOS developer

We invite specialists to the **IOS developer** position to the office in Poland, Gdansk. We provide software development services and technical customer support in the banking and financial sectors.

**Responsibilities:**

* development of mobile applications for the iOS platform
* team work with colleagues (in Russian)
* participation in business correspondence and telephone conferences with colleagues from the company's American office (in English)


**Requirements:**

* Xcode / iOS knowledge
* Knowledge of Swift 4, RxSwift, RxCocoa
* Knowledge of drm video (fairplay)
* Experience in developing and using various interface components
* Knowledge and understanding of OOP principles
* Knowledge of network protocols and understanding of interaction with the server: rest / json-rpc / protobuffers, etc.
* Feel confident using git


**Conditions of employment and relocation:**

* relocation to Poland, Gdansk
*	sponsorship of relocation costs (visa for an employee and his family, tickets, accommodation for 2 weeks, assistance in finding accommodation)
*	formal employment under a full employment contract
*	salary according to the results of the interview and carrying out technical assignment is starting from $ 2500
*	the opportunity to obtain a residence permit in Poland, assistance in the preparation of documents
*	flexible working hours
*	Russian speaking team
*	office in a new business center in the business district of Gdansk


**Key skills**

Swift 4 RxSwift RxCocoa drm video REST OOP Git Xcode