### JavaScript developer

We invite specialists to the **JavaScript developer** position to the office in Gdansk, Poland. We provide software development services and technical customer support in the banking and financial sectors.

**What you’ll do:**

* developing RIA on NodeJs and React
*	maintenance of existing UI ExtJS, jQuery
*	team work with colleagues (in Russian)
*	participation in business correspondence and telephone conferences with colleagues from the company's American office (in English)

**Requirements for qualifications and experience:**

*	Development knowledge of ES6, HTML, CSS / LESS
*	Experience with ReactJS, Redux, NodeJS, Webpack
*	Experience with version control systems
*	Intermediate and higher English proficiency

**Desirable:**

*	Code Testing Experience (TDD, BDD)
*	Knowledge of Java (ExtGWT)
*	Knowledge of ActionScript3, Adobe Flex
*	Knowledge of ExtJS 3

**Conditions of employment and relocation:**

*	relocation to Poland, Gdansk
*	sponsorship of relocation costs (visa for an employee and his family, tickets, accommodation for 2 weeks, assistance in finding accommodation)
*	formal employment under a full employment contract
*	salary according to the results of the interview and carrying out technical assignment is starting from $ 2500
*	the opportunity to obtain a residence permit in Poland, assistance in the preparation of documents
*	flexible working hours
*	Russian speaking team
*	office in a new business center in the business district of Gdansk

**Key skills:**

Java, JavaScript, HTML, CSS, Flash Actionscript, React, ExtJS, Flux
