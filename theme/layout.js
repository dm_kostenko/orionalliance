const sectionMarginLeftRight = '12.5vw';
const sectionMarginLeftRightMobile = '8vw';

const sectionHeading = {
  paddingTopBottom: '1.25rem',
  paddingLeftRight: 0,
}

const sectionItemPaddingToCenter = '2.5rem';
const sectionItemPaddingToBorder = '2rem';
const sectionItemPaddingTop = '1rem';

const bannerMarginTop = '0';
const bannerMarginBottom = '3rem';

const bannerMarginIndex = `${bannerMarginTop} 0 ${bannerMarginBottom}`;
const bannerMargin = `6rem 0 ${bannerMarginBottom}`;
const bannerMarginNarrowMobile = `${bannerMarginTop} ${sectionMarginLeftRightMobile} ${bannerMarginBottom}`;
const bannerMarginNarrow = `${bannerMarginTop} ${sectionMarginLeftRight} ${bannerMarginBottom}`;

const borderWidth = '1px';

const simpleBannerTop = '3.425em';
const simpleBannerMargin = `${simpleBannerTop} 8vw`;
const simpleBannerMarginNarrow = `${simpleBannerTop} ${sectionMarginLeftRight} 0`;
const simpleBannerMarginNarrowMobile = `${simpleBannerTop} ${sectionMarginLeftRightMobile} 0`;

export default {
  /*general*/
  simpleBannerTop,
  simpleBannerMargin,
  simpleBannerMarginNarrow,
  simpleBannerMarginNarrowMobile,

  sectionMarginLeftRightMobile,
  sectionMarginLeftRight,

  bannerMarginIndex,
  bannerMargin,
  bannerMarginNarrow,
  bannerMarginNarrowMobile,
  sectionItemPaddingToCenter,

  borderWidth,

  sectionMargin: `
    0 
    ${sectionMarginLeftRight}
  `,
  sectionMarginMobile: `
    0 
    ${sectionMarginLeftRightMobile}
  `,
  sectionHeadingPadding: `
    ${sectionHeading.paddingTopBottom}
    ${sectionHeading.paddingLeftRight}
  `,

  /*sections*/
  /*solutions*/
  solutionsHeadingPadding:`
    ${sectionHeading.paddingTopBottom}
    0
    ${sectionHeading.paddingTopBottom}
    ${sectionItemPaddingToBorder}
  `,
  solutionsTextPadding: `
    0
    ${sectionItemPaddingToCenter}
    0
    ${sectionItemPaddingToBorder}
  `,
  solutionsTextPaddingMobile: `
    1.5em
    ${sectionItemPaddingToCenter}
    1.5em
    ${sectionItemPaddingToBorder}
  `,
  /*overview*/
  overviewTextPadding: `
    0
    ${sectionItemPaddingToBorder}
    8rem
    ${sectionItemPaddingToCenter}
  `,
}