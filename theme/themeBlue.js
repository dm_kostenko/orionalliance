import layout from './layout';

const textColor = '#464646'; //gray
const textColorAlt = '#fff';
const textColorGray = '#c3c3c3';

const elementColor = '#4ca5db';
const elementColorAlt = textColorAlt;
const elementColorAlt2 = '#555';

const controlsBorderColor = 'rgba(145, 221, 250, 0.66)';
const controlsBorderColorActive = '#fff';

const mainBackgroundColor = '#fff';
const altBackgroundColor = '#2e75a1';

const verticalSeparatorColor = '#ebebeb';
const verticalSeparatorColorAlt = elementColor;

const imageBoxShadow = '0 0 10rem -3rem #000';

export default {
  ...layout,

  textColor,
  textColorAlt,
  textColorGray,
  elementColor,
  elementColorAlt,
  elementColorAlt2,
  mainBackgroundColor,
  altBackgroundColor,
  controlsBorderColor,
  controlsBorderColorActive,

  verticalSeparatorColor,
  verticalSeparatorColorAlt,

  imageBoxShadow,

  lightGrayBGColor: '#f7f7f7',
  darkGrayBGColor: '#f0f0f0',

  /*highlight inject*/
  highlightColor: '#fff',
  highlightShadowColor: '#4ca5db',

  /*section header*/
  sectionHeaderColor1: elementColor,
  sectionHeaderColor2: elementColorAlt2,

  /*header separator*/
  headerSeparatorColor: elementColor,
  headerSeparatorColorAlt: elementColorAlt,

  /*main button*/
  mainButtonColor: 'rgba(255,255,255,1)',
  mainButtonBorderColor: controlsBorderColor,
  mainButtonShadowColor: 'rgba(10, 181, 250, 0.65)',
  //'#2e90b4',
  //'rgba(10, 181, 250, 0.3)',

  /*tile icon*/
  tileIconBorderColor: controlsBorderColor,
  tileIconShadowColor: 'rgba(66, 189, 240, 0.5)',

  /*text link*/
  linkColor: elementColor,
  linkColorAlt: elementColorAlt,

  /*Carousel*/
  carousel: {
    arrowsWidth: '10.5%',
    contentWidth: '79%',
    contentPadding: '2rem 0',
    arrowsSize: '1rem',
    arrowsLineWidth: layout.borderWidth,
    arrowsColor: textColor,
  },

  footerBackgroundColor: '#18181a',

  /*intro*/
  introBannerColors: ['#fff', textColorGray],

  solutionsTileBackgroundColor: '#fff',
  solutionsTileBackgroundColorAlt: '#f2f2f2',
  solutionsTileBackgroundColorHover: altBackgroundColor,
  solutionTextColor: textColorAlt,
  solutionTextBackgroundColor: altBackgroundColor,

  /*opinions*/
  opinionsBackgroundColor: '#f3f3f4',

  opinionTileHeadingColor: elementColor,
  opinionTileRatingColor: '#ffc80a',
  opinionTileMargin: '1rem 2rem 5rem',
  opinionTileMarginOdd: '5rem 2rem 1rem',

  /*  !!!!!REMOVE THIS industries/banking*/
  indBankingServicesBackgroundColor: '#f7f7f7',
  indBankingServicesListBackgroundColor: '#f0f0f0',
  chooseTilesColors: ['#2b2b2b', '#222222', '#1a1a1a', '#0f0f0f', '#000000'],

  /*footer*/
  footerTextColor: 'rgb(90, 90, 90)',
  dividerColor: 'rgb(33, 33, 33)',

  errorTextColor: 'red',

  addressBorders: ['#eaeaec', '#f6f6f8'],

  nav: {
    borderColor: 'rgba(145, 221, 250, 0.66)',

    backgroundColor: '#141212',

    ancColor: 'rgb(149, 149, 149)',
    ancColorHover: 'rgb(184, 184, 184)',
    ancColorActive: 'rgb(219, 219, 219)',
    ancColorActiveHover: 'rgb(255,255,255)',

    ancColorDark: 'rrgb(125, 125, 125)',
    ancColorHoverDark: 'rgb(150, 150, 150)',
    ancColorActiveDark: 'rgb(175, 175, 175)',
    ancColorActiveHoverDark: 'rgb(200, 200, 200)',
  },
};
