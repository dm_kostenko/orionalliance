FROM node:latest
ARG PUBLIC_URL
ENV NODE_ENV=production
ENV PORT=80
WORKDIR /usr/src/app
COPY . /usr/src/app

RUN npm install && \
    npm run build

ENTRYPOINT ["/usr/local/bin/node", "server/server.js"]
EXPOSE 80