const appConfig = require('../app.config');

function createCustomAppProps() {
  return {
    currentYear: new Date().getFullYear(),
    urlPathPrefix: appConfig.urlPathPrefix,
  }
}

function appPropsMiddleware(req, res, next) {
  req.customAppProps = createCustomAppProps();
  next();
}

module.exports = appPropsMiddleware;