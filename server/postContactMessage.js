const nodemailer = require('nodemailer');
const util = require('util');

const validateContactForm = require('../shared/validateContactForm');

const transport = nodemailer.createTransport({
  host: 'localhost',
  port: 25,
  tls: {
     rejectUnauthorized: false
  },
});

const sendMailAsync = util.promisify(transport.sendMail.bind(transport));

const getSubject = name => `Orion Alliance: message from ${name}`;

const getText = (name, email, contactNumber, message) =>
  (`This is a message send via orion-alliance.com

Sender name: ${name}
Sender email: ${email}
Sender contact number: ${contactNumber ? contactNumber : 'none'}

Message:
${message}
`)

async function sendMail(values) {
  const { name, email, contactNumber, message } = values;

  const mailOptions = {
    from: 'info@orion-alliance.com',
    to: 'info@orion-alliance.com',
    subject: getSubject(name),
    text: getText(name, email, contactNumber, message),
  };

  try {
    await sendMailAsync(mailOptions);
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}

async function postContactMessage(req, res) {
  const values = req.body || {};

  const errors = validateContactForm(values);

  let result = {};

  if (Object.keys(errors).length) {
    result.error = 'Input is incorrect';
  } else if (!await sendMail(values)) {
    result.error = 'Can not send your mail now. Please try again later.';
  }
  res.send(JSON.stringify(result));
}

module.exports = postContactMessage;