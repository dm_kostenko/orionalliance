const sharp = require('sharp');
const onHeaders = require('on-headers');

function getResize(resize) {
  const numResize = Number(resize);
  if (!Number.isNaN(numResize)) {
    return { width: numResize };
  } else {
    return {};
  }
}

function createStream(resize) {
  return sharp().resize(getResize(resize));
}

function imageResizeMiddleware(req, res, next) {
  const { query } = req;
  if (query.hasOwnProperty('s')) {
    const resizeParam = query.s;

    let ended = false;
    let listeners = [];
    let stream;

    let _end = res.end;
    let _on = res.on;
    let _write = res.write;

    res.flush = function flush() {
      if (stream) {
        stream.flush();
      }
    };

    res.write = function write(chunk, encoding) {
      if (ended) {
        return false;
      }

      if (!this._header) {
        this._implicitHeader();
      }

      return stream
        ? stream.write(Buffer.from(chunk, encoding))
        : _write.call(this, chunk, encoding);
    };

    res.end = function end(chunk, encoding) {
      if (ended) {
        return false;
      }

      if (!stream) {
        return _end.call(this, chunk, encoding);
      }

      // mark ended
      ended = true;

      // write Buffer for Node.js 0.8
      return chunk ? stream.end(Buffer.from(chunk, encoding)) : stream.end();
    };

    res.on = function on(type, listener) {
      if (!listeners || type !== 'drain') {
        return _on.call(this, type, listener);
      }

      if (stream) {
        return stream.on(type, listener);
      }

      // buffer listeners for future stream
      listeners.push([type, listener]);

      return this;
    };

    onHeaders(res, function onResponseHeaders() {
      if (this.statusCode === 304) {
        addListeners(res, _on, listeners);
        listeners = null;
        return;
      }

      stream = createStream(resizeParam);

      addListeners(stream, stream.on, listeners);

      res.removeHeader('Content-Length');

      stream.on('data', function onStreamData(chunk) {
        if (_write.call(res, chunk) === false) {
          stream.pause();
        }
      });

      stream.on('end', function onStreamEnd() {
        _end.call(res);
      });

      _on.call(res, 'drain', function onResponseDrain() {
        stream.resume();
      });
    });

    next();
  } else {
    next();
  }
}

function addListeners(stream, on, listeners) {
  for (var i = 0; i < listeners.length; i++) {
    on.apply(stream, listeners[i]);
  }
}

module.exports = imageResizeMiddleware;
