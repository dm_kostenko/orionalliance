const express = require('express');
const next = require('next');
const compression = require('compression');
const path = require('path');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const nakedRedirect = require('express-naked-redirect');

const postContactMessage = require('./postContactMessage');
const getVacancies = require('./getVacancies');
const appPropsMiddleware = require('./appPropsMiddleware');
const imageResizeMiddleware = require('./imageResizeMiddleware');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const rootDir = path.resolve(__dirname, '..');
(async function run() {
  await app.prepare();
  const server = express();

  server.use(helmet());
  server.use(
    nakedRedirect({
      reverse: true,
      status: 301,
    })
  );
  server.use(compression());
  //server.use(/.*\/img\/.*\.(jpg|png)/, imageResizeMiddleware);
  server.use(express.static(path.resolve(rootDir, 'static')));
  server.post('/contact', bodyParser.json(), postContactMessage);
  server.get('/get_vacancies', getVacancies);
  //only routes in pages
  server.use(appPropsMiddleware);
  server.get('*', handle);

  server.listen(port, error => {
    if (error) {
      throw error;
    }

    console.log(`Ready on ${port} port`);
  });
})();
