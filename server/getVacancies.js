const path = require('path');
const loadVacancies = require('../shared/loadVacancies');

const rootDir = path.resolve(__dirname, '..');

function getVacancies(req, res) {
  const { data, error } = loadVacancies(rootDir);
  if (error) {
    res.status(500).send(error);
  } else {
    res.send(JSON.stringify(data));
  }
}

module.exports = getVacancies;