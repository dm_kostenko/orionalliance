const withBundleAnalyzer = require('@zeit/next-bundle-analyzer');
const path = require('path');
const appConfig = require('./app.config');

const dev = process.env.NODE_ENV !== 'production';

module.exports = withBundleAnalyzer({
  publicRuntimeConfig: {
    GA_TRACKING_ID: appConfig.GA_TRACKING_ID
  },
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` module
    const originalEntry = config.entry;
    config.entry = async () => {
      const entries = await originalEntry();

      if (
        entries['main.js'] &&
        !entries['main.js'].includes('./client/polyfills.js')
      ) {
        entries['main.js'].unshift('./client/polyfills.js');
      }

      return entries;
    };

    config.node = {
      fs: 'empty',
    };
    config.resolve = {
      ...config.resolve,
      alias: {
        ...config.resolve.alias,
        elementals: path.resolve(__dirname, 'components/elementals'),
        components: path.resolve(__dirname, 'components'),
        utils: path.resolve(__dirname, 'utils'),
        shared: path.resolve(__dirname, 'shared'),
      },
    };

    return config;
  },
  assetPrefix: appConfig.urlPathPrefix,

  analyzeServer: ['server', 'both'].includes(process.env.BUNDLE_ANALYZE),
  analyzeBrowser: ['browser', 'both'].includes(process.env.BUNDLE_ANALYZE),
  bundleAnalyzerConfig: {
    server: {
      analyzerMode: 'static',
      reportFilename: '../../bundles/server.html',
    },
    browser: {
      analyzerMode: 'static',
      reportFilename: '../bundles/client.html',
    },
  },
});
